package in.net.webinfotech.ashia.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.presentation.presenters.WalletPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.WalletPresenterImpl;
import in.net.webinfotech.ashia.presentation.ui.adapters.WalletHistoryAdapter;
import in.net.webinfotech.ashia.threading.MainThreadImpl;

public class WalletActivity extends AppCompatActivity implements WalletPresenter.View {

    WalletPresenterImpl mPresenter;
    @BindView(R.id.txt_view_wallet_amount)
    TextView txtViewWalletAmount;
    @BindView(R.id.recycler_view_wallet_histories)
    RecyclerView recyclerViewWalletHistories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);
        initialisePresenter();
        mPresenter.fetchWalletAmount();
        mPresenter.fetchWalletHistories();
        getSupportActionBar().setTitle("My Wallet");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new WalletPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadWalletAmount(long walletAmount) {
        txtViewWalletAmount.setText("Rs. " + walletAmount);
    }

    @Override
    public void loadWalletHistoriesAdapter(WalletHistoryAdapter adapter) {
        recyclerViewWalletHistories.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewWalletHistories.setAdapter(adapter);
        recyclerViewWalletHistories.setNestedScrollingEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
