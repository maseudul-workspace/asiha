package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.SubCategoryList;
import in.net.webinfotech.ashia.util.GlideHelper;

/**
 * Created by Raj on 09-07-2019.
 */

public class SubcategoryGridAdapter extends RecyclerView.Adapter<SubcategoryGridAdapter.ViewHolder>{

    public interface Callback{
        void onSubCategoryClicked(int id);
    }

    Context mContext;
    SubCategoryList[] subCategories;
    Callback mCallback;

    public SubcategoryGridAdapter(Context mContext, SubCategoryList[] subCategories, Callback callback) {
        this.mContext = mContext;
        this.subCategories = subCategories;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_subcategory, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        GlideHelper.setImageView(mContext, viewHolder.imgViewSubcategory, subCategories[i].imgPath);
        viewHolder.txtViewSubcategory.setText(subCategories[i].subCategoryName);
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSubCategoryClicked(subCategories[i].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subCategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_subcategory)
        ImageView imgViewSubcategory;
        @BindView(R.id.txt_view_subcategory)
        TextView txtViewSubcategory;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
