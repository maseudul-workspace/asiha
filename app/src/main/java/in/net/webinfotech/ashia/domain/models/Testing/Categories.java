package in.net.webinfotech.ashia.domain.models.Testing;

/**
 * Created by Raj on 09-07-2019.
 */

public class Categories {
    public String imageUrl;
    public String categoryName;

    public Categories(String categoryName, String imageUrl) {
        this.imageUrl = imageUrl;
        this.categoryName = categoryName;
    }
}
