package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetHomeDataInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.HomeData;
import in.net.webinfotech.ashia.domain.models.HomeDataWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 13-07-2019.
 */

public class GetHomeDataInteractorImpl extends AbstractInteractor implements GetHomeDataInteractor {

    OtherRepositoryImpl mRepository;
    Callback mCallback;

    public GetHomeDataInteractorImpl(Executor threadExecutor, MainThread mainThread, OtherRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDataGettingFail(errorMsg);
            }
        });
    }

    private void postMessage(final HomeData homeData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDataGettingSuccess(homeData);
            }
        });
    }


    @Override
    public void run() {
        final HomeDataWrapper homeDataWrapper = mRepository.getHomeData();
        if(homeDataWrapper == null){
            notifyError("Something went wrong");
        }else if(!homeDataWrapper.status){
            notifyError("Something went wrong");
        }else{
            postMessage(homeDataWrapper.homeData);
        }
    }
}
