package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 23-08-2019.
 */

public interface AddToWishListInteractor {
    interface Callback {
        void onWishListAddingSuccess();
        void onWishListAddingFail(String errorMsg);
    }
}
