package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 30-07-2019.
 */

public interface RegistrationInteractor {
    interface Callback{
        void onAddDownlineSuccess();
        void onAddDownlineFail(String errorMsg);
    }
}
