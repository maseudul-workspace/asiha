package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 21-08-2019.
 */

public class OrderProducts {

    @SerializedName("productId")
    @Expose
    public int productId;

    @SerializedName("proName")
    @Expose
    public String productName;

    @SerializedName("brand")
    @Expose
    public String brand;

    @SerializedName("url")
    @Expose
    public String url;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("quantity")
    @Expose
    public String quantity;

}
