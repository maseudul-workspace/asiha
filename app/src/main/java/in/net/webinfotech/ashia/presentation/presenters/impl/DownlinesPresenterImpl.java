package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.FetchDownlinesInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.FetchDownlineInteractorImpl;
import in.net.webinfotech.ashia.domain.models.Downlines;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.presentation.presenters.DownlinesPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.ui.adapters.DownlinesAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 05-08-2019.
 */

public class DownlinesPresenterImpl extends AbstractPresenter implements DownlinesPresenter, FetchDownlinesInteractor.Callback{

    Context mContext;
    DownlinesPresenter.View mView;
    FetchDownlineInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    DownlinesAdapter adapter;

    public DownlinesPresenterImpl(Executor executor, MainThread mainThread, Context context, View mView) {
        super(executor, mainThread);
        mContext = context;
        this.mView = mView;
    }

    @Override
    public void fetchDownlines() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new FetchDownlineInteractorImpl(mExecutor, mMainThread,this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
        mInteractor.execute();
    }

    @Override
    public void onGetDownlinesSuccess(Downlines[] downlines) {
        adapter = new DownlinesAdapter(downlines, mContext);
        mView.loadData(adapter, downlines.length);
        mView.hideLoader();
    }

    @Override
    public void onGetDownlinesFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
