package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.Downlines;

/**
 * Created by Raj on 05-08-2019.
 */

public interface FetchDownlinesInteractor {
    interface Callback {
        void onGetDownlinesSuccess(Downlines[] downlines);
        void onGetDownlinesFail(String errorMsg);
    }
}
