package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.ProductList;

/**
 * Created by Raj on 26-08-2019.
 */

public interface GetRelatedProductsInteractor {
    interface Callback {
        void onGettingRelatedProducts(ProductList[] productLists);
        void onGettingRelatedProductsFail();
    }
}
