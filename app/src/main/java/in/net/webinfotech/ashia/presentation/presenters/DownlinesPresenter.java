package in.net.webinfotech.ashia.presentation.presenters;

import in.net.webinfotech.ashia.presentation.ui.adapters.DownlinesAdapter;

/**
 * Created by Raj on 05-08-2019.
 */

public interface DownlinesPresenter {
    void fetchDownlines();
    interface View {
        void showLoader();
        void hideLoader();
        void loadData(DownlinesAdapter adapter, int count);
    }
}
