package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.CancelOrderInteractor;
import in.net.webinfotech.ashia.domain.interactors.FetchOrderHistoriesInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.CancelOrderInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.FetchOrderHistoriesInteractorImpl;
import in.net.webinfotech.ashia.domain.models.Orders;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.presentation.presenters.OrderListPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.ui.adapters.OrdersAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 21-08-2019.
 */

public class OrderListPresenterImpl extends AbstractPresenter implements OrderListPresenter, FetchOrderHistoriesInteractor.Callback, OrdersAdapter.Callback, CancelOrderInteractor.Callback {

    Context mContext;
    OrderListPresenter.View mView;
    FetchOrderHistoriesInteractorImpl fetchOrderHistoriesInteractor;
    AndroidApplication androidApplication;
    OrdersAdapter adapter;
    CancelOrderInteractorImpl cancelOrderInteractor;

    public OrderListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, OrderListPresenter.View view) {
        super(executor, mainThread);
        this.mContext = mContext;
        mView = view;
    }

    @Override
    public void fetchOrderHistory() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo !=  null) {
            fetchOrderHistoriesInteractor = new FetchOrderHistoriesInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            fetchOrderHistoriesInteractor.execute();
        }
    }

    @Override
    public void onGettingOrdersSuccess(Orders[] orders) {
        adapter = new OrdersAdapter(mContext, orders, this);
        mView.loadAdapter(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingOrdersFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onCancelClicked(String orderId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo !=  null) {
            cancelOrderInteractor = new CancelOrderInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, orderId);
            cancelOrderInteractor.execute();
            mView.showTaskLoader();
        }
    }

    @Override
    public void onItemClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onOrderCancelSuccess() {
        fetchOrderHistory();
        Toasty.success(mContext, "Cancelled", Toast.LENGTH_SHORT, true).show();
        mView.hideTaskLoader();
    }

    @Override
    public void onOrderCancelFailed(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideTaskLoader();
    }
}
