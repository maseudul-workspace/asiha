package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 25-07-2019.
 */

public class ProductImages {
    @SerializedName("img")
    @Expose
    public String img;
}
