package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.WalletAmount;

/**
 * Created by Raj on 10-08-2019.
 */

public interface GetWalletAmountInteractor {
    interface Callback {
        void onGettingWalletAmountSuccess(WalletAmount walletAmount);
        void onGettingWalletAmountFail(String errorMsg);
    }
}
