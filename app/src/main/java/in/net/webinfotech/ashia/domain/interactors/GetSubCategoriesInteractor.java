package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.SubCategoryList;

/**
 * Created by Raj on 15-07-2019.
 */

public interface GetSubCategoriesInteractor {
    interface Callback{
        void onGettingDataSuccess(SubCategoryList[] subCategoryLists);
        void onGettingDataFail(String errorMsg);
    }
}
