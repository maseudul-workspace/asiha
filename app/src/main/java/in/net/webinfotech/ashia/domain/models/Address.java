package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 02-08-2019.
 */

public class Address {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("postalCode")
    @Expose
    public String postalCode;

    @SerializedName("phoneNo")
    @Expose
    public String phoneNo;

    @SerializedName("landmark")
    @Expose
    public String landmark;

    public boolean isSelected = false;

}
