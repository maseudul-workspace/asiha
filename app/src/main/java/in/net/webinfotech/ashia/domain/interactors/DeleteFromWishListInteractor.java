package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 26-08-2019.
 */

public interface DeleteFromWishListInteractor {
    interface Callback {
        void onDeleteFromWishListSuccess();
        void onDeleteFromWishListFail();
    }
}
