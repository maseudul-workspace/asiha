package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.AddRecentViewInteractor;
import in.net.webinfotech.ashia.domain.interactors.AddToCartInteractor;
import in.net.webinfotech.ashia.domain.interactors.AddToWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.DeleteFromWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.FetchWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetCartDetailsInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetProductDetailsInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetRelatedProductsInteractor;
import in.net.webinfotech.ashia.domain.interactors.SubmitReviewInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.AddRecentViewInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.AddToCartInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.AddToWishListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.DeleteFromWishListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.FetchWishListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetCartDetailsInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetProductDetailsInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetRelatedProductsInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.SubmitReviewInteractorImpl;
import in.net.webinfotech.ashia.domain.models.CartDetails;
import in.net.webinfotech.ashia.domain.models.ProductDetails;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.presentation.presenters.ProductDetailsPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductDetailsImagesAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListHorizontalAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ReviewsAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 25-07-2019.
 */

public class ProductDetailsPresenterImpl extends AbstractPresenter implements ProductDetailsPresenter,
                                                                                GetProductDetailsInteractor.Callback,
                                                                                SubmitReviewInteractor.Callback,
                                                                                GetCartDetailsInteractor.Callback,
                                                                                AddToCartInteractor.Callback,
                                                                                AddRecentViewInteractor.Callback,
                                                                                AddToWishListInteractor.Callback,
                                                                                FetchWishListInteractor.Callback,
                                                                                DeleteFromWishListInteractor.Callback,
                                                                                GetRelatedProductsInteractor.Callback,
                                                                                ProductListHorizontalAdapter.Callback,
                                                                                ProductDetailsImagesAdapter.Callback
                                                                                {

    Context mContext;
    ProductDetailsPresenter.View mView;
    GetProductDetailsInteractorImpl getProductDetailsInteractor;
    SubmitReviewInteractorImpl submitReviewInteractor;
    AndroidApplication androidApplication;
    GetCartDetailsInteractorImpl getCartDetailsInteractor;
    int productId;
    AddToCartInteractorImpl addToCartInteractor;
    AddRecentViewInteractorImpl addRecentViewInteractor;
    AddToWishListInteractorImpl addToWishListInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;
    DeleteFromWishListInteractorImpl deleteFromWishListInteractor;
    GetRelatedProductsInteractorImpl relatedProductsInteractor;

    public ProductDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getProductDetails(int productId) {
        getProductDetailsInteractor = new GetProductDetailsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), productId);
        getProductDetailsInteractor.execute();
    }

    @Override
    public void getCartDetails(int productId) {
        this.productId = productId;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        getCartDetailsInteractor = new GetCartDetailsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
        getCartDetailsInteractor.execute();
    }

    @Override
    public void submitReview(int productId, int rating, String comment) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        submitReviewInteractor = new SubmitReviewInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.userId, productId, userInfo.apiToken, comment, rating);
        submitReviewInteractor.execute();
    }

    @Override
    public void addToCart(int productId, int quantity) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, quantity, productId);
        addToCartInteractor.execute();
    }

    @Override
    public void addRecentView(int productId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo != null) {
            addRecentViewInteractor = new AddRecentViewInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, productId);
            addRecentViewInteractor.execute();
        }
    }

    @Override
    public void addToWishList(int productId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, productId);
        addToWishListInteractor.execute();
    }

    @Override
    public void fetchWishList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
        fetchWishListInteractor.execute();
    }

    @Override
    public void deleteFromWishList(int productId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        deleteFromWishListInteractor = new DeleteFromWishListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, productId);
        deleteFromWishListInteractor.execute();
    }

    @Override
    public void fetchRelatedProducts(int brandId) {
        relatedProductsInteractor = new GetRelatedProductsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), brandId);
        relatedProductsInteractor.execute();
    }

    @Override
    public void onGettingProductDetailSuccess(ProductDetails productDetails) {
        mView.loadProductDetails(productDetails);
        ProductDetailsImagesAdapter imagesAdapter = new ProductDetailsImagesAdapter(mContext, productDetails.productImages, this);
        mView.loadProductImagesAdapter(imagesAdapter, productDetails.productImages.length);
        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(productDetails.reviews);
        mView.loadReviewsAdapter(reviewsAdapter);
        mView.hideLoader();
        addRecentView(productDetails.productId);
    }

    @Override
    public void onGettingProductDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onReviewSubmitSuccess() {
        mView.hideDialog();
        Toasty.success(mContext, "Successful!! Review sent for verification", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onReviewSubmitFail() {
        mView.hideDialog();
        Toasty.error(mContext, "Failed!! Something went wrong", Toast.LENGTH_SHORT, true).show();

    }

    @Override
    public void onGettingCartDetailsSuccess(CartDetails[] cartDetails) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setCartDetails(mContext, cartDetails);
        boolean isCartPresent = false;
        for(int i = 0; i < cartDetails.length; i++){
            if(cartDetails[i].productId == productId){
                isCartPresent = true;
                break;
            }
        }
        mView.loadCartCount(cartDetails.length, isCartPresent);
        getProductDetails(productId);
    }

    @Override
    public void onGettingCartDetailsFail() {
        getProductDetails(productId);
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setCartDetails(mContext, null);
    }

    @Override
    public void onAddToCartSuccess() {
        Toasty.success(mContext, "Added To Cart", Toast.LENGTH_SHORT, true).show();
        getCartDetails(productId);
        mView.onCartAddedFail();
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        getCartDetails(productId);
        mView.onCartAddedFail();
    }

    @Override
    public void onRecentAddSuccess() {

    }

    @Override
    public void onRecentAddFailed() {

    }

    @Override
    public void onWishListAddingSuccess() {
        Toasty.success(mContext, "Added to wishlist", Toast.LENGTH_SHORT, true).show();
        mView.onWishListAddSuccess();
        fetchWishList();
    }

    @Override
    public void onWishListAddingFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onGettingWishListSuccess(ProductList[] productLists) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, productLists);
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {

    }

    @Override
    public void onDeleteFromWishListSuccess() {
        Toasty.success(mContext, "Removed from wishlist", Toast.LENGTH_SHORT, true).show();
        mView.onWishListRemovedSuccess();
        fetchWishList();
    }

    @Override
    public void onDeleteFromWishListFail() {
        Toasty.error(mContext, "Something went wrong", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onGettingRelatedProducts(ProductList[] productLists) {
        ProductListHorizontalAdapter productListHorizontalAdapter = new ProductListHorizontalAdapter(mContext, productLists, this);
        mView.loadRelatedProducts(productListHorizontalAdapter);
    }

    @Override
    public void onGettingRelatedProductsFail() {

    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onImageClicked(String image) {
        mView.goToPreviewImage(image);
    }
}
