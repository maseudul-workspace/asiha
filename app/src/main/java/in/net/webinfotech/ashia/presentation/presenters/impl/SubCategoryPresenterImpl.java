package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetSubCategoriesInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.GetSubCategoriesInteractorImpl;
import in.net.webinfotech.ashia.domain.models.SubCategoryList;
import in.net.webinfotech.ashia.presentation.presenters.SubCategoryPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.routers.SubCategoryRouter;
import in.net.webinfotech.ashia.presentation.ui.adapters.SubcategoryGridAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 15-07-2019.
 */

public class SubCategoryPresenterImpl extends AbstractPresenter implements SubCategoryPresenter, GetSubCategoriesInteractor.Callback, SubcategoryGridAdapter.Callback {

    Context mContext;
    SubCategoryPresenter.View mView;
    GetSubCategoriesInteractorImpl mInteractor;
    SubCategoryRouter mRouter;

    public SubCategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView, SubCategoryRouter router) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
        mRouter = router;
    }

    @Override
    public void getSubCategoriesList(int id) {
       mInteractor = new GetSubCategoriesInteractorImpl(mExecutor, mMainThread, new OtherRepositoryImpl(), id, this);
       mInteractor.execute();
    }


    @Override
    public void onGettingDataSuccess(SubCategoryList[] subCategoryLists) {
        SubcategoryGridAdapter adapter = new SubcategoryGridAdapter(mContext, subCategoryLists, this);
        mView.loadAdapter(adapter);
    }

    @Override
    public void onGettingDataFail(String errorMsg) {

    }

    @Override
    public void onSubCategoryClicked(int id) {
        mRouter.goToProductListActivity(id);
    }
}
