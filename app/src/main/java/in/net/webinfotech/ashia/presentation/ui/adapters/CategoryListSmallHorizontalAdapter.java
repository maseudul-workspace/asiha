package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.CategoryList;
import in.net.webinfotech.ashia.util.GlideHelper;

/**
 * Created by Raj on 08-07-2019.
 */

public class CategoryListSmallHorizontalAdapter extends RecyclerView.Adapter<CategoryListSmallHorizontalAdapter.ViewHolder> {

    public interface Callback{
        void onCategoryClicked(ImageView imageView, int categoryId, String categoryName, String categoryImage);
    }

    Context mContext;
    CategoryList[] categoryLists;
    Callback mCallback;

    public CategoryListSmallHorizontalAdapter(Context mContext, CategoryList[] categories, Callback callback) {
        this.mContext = mContext;
        this.categoryLists = categories;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_category_small_horizontal, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgCategoryIcon, categoryLists[position].imgPath, 100);
        holder.txtViewCategoryName.setText(categoryLists[position].categoryName);
        holder.imgCategoryIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCategoryClicked(holder.imgCategoryIcon, categoryLists[position].id, categoryLists[position].categoryName, categoryLists[position].imgPath);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_category_icon)
        ImageView imgCategoryIcon;
        @BindView(R.id.txt_view_category_name)
        TextView txtViewCategoryName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
