package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.FetchRankInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.Ranking;
import in.net.webinfotech.ashia.domain.models.RankingWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 27-08-2019.
 */

public class FetchRankInteractorImpl extends AbstractInteractor implements FetchRankInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;

    public FetchRankInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingRankFail();
            }
        });
    }

    private void postMessage(final Ranking ranking){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingRankSuccess(ranking);
            }
        });
    }

    @Override
    public void run() {
        final RankingWrapper rankingWrapper = mRepository.fetchRank(apiKey, userId);
        if (rankingWrapper == null) {
            notifyError();
        } else if (!rankingWrapper.status) {
            notifyError();
        } else {
            postMessage(rankingWrapper.ranking);
        }
    }
}
