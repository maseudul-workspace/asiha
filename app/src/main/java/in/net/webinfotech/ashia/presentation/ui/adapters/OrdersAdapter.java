package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.Orders;

/**
 * Created by Raj on 20-08-2019.
 */

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> implements OrdersProductsListAdapter.Callback {

    public interface Callback {
        void onCancelClicked(String orderId);
        void onItemClicked(int productId);
    }

    Context mContext;
    Orders[] orders;
    Callback mCallback;

    public OrdersAdapter(Context mContext, Orders[] orders, Callback callback) {
        this.mContext = mContext;
        this.orders = orders;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_order_list, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewOrderId.setText(orders[i].orderId);
        viewHolder.txtViewOrderTotalPrice.setText("Rs. " + orders[i].totalAmt);
        if (orders[i].orderStatus == 0) {
            viewHolder.txtViewOrderStatus.setText("Order Placed");
            viewHolder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.blue2));
            viewHolder.layoutBlue.setVisibility(View.VISIBLE);
            viewHolder.layoutGreen.setVisibility(View.GONE);
            viewHolder.layoutRed.setVisibility(View.GONE);
            viewHolder.txtViewOrderCancel.setVisibility(View.VISIBLE);
            viewHolder.txtViewOrderCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Confirmation dialog");
                    builder.setMessage("You are about cancel a order. Do you really want to proceed ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCallback.onCancelClicked(orders[i].orderId);
                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();
                }
            });
        } else if (orders[i].orderStatus == 1) {
            viewHolder.txtViewOrderStatus.setText("Out For Delivery");
            viewHolder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.blue2));
            viewHolder.layoutBlue.setVisibility(View.VISIBLE);
            viewHolder.layoutGreen.setVisibility(View.GONE);
            viewHolder.layoutRed.setVisibility(View.GONE);
            viewHolder.txtViewOrderCancel.setVisibility(View.GONE);
        } else if (orders[i].orderStatus == 2) {
            viewHolder.txtViewOrderStatus.setText("Delivered");
            viewHolder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.green2));
            viewHolder.layoutBlue.setVisibility(View.GONE);
            viewHolder.layoutGreen.setVisibility(View.VISIBLE);
            viewHolder.layoutRed.setVisibility(View.GONE);
            viewHolder.txtViewOrderCancel.setVisibility(View.GONE);
        } else {
            viewHolder.txtViewOrderStatus.setText("Cancelled");
            viewHolder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.red2));
            viewHolder.layoutBlue.setVisibility(View.GONE);
            viewHolder.layoutGreen.setVisibility(View.GONE);
            viewHolder.layoutRed.setVisibility(View.VISIBLE);
            viewHolder.txtViewOrderCancel.setVisibility(View.GONE);
        }

        if(orders[i].walletPayStatus == 1) {
            viewHolder.layoutWalletPay.setVisibility(View.VISIBLE);
            viewHolder.txtViewWalletAmount.setText("Rs. " + orders[i].walletAmount);
        } else {
            viewHolder.layoutWalletPay.setVisibility(View.GONE);
        }

        viewHolder.txtViewTotalItems.setText(Integer.toString(orders[i].orderProducts.length));

        OrdersProductsListAdapter adapter = new OrdersProductsListAdapter(mContext, orders[i].orderProducts, this);
        viewHolder.recyclerViewOrderProducts.setAdapter(adapter);
        viewHolder.recyclerViewOrderProducts.setLayoutManager(new LinearLayoutManager(mContext));
        viewHolder.recyclerViewOrderProducts.setNestedScrollingEnabled(false);
        viewHolder.txtViewDate.setText(orders[i].orderDate);
    }

    @Override
    public int getItemCount() {
        return orders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_order_id)
        TextView txtViewOrderId;
        @BindView(R.id.txt_view_cancel_order)
        TextView txtViewOrderCancel;
        @BindView(R.id.txt_view_order_total_price)
        TextView txtViewOrderTotalPrice;
        @BindView(R.id.txt_view_order_status)
        TextView txtViewOrderStatus;
        @BindView(R.id.layout_blue)
        View layoutBlue;
        @BindView(R.id.layout_green)
        View layoutGreen;
        @BindView(R.id.layout_red)
        View layoutRed;
        @BindView(R.id.recycler_view_orders_products)
        RecyclerView recyclerViewOrderProducts;
        @BindView(R.id.txt_view_total_item)
        TextView txtViewTotalItems;
        @BindView(R.id.layout_wallet_pay)
        View layoutWalletPay;
        @BindView(R.id.txt_view_wallet_amount)
        TextView txtViewWalletAmount;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onItemClicked(int productId) {
        mCallback.onItemClicked(productId);
    }

}
