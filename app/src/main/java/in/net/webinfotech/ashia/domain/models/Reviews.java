package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 25-07-2019.
 */

public class Reviews {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("userId")
    @Expose
    public String userId;

    @SerializedName("userName")
    @Expose
    public String userName;

    @SerializedName("productId")
    @Expose
    public String productId;

    @SerializedName("ratingVal")
    @Expose
    public int rating;

    @SerializedName("comment")
    @Expose
    public String comment;

    @SerializedName("date")
    @Expose
    public String date;

}
