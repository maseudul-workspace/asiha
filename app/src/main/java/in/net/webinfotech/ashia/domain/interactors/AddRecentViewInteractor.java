package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 23-08-2019.
 */

public interface AddRecentViewInteractor {
    interface Callback {
        void onRecentAddSuccess();
        void onRecentAddFailed();
    }
}
