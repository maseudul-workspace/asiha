package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.Address;

/**
 * Created by Raj on 02-08-2019.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    public interface Callback {
        void onDeleteClicked(int addressId);
        void onEditClicked(Address address);
        void layoutClicked(int selectedId);
    }

    Context mContext;
    Address[] addresses;
    Callback mCallback;

    public AddressAdapter(Context mContext, Address[] addresses, Callback callback) {
        this.mContext = mContext;
        this.addresses = addresses;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_addresses, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.txtViewState.setText(addresses[i].state);
        viewHolder.txtViewCity.setText(addresses[i].city);
        viewHolder.txtViewPostalCode.setText(addresses[i].postalCode);
        viewHolder.txtViewLandmark.setText(addresses[i].landmark);
        viewHolder.txtViewPhoneNo.setText(addresses[i].phoneNo);
        viewHolder.imgViewDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewHolder.mainLayout.getVisibility() == View.VISIBLE){
                    viewHolder.mainLayout.setVisibility(View.GONE);
                } else {
                    viewHolder.mainLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        int index = i + 1;
        viewHolder.txtViewAddressLine.setText("Address Line " + index);
        viewHolder.imgViewDeleteAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirmation dialog");
                builder.setMessage("You are about to delete a address. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(addresses[i].id);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.show();
            }
        });
        viewHolder.imgViewEditAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(addresses[i]);
            }
        });
        viewHolder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.layoutClicked(addresses[i].id);
            }
        });
        if(addresses[i].isSelected) {
            viewHolder.imgViewSelectedAddress.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgViewSelectedAddress.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return addresses.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_state)
        TextView txtViewState;
        @BindView(R.id.txt_view_city)
        TextView txtViewCity;
        @BindView(R.id.txt_view_postal_code)
        TextView txtViewPostalCode;
        @BindView(R.id.txt_view_landmark)
        TextView txtViewLandmark;
        @BindView(R.id.txt_view_phone_no)
        TextView txtViewPhoneNo;
        @BindView(R.id.img_view_dropdown)
        ImageView imgViewDropDown;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_address_line)
        TextView txtViewAddressLine;
        @BindView(R.id.img_edit_address)
        ImageView imgViewEditAddress;
        @BindView(R.id.img_delete_address)
        ImageView imgViewDeleteAddress;
        @BindView(R.id.layout_main)
        View layoutMain;
        @BindView(R.id.img_view_selected_address)
        ImageView imgViewSelectedAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(Address[] addresses) {
        this.addresses = addresses;
        notifyDataSetChanged();
    }

}
