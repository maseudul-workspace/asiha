package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetCartDetailsInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.CartDetails;
import in.net.webinfotech.ashia.domain.models.CartDetailsWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 31-07-2019.
 */

public class GetCartDetailsInteractorImpl extends AbstractInteractor implements GetCartDetailsInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;

    public GetCartDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartDetailsFail();
            }
        });
    }

    private void postMessage(final CartDetails[] cartDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartDetailsSuccess(cartDetails);
            }
        });
    }

    @Override
    public void run() {
        final CartDetailsWrapper cartDetailsWrapper = mRepository.getCartDetails(apiToken, userId);
        if(cartDetailsWrapper == null){
            notifyError();
        }else if(!cartDetailsWrapper.status){
            notifyError();
        }else{
            postMessage(cartDetailsWrapper.cartDetails);
        }
    }
}
