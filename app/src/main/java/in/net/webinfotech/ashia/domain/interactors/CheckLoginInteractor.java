package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.UserInfo;

/**
 * Created by Raj on 22-07-2019.
 */

public interface CheckLoginInteractor {
    interface Callback{
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
