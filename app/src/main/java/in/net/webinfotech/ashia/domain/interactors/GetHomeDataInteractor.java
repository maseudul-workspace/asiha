package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.HomeData;

/**
 * Created by Raj on 13-07-2019.
 */

public interface GetHomeDataInteractor {
    interface Callback{
        void onDataGettingSuccess(HomeData homeData);
        void onDataGettingFail(String errorMsg);
    }
}
