package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 27-08-2019.
 */

public class Ranking {
    @SerializedName("rankingId")
    @Expose
    public int rankingId;

    @SerializedName("rank")
    @Expose
    public String rank;
}
