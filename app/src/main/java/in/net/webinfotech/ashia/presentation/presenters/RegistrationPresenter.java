package in.net.webinfotech.ashia.presentation.presenters;

import in.net.webinfotech.ashia.presentation.ui.adapters.PlansAdapter;

/**
 * Created by Raj on 30-07-2019.
 */

public interface RegistrationPresenter {
    void addDownline(String membersName, String membersEmail, String memberPhone, String address, String password);
    interface View{
        void showLoader();
        void hideLoader();
        void onRegistrationSuccess();
    }
}
