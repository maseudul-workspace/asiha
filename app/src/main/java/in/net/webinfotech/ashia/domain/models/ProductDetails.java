package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 25-07-2019.
 */

public class ProductDetails {

    @SerializedName("sub_cate_id")
    @Expose
    public String subCategoryId;

    @SerializedName("brands_id")
    @Expose
    public int brandId;

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("pro_name")
    @Expose
    public String productName;

    @SerializedName("pro_desc")
    @Expose
    public String productDescription;

    @SerializedName("price")
    @Expose
    public Double price;

    @SerializedName("selling_price")
    @Expose
    public Double sellingPrice;

    @SerializedName("rating")
    @Expose
    public int rating;

    @SerializedName("reviews")
    @Expose
    public Reviews[] reviews;

    @SerializedName("images")
    @Expose
    public ProductImages[] productImages;

    @SerializedName("stock")
    @Expose
    public int stock;

}
