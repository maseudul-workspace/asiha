package in.net.webinfotech.ashia.presentation.routers;

/**
 * Created by Raj on 16-07-2019.
 */

public interface SubCategoryRouter {
   void goToProductListActivity(int id);
}
