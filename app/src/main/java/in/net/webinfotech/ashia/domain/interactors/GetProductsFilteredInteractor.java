package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.ProductList;

/**
 * Created by Raj on 24-07-2019.
 */

public interface GetProductsFilteredInteractor {
    interface Callback{
        void onGettingProductsFilteredSuccess(ProductList[] productLists, int totalPage);
        void onGettingProductsFilteredFail(String errorMsg);
    }
}
