package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.OrderProducts;
import in.net.webinfotech.ashia.util.GlideHelper;

/**
 * Created by Raj on 20-08-2019.
 */

public class OrdersProductsListAdapter extends RecyclerView.Adapter<OrdersProductsListAdapter.ViewHolder> {

    interface Callback {
        void onItemClicked(int productId);
    }

    Context mContext;
    OrderProducts[] orderProducts;
    Callback mCallback;

    public OrdersProductsListAdapter(Context mContext, OrderProducts[] products, Callback callback) {
        this.mContext = mContext;
        this.orderProducts = products;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_order_list_products, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewProductName.setText(orderProducts[i].productName);
        viewHolder.txtViewProductBrand.setText(orderProducts[i].brand);
        viewHolder.txtViewProductPrice.setText("Rs. " + orderProducts[i].price);
        GlideHelper.setImageView(mContext, viewHolder.imgViewProductImage, orderProducts[i].url);
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onItemClicked(orderProducts[i].productId);
            }
        });
        viewHolder.txtViewQuantity.setText(orderProducts[i].quantity);
    }

    @Override
    public int getItemCount() {
        return orderProducts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.img_view_product_image)
        ImageView imgViewProductImage;
        @BindView(R.id.txt_view_product_brand)
        TextView txtViewProductBrand;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_quantity)
        TextView txtViewQuantity;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
