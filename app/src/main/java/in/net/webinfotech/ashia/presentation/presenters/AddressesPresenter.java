package in.net.webinfotech.ashia.presentation.presenters;

import in.net.webinfotech.ashia.domain.models.Address;
import in.net.webinfotech.ashia.presentation.ui.adapters.AddressAdapter;

/**
 * Created by Raj on 02-08-2019.
 */

public interface AddressesPresenter {
    void fetchAddress();
    void addAddress(String state, String city, String phoneNo, String postalCode, String landmark);
    void editAddress(String state, String city, String phoneNo, String postalCode, String landmark, int addressId);
    interface View {
        void showLoader();
        void hideLooader();
        void loadAddressAdapter(AddressAdapter adapter);
        void onAddAddressSuccess();
        void onAddressFailed();
        void onAddressEditClicked(Address address);
    }
}
