package in.net.webinfotech.ashia.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.Address;

/**
 * Created by Raj on 14-08-2019.
 */

public class OrderConfirmDialog {

    public interface Callback {
        void onEditAddressClicked();
        void onConfirmOrderClicked();
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    TextView txtViewState;
    TextView txtViewCity;
    TextView txtViewPin;
    TextView txtViewPhoneNo;
    TextView txtViewLandmark;
    TextView txtViewTotalPrice;
    TextView txtViewEditAddress;
    Button btnConfirmOrder;
    View layoutLoader;
    Callback mCallback;

    public OrderConfirmDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.order_confirm_dialog, null);
        txtViewTotalPrice = (TextView) dialogContainer.findViewById(R.id.txt_view_total_price);
        txtViewState = (TextView) dialogContainer.findViewById(R.id.txt_view_state);
        txtViewCity = (TextView) dialogContainer.findViewById(R.id.txt_view_city);
        txtViewPhoneNo = (TextView) dialogContainer.findViewById(R.id.txt_view_phone_no);
        txtViewPin = (TextView) dialogContainer.findViewById(R.id.txt_view_postal_code);
        txtViewLandmark = (TextView) dialogContainer.findViewById(R.id.txt_view_landmark);
        txtViewEditAddress = (TextView) dialogContainer.findViewById(R.id.txt_view_edit_address);
        layoutLoader = (View) dialogContainer.findViewById(R.id.layout_loader);
        btnConfirmOrder = (Button) dialogContainer.findViewById(R.id.btn_confirm_order);
        btnConfirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onConfirmOrderClicked();
                layoutLoader.setVisibility(View.VISIBLE);
            }
        });
        txtViewEditAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditAddressClicked();
            }
        });
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void showDialog(Address address, double totalPrice) {
        txtViewTotalPrice.setText("Rs. " + totalPrice);
        txtViewState.setText(address.state);
        txtViewCity.setText(address.city);
        txtViewLandmark.setText(address.landmark);
        txtViewPin.setText(address.postalCode);
        txtViewPhoneNo.setText(address.phoneNo);
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

}
