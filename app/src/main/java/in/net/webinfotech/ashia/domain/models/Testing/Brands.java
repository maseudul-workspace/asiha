package in.net.webinfotech.ashia.domain.models.Testing;

/**
 * Created by Raj on 22-07-2019.
 */

public class Brands {
    public String brandName;
    public Boolean isChecked;
    public Brands(String brandName, Boolean isChecked) {
        this.brandName = brandName;
        this.isChecked = isChecked;
    }
}
