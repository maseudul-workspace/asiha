package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetProductDetailsInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.ProductDetails;
import in.net.webinfotech.ashia.domain.models.ProductDetailsWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 25-07-2019.
 */

public class GetProductDetailsInteractorImpl extends AbstractInteractor implements GetProductDetailsInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    int productId;

    public GetProductDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, int productId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.productId = productId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final ProductDetails productDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailSuccess(productDetails);
            }
        });
    }

    @Override
    public void run() {
        final ProductDetailsWrapper productDetailsWrapper = mRepository.getProductDetails(productId);
        if(productDetailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!productDetailsWrapper.status){
            notifyError("Something went wrong");
        }else{
            postMessage(productDetailsWrapper.productDetails);
        }
    }
}
