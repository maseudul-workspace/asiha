package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetRelatedProductsInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.ProductListWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 26-08-2019.
 */

public class GetRelatedProductsInteractorImpl extends AbstractInteractor implements GetRelatedProductsInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    int brandId;

    public GetRelatedProductsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, int brandId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.brandId = brandId;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingRelatedProductsFail();
            }
        });
    }

    private void postMessage(final ProductList[] productLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingRelatedProducts(productLists);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper productListWrapper = mRepository.getRelatedProducts(brandId);
        if(productListWrapper == null) {
            notifyError();
        } else if (!productListWrapper.status) {
            notifyError();
        } else {
            postMessage(productListWrapper.productLists);
        }
    }
}
