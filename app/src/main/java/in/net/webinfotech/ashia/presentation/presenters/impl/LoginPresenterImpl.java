package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.CheckLoginInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.CheckLoginInteractorImpl;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.presentation.presenters.LoginPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.ui.router.LoginRouter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 22-07-2019.
 */

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;
    CheckLoginInteractorImpl mInteractor;
    LoginRouter mRouter;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView, LoginRouter router) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
        mRouter = router;
    }

    @Override
    public void checkLogin(String phone, String password) {
          mInteractor = new CheckLoginInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), phone, password);
          mInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        mView.hideLoader();
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        Toasty.success(mContext, "Login Successful", Toast.LENGTH_SHORT).show();
        mRouter.goToLogin();
    }

    @Override
    public void onLoginFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
