package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.AddToCartInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.AddToCartResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 01-08-2019.
 */

public class AddToCartInteractorImpl extends AbstractInteractor implements AddToCartInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;
    int quantity;
    int productId;

    public AddToCartInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId, int quantity, int productId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.quantity = quantity;
        this.productId = productId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartSuccess();
            }
        });
    }

    @Override
    public void run() {
        final AddToCartResponse addToCartResponse = mRepository.addToCart(apiToken, userId, productId, quantity);
        if(addToCartResponse == null){
            notifyError("Something went wrong");
        } else if(!addToCartResponse.status) {
            notifyError("Something went wrong");
        } else {
            postMessage();
        }
    }
}
