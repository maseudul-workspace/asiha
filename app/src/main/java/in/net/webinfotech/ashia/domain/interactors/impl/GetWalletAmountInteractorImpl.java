package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetWalletAmountInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.WalletAmount;
import in.net.webinfotech.ashia.domain.models.WalletAmountWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 10-08-2019.
 */

public class GetWalletAmountInteractorImpl extends AbstractInteractor implements GetWalletAmountInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;

    public GetWalletAmountInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletAmountFail(errorMsg);
            }
        });
    }

    private void postMessage(final WalletAmount walletAmount){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletAmountSuccess(walletAmount);
            }
        });
    }

    @Override
    public void run() {
        final WalletAmountWrapper walletAmountWrapper = mRepository.fetchWalletAmount(apiToken, userId);
        if (walletAmountWrapper ==  null) {
            notifyError("Something went wrong");
        } else if(!walletAmountWrapper.status) {
            notifyError("Something went wrong");
        } else {
            postMessage(walletAmountWrapper.walletAmount);
        }
    }
}
