package in.net.webinfotech.ashia.presentation.presenters;

import in.net.webinfotech.ashia.presentation.ui.adapters.OrdersAdapter;

/**
 * Created by Raj on 21-08-2019.
 */

public interface OrderListPresenter {
    void fetchOrderHistory();
    interface View {
        void loadAdapter(OrdersAdapter adapter);
        void showLoader();
        void hideLoader();
        void showTaskLoader();
        void hideTaskLoader();
        void goToProductDetails(int productId);
    }
}
