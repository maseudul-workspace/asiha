package in.net.webinfotech.ashia.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.presentation.presenters.DownlinesPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.DownlinesPresenterImpl;
import in.net.webinfotech.ashia.presentation.ui.adapters.DownlinesAdapter;
import in.net.webinfotech.ashia.threading.MainThreadImpl;

public class DownlineActivity extends AppCompatActivity implements DownlinesPresenter.View {

    @BindView(R.id.recycler_view_downlines)
    RecyclerView recyclerViewDownlines;
    @BindView(R.id.txt_view_downline_count)
    TextView txtViewDownlineCount;
    @BindView(R.id.layoutLoader)
    View layoutLoader;
    @BindView(R.id.main_layout)
    View mainLayout;
    DownlinesPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downline);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("My Downlines");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        mPresenter.fetchDownlines();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new DownlinesPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void loadData(DownlinesAdapter adapter, int count) {
        mainLayout.setVisibility(View.VISIBLE);
        recyclerViewDownlines.setAdapter(adapter);
        recyclerViewDownlines.setLayoutManager(new LinearLayoutManager(this));
        txtViewDownlineCount.setText("TOTAL DOWNLNES: " + Integer.toString(count));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
