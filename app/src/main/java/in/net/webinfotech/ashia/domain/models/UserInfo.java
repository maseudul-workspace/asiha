package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-07-2019.
 */

public class UserInfo {
    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("api_token")
    @Expose
    public String apiToken;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

}
