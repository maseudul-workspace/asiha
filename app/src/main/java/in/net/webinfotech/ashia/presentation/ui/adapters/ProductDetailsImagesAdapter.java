package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import in.net.webinfotech.ashia.domain.models.ProductImages;
import in.net.webinfotech.ashia.util.GlideHelper;

/**
 * Created by Raj on 13-07-2019.
 */

public class ProductDetailsImagesAdapter extends PagerAdapter {

    public interface Callback {
        void onImageClicked(String image);
    }

    Context mContext;
    ProductImages[] productImages;
    Callback mCallback;

    public ProductDetailsImagesAdapter(Context mContext, ProductImages[] productImages, Callback callback) {
        this.mContext = mContext;
        this.productImages = productImages;
        mCallback = callback;
    }

    @Override
    public int getCount() {
        return productImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideHelper.setImageView(mContext, imageView, productImages[position].img);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onImageClicked(productImages[position].img);
            }
        });
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
