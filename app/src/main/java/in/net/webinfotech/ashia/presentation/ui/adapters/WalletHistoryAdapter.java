package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.WalletHistory;

/**
 * Created by Raj on 10-08-2019.
 */

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.ViewHolder>{

    Context mContext;
    WalletHistory[] walletHistories;

    public WalletHistoryAdapter(Context mContext, WalletHistory[] walletHistories) {
        this.mContext = mContext;
        this.walletHistories = walletHistories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_wallet_history, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if(walletHistories[i].transactionType == 2) {
            viewHolder.viewIndiator.setBackgroundColor(mContext.getResources().getColor(R.color.green2));
            viewHolder.txtViewWalletAmount.setText("+ Rs." + walletHistories[i].amount);
            viewHolder.txtViewWalletAmount.setTextColor(mContext.getResources().getColor(R.color.green2));
        } else {
            viewHolder.viewIndiator.setBackgroundColor(mContext.getResources().getColor(R.color.red2));
            viewHolder.txtViewWalletAmount.setText("- Rs." + walletHistories[i].amount);
            viewHolder.txtViewWalletAmount.setTextColor(mContext.getResources().getColor(R.color.red2));
        }
        viewHolder.txtViewWalletMessage.setText(walletHistories[i].comment);
        viewHolder.txtViewWalletDate.setText(walletHistories[i].date);
        viewHolder.txtViewTotalWalletAmount.setText(walletHistories[i].totalAmount);
    }

    @Override
    public int getItemCount() {
        return walletHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

       @BindView(R.id.txt_view_wallet_amount)
       TextView txtViewWalletAmount;
       @BindView(R.id.txt_view_total_wallet_amount)
       TextView txtViewTotalWalletAmount;
       @BindView(R.id.txt_view_wallet_message)
       TextView txtViewWalletMessage;
       @BindView(R.id.txt_view_wallet_date)
       TextView txtViewWalletDate;
       @BindView(R.id.view_indicator)
       View viewIndiator;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
