package in.net.webinfotech.ashia.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.presentation.ui.adapters.BrandsAdapter;

/**
 * Created by Raj on 18-07-2019.
 */

public class FilterByDialog implements BrandsAdapter.Callback {

    public interface Callback{
        void onFilterApplyBtnClicked(int minAmt, int maxAmt);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    RecyclerView recyclerView;
    ImageView imgViewCancel;
    Button btnApply;
    Callback mCallback;
    EditText editTextMinPrice;
    EditText editTextMaxPrice;
    int minPrice;
    int maxPrice;

    public FilterByDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialogView(){
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.filter_by_dialog_layout, null);
        recyclerView = (RecyclerView) dialogContainer.findViewById(R.id.recycler_view_brands);
        imgViewCancel = (ImageView) dialogContainer.findViewById(R.id.img_view_cancel);
        btnApply = (Button) dialogContainer.findViewById(R.id.btn_apply);
        editTextMinPrice = (EditText) dialogContainer.findViewById(R.id.edit_text_min_price);
        editTextMaxPrice = (EditText) dialogContainer.findViewById(R.id.edit_text_max_price);
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextMinPrice.getText().toString().trim().isEmpty() || editTextMaxPrice.getText().toString().trim().isEmpty()){
                    minPrice = 0;
                    maxPrice = 0;
                }else{
                    minPrice = Integer.parseInt(editTextMinPrice.getText().toString());
                    maxPrice = Integer.parseInt(editTextMaxPrice.getText().toString());
                }
                if(minPrice > maxPrice){
                    Toasty.warning(mContext, "Minimum price should be less", Toast.LENGTH_SHORT, true).show();
                }else{
                    dialog.dismiss();
                    mCallback.onFilterApplyBtnClicked(minPrice, maxPrice);
                }
            }
        });
        imgViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void setAdapter(BrandsAdapter brandsAdapter){

        recyclerView.setAdapter(brandsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

    }

    public void showDialog(){
        dialog.show();
    }

    @Override
    public void onBrandsSelected(int position) {

    }
}
