package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.CartDetails;

/**
 * Created by Raj on 31-07-2019.
 */

public interface GetCartDetailsInteractor {
    interface Callback{
        void onGettingCartDetailsSuccess(CartDetails[] cartDetails);
        void onGettingCartDetailsFail();
    }
}
