package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.ProductList;

/**
 * Created by Raj on 22-08-2019.
 */

public interface FetchRecentlyViewedInteractor {
    interface Callback {
        void onGettingRecentlyViewedSuccess(ProductList[] productLists);
        void onGettingRecentlyViewedFail(String errorMsg);
    }
}
