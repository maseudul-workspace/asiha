package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.Downlines;

/**
 * Created by Raj on 05-08-2019.
 */

public class DownlinesAdapter extends RecyclerView.Adapter<DownlinesAdapter.ViewHolder> {

    Downlines[] downlines;
    Context mContext;

    public DownlinesAdapter(Downlines[] downlines, Context mContext) {
        this.downlines = downlines;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_downlines, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewDownlineName.setText(downlines[i].userName);
        viewHolder.txtViewDownlineAddress.setText(downlines[i].userAdd);
        viewHolder.txtViewDownlineEmail.setText(downlines[i].userEmail);
        viewHolder.txtViewDownlinePhoneNo.setText(downlines[i].userMobileNo);
    }

    @Override
    public int getItemCount() {
        return downlines.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_downline_name)
        TextView txtViewDownlineName;
        @BindView(R.id.txt_view_downline_address)
        TextView txtViewDownlineAddress;
        @BindView(R.id.txt_view_downline_phone_no)
        TextView txtViewDownlinePhoneNo;
        @BindView(R.id.txt_view_downline_email)
        TextView txtViewDownlineEmail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
