package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.DeleteFromWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.DeleteFromWishListResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 26-08-2019.
 */

public class DeleteFromWishListInteractorImpl extends AbstractInteractor implements DeleteFromWishListInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepsitory;
    String apiToken;
    int userId;
    int productId;

    public DeleteFromWishListInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepsitory, String apiToken, int userId, int productId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepsitory = mRepsitory;
        this.apiToken = apiToken;
        this.userId = userId;
        this.productId = productId;
    }


    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDeleteFromWishListFail();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDeleteFromWishListSuccess();
            }
        });
    }

    @Override
    public void run() {
        final DeleteFromWishListResponse deleteFromWishListResponse = mRepsitory.deleteFromWishList(apiToken, userId, productId);
        if (deleteFromWishListResponse == null) {
            notifyError();
        } else if (!deleteFromWishListResponse.status) {
            notifyError();
        } else {
            postMessage();
        }
    }
}
