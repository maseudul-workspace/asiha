package in.net.webinfotech.ashia.presentation.ui.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.Plan;

/**
 * Created by Raj on 30-07-2019.
 */

public class PlansAdapter extends RecyclerView.Adapter<PlansAdapter.ViewHolder> {

    public interface Callback{
        void onPlanClicked(String text, int id);
    }

    Plan[] plans;
    Callback mCallback;

    public PlansAdapter(Plan[] plans, Callback mCallback) {
        this.plans = plans;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_plans, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.txtViewPlan.setText(plans[i].planName + ": Amount Rs." + plans[i].planAmount + ", Commission Rs." + plans[i].planComm);
        viewHolder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onPlanClicked(viewHolder.txtViewPlan.getText().toString(), plans[i].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return plans.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_plan)
        TextView txtViewPlan;
        @BindView(R.id.layout_main)
        View layoutMain;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
