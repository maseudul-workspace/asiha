package in.net.webinfotech.ashia.presentation.presenters;

import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListVerticalAdapter;

/**
 * Created by Raj on 26-08-2019.
 */

public interface WishListPresenter {
    void fetchWishList();
    void fetchCartDetails();
    interface View {
        void showLoader();
        void hideLoader();
        void loadData(ProductListVerticalAdapter adapter);
        void goToProductDetails(int productId);
    }
}
