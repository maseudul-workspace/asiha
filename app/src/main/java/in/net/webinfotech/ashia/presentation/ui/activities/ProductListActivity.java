package in.net.webinfotech.ashia.presentation.ui.activities;

import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.presentation.presenters.ProductListPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.ProductListPresenterImpl;
import in.net.webinfotech.ashia.presentation.routers.ProductListRouter;
import in.net.webinfotech.ashia.presentation.ui.adapters.BrandsAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListVerticalAdapter;
import in.net.webinfotech.ashia.presentation.ui.dialogs.FilterByDialog;
import in.net.webinfotech.ashia.presentation.ui.dialogs.SortByDialog;
import in.net.webinfotech.ashia.threading.MainThreadImpl;

public class ProductListActivity extends AppCompatActivity implements ProductListPresenter.View, FilterByDialog.Callback, SortByDialog.Callback, ProductListRouter {

    @BindView(R.id.recycler_view_product_list_vertical)
    RecyclerView recyclerViewProductListVertical;
    @BindView(R.id.layoutLoader)
    View layoutLoader;
    @BindView(R.id.paginationProgressBar)
    View paginationProgressBar;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.relative_layout)
    RelativeLayout relativeLayout;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;
    ProductListPresenterImpl mPresenter;
    int subCategoryId;
    SortByDialog sortByDialog;
    FilterByDialog filterByDialog;
    int sort = 1;
    int minPrice = 0;
    int maxPrice = 0;
    boolean isFirstLoad = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        initialisePresenter();
        initialiseSortByDialog();
        initialiseFilterByDialog();
        sortByDialog.setUpDialogView();
        filterByDialog.setUpDialogView();
        subCategoryId = getIntent().getIntExtra("subCategoryId", 1);
        mPresenter.getBrands(subCategoryId);
        getSupportActionBar().setTitle("Product List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter(){
        mPresenter = new ProductListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    public void initialiseSortByDialog(){
        sortByDialog = new SortByDialog(this, this, this);
    }

    public void initialiseFilterByDialog(){
        filterByDialog = new FilterByDialog(this, this, this);
    }

    @Override
    public void loadProductListAdapter(ProductListVerticalAdapter adapter, int totalPages) {
        totalPage = totalPages;
        mainLayout.setVisibility(View.VISIBLE);
        layoutManager = new GridLayoutManager(this, 2);
        recyclerViewProductListVertical.setAdapter(adapter);
        recyclerViewProductListVertical.setLayoutManager(layoutManager);
        recyclerViewProductListVertical.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressBar();
                        if(isFirstLoad){
                            mPresenter.getProductList(subCategoryId, pageNo, "");
                        }else{
                            mPresenter.getProductsFiltered(subCategoryId, sort, minPrice, maxPrice, pageNo, "");
                        }
                    }
                }
            }
        });
        recyclerViewProductListVertical.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPaginationProgressBar() {
        paginationProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePaginationProgressBar() {
        paginationProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void loadBrandsAdapter(BrandsAdapter adapter) {
        filterByDialog.setAdapter(adapter);
    }

    @Override
    public void showLoginSnackbar() {
        Snackbar snackbar = Snackbar.make(relativeLayout, "You must be logged in", Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(cartIntent);
                finish();
            }
        });
        snackbar.show();
    }

    @OnClick(R.id.layout_sort_by) void onSortByClicked(){
        sortByDialog.showDialog();
    }

    @OnClick(R.id.layout_filter_by) void onFilterByClicked(){
        filterByDialog.showDialog();
    }

    @Override
    public void onFilterApplyBtnClicked(int minAmt, int maxAmt) {
        isScrolling = false;
        pageNo = 1;
        totalPage = 1;
        isFirstLoad = false;
        minPrice = minAmt;
        maxPrice = maxAmt;
        recyclerViewProductListVertical.setVisibility(View.GONE);
        showLoader();
        mPresenter.getProductsFiltered(subCategoryId, this.sort, minPrice, maxPrice, pageNo, "refresh");
    }

    @Override
    public void onSortApplyBtnClicked(int sort) {
        isScrolling = false;
        pageNo = 1;
        totalPage = 1;
        isFirstLoad = false;
        this.sort = sort;
        recyclerViewProductListVertical.setVisibility(View.GONE);
        showLoader();
        mPresenter.getProductsFiltered(subCategoryId, this.sort, minPrice, maxPrice, pageNo, "refresh");
    }


    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void onResume(){
        super.onResume();
        isScrolling = false;
        pageNo = 1;
        totalPage = 1;
        if(isFirstLoad){
            mPresenter.getProductList(subCategoryId, pageNo, "refresh");
        }else{
            mPresenter.getProductsFiltered(subCategoryId, this.sort, minPrice, maxPrice, pageNo, "refresh");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
