package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 19-08-2019.
 */

public interface PlaceOrderInteractor {
    interface Callback {
        void onPlaceOrderSuccess();
        void onPlaceOrderFail(String errorMsg);
    }
}
