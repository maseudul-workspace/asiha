package in.net.webinfotech.ashia.presentation.presenters;

import android.widget.ImageView;

import in.net.webinfotech.ashia.domain.models.HomeSliders;
import in.net.webinfotech.ashia.presentation.ui.adapters.CategoryListSmallHorizontalAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListHorizontalAdapter;

/**
 * Created by Raj on 08-07-2019.
 */

public interface MainActivityPresenter{
    void getData();
    void getCartDetails();
    void fetchRecentlyViewed();
    void fetchWishList();
    interface View{
        void loadCategoryAdapter(CategoryListSmallHorizontalAdapter adapter);
        void loadData(HomeSliders[] homeSliders, HomeSliders[] promotionSliders, ProductListHorizontalAdapter newestArrivalsAdapter, ProductListHorizontalAdapter bestSellingAdapter);
        void goToSubCategoryActivity(ImageView imageView, int categoryId, String categoryName, String categoryImage);
        void loadCartCount(int count);
        void loadRecentlyViewed(ProductListHorizontalAdapter adapter);
        void goToProductDetails(int productId);
    }
}
