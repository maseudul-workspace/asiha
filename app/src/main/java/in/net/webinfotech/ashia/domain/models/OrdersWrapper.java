package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 21-08-2019.
 */

public class OrdersWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public Orders[] orders;

}
