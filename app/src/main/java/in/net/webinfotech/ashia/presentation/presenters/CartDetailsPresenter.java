package in.net.webinfotech.ashia.presentation.presenters;

import in.net.webinfotech.ashia.domain.models.Address;
import in.net.webinfotech.ashia.presentation.ui.adapters.CartAdapter;

/**
 * Created by Raj on 31-07-2019.
 */

public interface CartDetailsPresenter {
    void getCartDetails();
    void getWalletDetails();
    void fetchAddress();
    void placeOrder(int addressID, int walletStatus);
    interface View{
        void loadData(CartAdapter adapter, int itemCount, double totalPrice);
        void loadWalletAmount(long amount);
        void showLoader();
        void hideLoader();
        void loadAddress(Address address);
        void onOrderPlacedSuccess();
        void onOrderPlacedFail();
    }
}
