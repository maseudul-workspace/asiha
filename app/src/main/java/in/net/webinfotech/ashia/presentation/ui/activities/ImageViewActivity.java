package in.net.webinfotech.ashia.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.util.GlideHelper;

import android.os.Bundle;

import com.github.chrisbanes.photoview.PhotoView;

public class ImageViewActivity extends AppCompatActivity {

    @BindView(R.id.photoview)
    PhotoView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        ButterKnife.bind(this);
        String imageUrl = getIntent().getStringExtra("imageUrl");
        GlideHelper.setImageView(this, photoView, imageUrl);
    }

    @OnClick(R.id.img_cancel) void onCancelClicked(){
        finish();
    }

}
