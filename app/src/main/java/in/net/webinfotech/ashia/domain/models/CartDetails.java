package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 31-07-2019.
 */

public class CartDetails {
    @SerializedName("cartId")
    @Expose
    public int cartId;

    @SerializedName("productId")
    @Expose
    public int productId;

    @SerializedName("productName")
    @Expose
    public String productName;

    @SerializedName("stock")
    @Expose
    public int stock;

    @SerializedName("brands")
    @Expose
    public String brands;

    @SerializedName("price")
    @Expose
    public Double price;

    @SerializedName("sellingPrice")
    @Expose
    public Double sellingPrice;

    @SerializedName("productImage")
    @Expose
    public String productImage;

}
