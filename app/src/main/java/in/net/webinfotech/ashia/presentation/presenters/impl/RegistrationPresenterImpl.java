package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.RegistrationInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetPlansInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.RegistrationInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetPlansInteractorImpl;
import in.net.webinfotech.ashia.domain.models.Plan;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.presentation.presenters.RegistrationPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.ui.adapters.PlansAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 30-07-2019.
 */

public class RegistrationPresenterImpl extends AbstractPresenter implements RegistrationPresenter, RegistrationInteractor.Callback {

    Context mContext;
    RegistrationPresenter.View mView;
    GetPlansInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    RegistrationInteractorImpl addDownlineInteractor;

    public RegistrationPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void addDownline(String membersName, String membersEmail, String memberPhone, String address, String password) {
        addDownlineInteractor = new RegistrationInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), membersName, membersEmail, address, password, memberPhone);
        addDownlineInteractor.execute();
    }

    @Override
    public void onAddDownlineSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Registered successfully", Toast.LENGTH_SHORT, true).show();
        mView.onRegistrationSuccess();
    }

    @Override
    public void onAddDownlineFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
