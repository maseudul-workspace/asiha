package in.net.webinfotech.ashia.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.Address;

/**
 * Created by Raj on 31-07-2019.
 */

public class AddAddressDialog {

    public interface Callback {
        void onSubmitClicked(String state, String city, String postalCode, String landmark, String phoneNo);
        void editAddress(String state, String city, String postalCode, String landmark, String phoneNo, int addressId);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    EditText editTextState;
    EditText editTextCity;
    EditText editTextPostalCode;
    EditText editTextLandmark;
    EditText editTextPhoneNo;
    LinearLayout layoutLoader;
    Button btnSubmit;
    Callback mCallback;

    public AddAddressDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialog(){
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.address_form_dialog_layout, null);
        editTextState = (EditText) dialogContainer.findViewById(R.id.edit_text_state);
        editTextCity = (EditText) dialogContainer.findViewById(R.id.edit_text_city);
        editTextPostalCode = (EditText) dialogContainer.findViewById(R.id.edit_text_postal_code);
        editTextLandmark = (EditText) dialogContainer.findViewById(R.id.edit_text_landmark);
        editTextPhoneNo = (EditText) dialogContainer.findViewById(R.id.edit_text_phone);
        layoutLoader = (LinearLayout) dialogContainer.findViewById(R.id.layoutLoader);
        btnSubmit = (Button) dialogContainer.findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextCity.getText().toString().trim().isEmpty() ||
                        editTextState.getText().toString().trim().isEmpty() ||
                        editTextLandmark.getText().toString().trim().isEmpty() ||
                        editTextPhoneNo.getText().toString().trim().isEmpty() ||
                        editTextPostalCode.getText().toString().trim().isEmpty()
                        ){
                    Toasty.warning(mContext, "All fields required", Toast.LENGTH_SHORT, true).show();
                } else {
                    layoutLoader.setVisibility(View.VISIBLE);
                    mCallback.onSubmitClicked(editTextState.getText().toString(), editTextCity.getText().toString(), editTextPostalCode.getText().toString(), editTextLandmark.getText().toString(), editTextPhoneNo.getText().toString());
                }
            }
        });

        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void showDialog(){
        dialog.show();
    }

    public void hideLoader(){
       layoutLoader.setVisibility(View.INVISIBLE);
    }

    public void hideDialog(){
        dialog.dismiss();
    }

    public void showEditDialog(final Address address) {
        editTextState.setText(address.state);
        editTextCity.setText(address.city);
        editTextPhoneNo.setText(address.phoneNo);
        editTextLandmark.setText(address.landmark);
        editTextPostalCode.setText(address.postalCode);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextCity.getText().toString().trim().isEmpty() ||
                        editTextState.getText().toString().trim().isEmpty() ||
                        editTextLandmark.getText().toString().trim().isEmpty() ||
                        editTextPhoneNo.getText().toString().trim().isEmpty() ||
                        editTextPostalCode.getText().toString().trim().isEmpty()
                        ){
                    Toasty.warning(mContext, "All fields required", Toast.LENGTH_SHORT, true).show();
                } else {
                    layoutLoader.setVisibility(View.VISIBLE);
                    mCallback.editAddress(editTextState.getText().toString(), editTextCity.getText().toString(), editTextPostalCode.getText().toString(), editTextLandmark.getText().toString(), editTextPhoneNo.getText().toString(), address.id);
                }
            }
        });

        dialog.show();

    }

}
