package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetWalletHistoryInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.WalletHistory;
import in.net.webinfotech.ashia.domain.models.WalletHistoryWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 10-08-2019.
 */

public class GetWalletHistoryInteractorImpl extends AbstractInteractor implements GetWalletHistoryInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;

    public GetWalletHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletHistoryFail(errorMsg);
            }
        });
    }

    private void postMessage(final WalletHistory[] walletHistories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletHistorySuccess(walletHistories);
            }
        });
    }

    @Override
    public void run() {
        final WalletHistoryWrapper walletHistoryWrapper = mRepository.fetchWalletHistories(apiToken, userId);
        if (walletHistoryWrapper == null) {
            notifyError("Something went wrong");
        } else if (!walletHistoryWrapper.status) {
            notifyError("Something went wrong");
        } else {
            postMessage(walletHistoryWrapper.walletHistories);
        }
    }
}
