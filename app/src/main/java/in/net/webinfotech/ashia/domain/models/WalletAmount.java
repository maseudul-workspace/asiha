package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 10-08-2019.
 */

public class WalletAmount {
    @SerializedName("totalAmount")
    @Expose
    public long totalAmount;
}
