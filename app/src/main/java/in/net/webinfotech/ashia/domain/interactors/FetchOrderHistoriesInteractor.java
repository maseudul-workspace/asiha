package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.Orders;

/**
 * Created by Raj on 21-08-2019.
 */

public interface FetchOrderHistoriesInteractor {
    interface Callback {
        void onGettingOrdersSuccess(Orders[] orders);
        void onGettingOrdersFail(String errorMsg);
    }
}
