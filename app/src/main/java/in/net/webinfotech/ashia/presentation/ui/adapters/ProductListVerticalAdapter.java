package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.util.GlideHelper;

/**
 * Created by Raj on 10-07-2019.
 */

public class ProductListVerticalAdapter extends RecyclerView.Adapter<ProductListVerticalAdapter.ViewHolder> {

    public interface Callback{
        void goToProductDetails(int productId);
        void addToCart(int productId, int position);
        void addToWishList(int productId, int position);
        void deleteFromWishList(int productId, int position);
    }

    Context mContext;
    ProductList[] productLists;
    Callback mCallback;

    public ProductListVerticalAdapter(Context mContext, ProductList[] productLists, Callback callback) {
        this.mContext = mContext;
        this.productLists = productLists;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_product_list, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewProductName.setText(productLists[i].productName);
        viewHolder.txtViewProductPrize.setText(productLists[i].sellingPrice);
        viewHolder.txtViewMrp.setText(productLists[i].price);
        viewHolder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        GlideHelper.setImageView(mContext, viewHolder.imgViewProduct, productLists[i].image);

        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.goToProductDetails(productLists[i].id);
            }
        });

        viewHolder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.addToCart(productLists[i].id, i);
            }
        });

        if(productLists[i].isAddedToCart){
            viewHolder.btnAddToCart.setEnabled(false);
        }else{
            if (productLists[i].stock == 0) {
                viewHolder.btnAddToCart.setEnabled(false);
            } else {
                viewHolder.btnAddToCart.setEnabled(true);
            }
        }

        if (productLists[i].isWishListPresent) {
            viewHolder.imgViewHeartOutline.setVisibility(View.GONE);
            viewHolder.imgViewHeartFill.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgViewHeartFill.setVisibility(View.GONE);
            viewHolder.imgViewHeartOutline.setVisibility(View.VISIBLE);
        }

        viewHolder.imgViewHeartOutline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.addToWishList(productLists[i].id, i);
            }
        });

        viewHolder.imgViewHeartFill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.deleteFromWishList(productLists[i].id, i);
            }
        });
        viewHolder.txtViewBrandName.setText(productLists[i].brand_name);
    }

    @Override
    public int getItemCount() {
        return productLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_product_prize)
        TextView txtViewProductPrize;
        @BindView(R.id.img_view_heart_fill)
        ImageView imgViewHeartFill;
        @BindView(R.id.img_view_heart_outline)
        ImageView imgViewHeartOutline;
        @BindView(R.id.btn_add_to_cart)
        ImageView btnAddToCart;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_brand_name)
        TextView txtViewBrandName;
        @BindView(R.id.txt_view_product_mrp)
        TextView txtViewMrp;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(ProductList[] productLists){
        this.productLists = productLists;
    }

    public void onAddToCartSuccess(int position){
        productLists[position].isAddedToCart = true;
        notifyItemChanged(position);
    }

    public void onAddToWishListSuccess(int position) {
        productLists[position].isWishListPresent = true;
        notifyItemChanged(position);
    }

    public void onDeleteFromWishListSuccess(int position) {
        productLists[position].isWishListPresent = false;
        notifyItemChanged(position);
    }

}
