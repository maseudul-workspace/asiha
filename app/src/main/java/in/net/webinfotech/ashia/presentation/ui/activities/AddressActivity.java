package in.net.webinfotech.ashia.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.domain.models.Address;
import in.net.webinfotech.ashia.presentation.presenters.AddressesPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.AddressesPresenterImpl;
import in.net.webinfotech.ashia.presentation.ui.adapters.AddressAdapter;
import in.net.webinfotech.ashia.presentation.ui.dialogs.AddAddressDialog;
import in.net.webinfotech.ashia.threading.MainThreadImpl;

public class AddressActivity extends AppCompatActivity implements AddressesPresenter.View, AddAddressDialog.Callback {

    AddAddressDialog addAddressDialog;
    @BindView(R.id.recycler_view_address)
    RecyclerView recyclerViewAddress;
    AddressesPresenterImpl mPresenter;
    @BindView(R.id.layoutLoader)
    View layoutLoader;
    @BindView(R.id.btn_add_address)
    Button btnAddAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        ButterKnife.bind(this);
        initialiseDialog();
        addAddressDialog.setUpDialog();
        initialisePresenter();
        mPresenter.fetchAddress();
        getSupportActionBar().setTitle("Shipping Addresses");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new AddressesPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseDialog(){
        addAddressDialog = new AddAddressDialog(this, this, this);
    }

    @OnClick(R.id.btn_add_address) void onAddAddressClicked(){
        addAddressDialog.showDialog();
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLooader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void loadAddressAdapter(AddressAdapter adapter) {
        recyclerViewAddress.setVisibility(View.VISIBLE);
        recyclerViewAddress.setAdapter(adapter);
        recyclerViewAddress.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onAddAddressSuccess() {
        addAddressDialog.hideLoader();
        addAddressDialog.hideDialog();
    }

    @Override
    public void onAddressFailed() {
        addAddressDialog.hideLoader();
    }

    @Override
    public void onAddressEditClicked(Address address) {
        addAddressDialog.showEditDialog(address);
    }

    @Override
    public void onSubmitClicked(String state, String city, String postalCode, String landmark, String phoneNo) {
        mPresenter.addAddress(state, city, phoneNo, postalCode, landmark);
    }

    @Override
    public void editAddress(String state, String city, String postalCode, String landmark, String phoneNo, int addressId) {
        mPresenter.editAddress(state, city, postalCode, landmark, phoneNo, addressId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
