package in.net.webinfotech.ashia.repository.impl;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import in.net.webinfotech.ashia.domain.models.AddAddressResponse;
import in.net.webinfotech.ashia.domain.models.AddRecentlyViewedResponse;
import in.net.webinfotech.ashia.domain.models.AddToCartResponse;
import in.net.webinfotech.ashia.domain.models.AddToWishListResponse;
import in.net.webinfotech.ashia.domain.models.AddressDeleteResponse;
import in.net.webinfotech.ashia.domain.models.AddressWrapper;
import in.net.webinfotech.ashia.domain.models.BrandsWrapper;
import in.net.webinfotech.ashia.domain.models.CancelOrderResponse;
import in.net.webinfotech.ashia.domain.models.CartDetailsWrapper;
import in.net.webinfotech.ashia.domain.models.DeleteCartResponse;
import in.net.webinfotech.ashia.domain.models.DeleteFromWishListResponse;
import in.net.webinfotech.ashia.domain.models.DownlineAddResponse;
import in.net.webinfotech.ashia.domain.models.DownlinesWrapper;
import in.net.webinfotech.ashia.domain.models.EditAddressResponse;
import in.net.webinfotech.ashia.domain.models.HomeDataWrapper;
import in.net.webinfotech.ashia.domain.models.OrderPlaceResponse;
import in.net.webinfotech.ashia.domain.models.OrdersWrapper;
import in.net.webinfotech.ashia.domain.models.PlanWrapper;
import in.net.webinfotech.ashia.domain.models.ProductDetailsWrapper;
import in.net.webinfotech.ashia.domain.models.ProductListWrapper;
import in.net.webinfotech.ashia.domain.models.RankingWrapper;
import in.net.webinfotech.ashia.domain.models.ReviewResponse;
import in.net.webinfotech.ashia.domain.models.SubCategoryListWrapper;
import in.net.webinfotech.ashia.domain.models.UserInfoWrapper;
import in.net.webinfotech.ashia.domain.models.WalletAmountWrapper;
import in.net.webinfotech.ashia.domain.models.WalletHistoryWrapper;
import in.net.webinfotech.ashia.repository.ApiClient;
import in.net.webinfotech.ashia.repository.OtherRepository;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;

/**
 * Created by Raj on 13-07-2019.
 */

public class OtherRepositoryImpl {
    OtherRepository mRepository;

    public OtherRepositoryImpl() {
        mRepository = ApiClient.createService(OtherRepository.class);
    }

    public HomeDataWrapper getHomeData(){
        HomeDataWrapper homeDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getHomeData();

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    homeDataWrapper = null;
                }else{
                    homeDataWrapper = gson.fromJson(responseBody, HomeDataWrapper.class);

                }
            } else {
                homeDataWrapper = null;
            }
        }catch (Exception e){
            homeDataWrapper = null;
        }
        return homeDataWrapper;
    }

    public SubCategoryListWrapper getSubCategories(int id){
        SubCategoryListWrapper subCategoryListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getSubCategories(id);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    subCategoryListWrapper = null;
                }else{
                    subCategoryListWrapper = gson.fromJson(responseBody, SubCategoryListWrapper.class);
                }
            } else {
                subCategoryListWrapper = null;
            }
        }catch (Exception e){
            subCategoryListWrapper = null;
        }
        return subCategoryListWrapper;
    }

    public ProductListWrapper getProductsList(int subCategoryId, int page){
        ProductListWrapper productListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getProductsList(subCategoryId, page);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWrapper = null;
                }else{
                    productListWrapper = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                productListWrapper = null;
            }
        }catch (Exception e){
            productListWrapper = null;
        }
        return  productListWrapper;
    }

    public UserInfoWrapper checkLogin(String phone, String password){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(phone, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public BrandsWrapper getBrands(int subCategoryId){
        BrandsWrapper brandsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.getBrands(subCategoryId);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    brandsWrapper = null;
                }else{
                    brandsWrapper = gson.fromJson(responseBody, BrandsWrapper.class);
                }
            } else {
                brandsWrapper = null;
            }
        }catch (Exception e){
            brandsWrapper = null;
        }
        return brandsWrapper;
    }

    public ProductListWrapper getProductsFiltered(int subcategoryId, int sort, ArrayList<String> brands, int min, int max, int page){

        ProductListWrapper productListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getProductsFiltered(subcategoryId, brands, sort, min, max, page);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWrapper = null;
                }else{
                    productListWrapper = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                productListWrapper = null;
            }
        }catch (Exception e){
            productListWrapper = null;
        }
        return productListWrapper;
    }

    public ProductDetailsWrapper getProductDetails(int productId){
        ProductDetailsWrapper productDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getProductDetails(productId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productDetailsWrapper = null;
                }else{
                    productDetailsWrapper = gson.fromJson(responseBody, ProductDetailsWrapper.class);
                }
            } else {
                productDetailsWrapper = null;
            }
        }catch (Exception e){
            productDetailsWrapper = null;
        }
        return productDetailsWrapper;
    }

    public ReviewResponse submitReview(int userId, int productId, int rating, String comment, String apiToken){
        ReviewResponse reviewResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> submit = mRepository.submitReview(productId, rating, comment, apiToken, userId);

            Response<ResponseBody> response = submit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    reviewResponse = null;
                }else{
                    reviewResponse = gson.fromJson(responseBody, ReviewResponse.class);
                }
            } else {
                reviewResponse = null;
            }
        }catch (Exception e){
            reviewResponse = null;
        }
        return reviewResponse;
    }

    public PlanWrapper getPlans(int userId, String apiToken){

        PlanWrapper planWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchPlans(apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    planWrapper = null;
                }else{
                    planWrapper = gson.fromJson(responseBody, PlanWrapper.class);
                }
            } else {
                planWrapper = null;
            }
        }catch (Exception e){
            planWrapper = null;
        }
        return planWrapper;
    }

    public DownlineAddResponse addDownline(String membersName, String memberEmail, String mobileNo, String password, String address){
        DownlineAddResponse downlineAddResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addDownline(membersName, memberEmail, mobileNo, password, address);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    downlineAddResponse = null;
                }else{
                    downlineAddResponse = gson.fromJson(responseBody, DownlineAddResponse.class);
                }
            } else {
                downlineAddResponse = null;
            }
        }catch (Exception e){
            downlineAddResponse = null;
        }
        return downlineAddResponse;
    }

    public CartDetailsWrapper getCartDetails(String apiToken, int userId){
        CartDetailsWrapper cartDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCartDetails(apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                isErrorResponse = true;
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cartDetailsWrapper = null;
                }else{
                    cartDetailsWrapper = gson.fromJson(responseBody, CartDetailsWrapper.class);
                }
            } else {
                cartDetailsWrapper = null;
            }
        }catch (Exception e){
            cartDetailsWrapper = null;
        }
        return cartDetailsWrapper;
    }

    public AddToCartResponse addToCart(String apiToken, int userId, int productId, int quantity){
        AddToCartResponse addToCartResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToCart(apiToken, userId, productId, quantity);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                isErrorResponse = true;
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addToCartResponse = null;
                }else{
                    addToCartResponse = gson.fromJson(responseBody, AddToCartResponse.class);
                }
            } else {
                addToCartResponse = null;
            }
        }catch (Exception e){
            addToCartResponse = null;
        }
        return addToCartResponse;
    }

    public DeleteCartResponse deleteFromCart(String apiToken, int userId, int cartId){
        DeleteCartResponse deleteCartResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> delete = mRepository.deleteFromCart(apiToken, userId, cartId);

            Response<ResponseBody> response = delete.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    deleteCartResponse = null;
                }else{
                    deleteCartResponse = gson.fromJson(responseBody, DeleteCartResponse.class);
                }
            } else {
                deleteCartResponse = null;
            }
        }catch (Exception e){
            deleteCartResponse = null;
        }
        return deleteCartResponse;
    }

    public AddressWrapper fetchAddresses(String apiToken, int userId) {
        AddressWrapper addressWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchAddress(apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addressWrapper = null;
                }else{
                    addressWrapper = gson.fromJson(responseBody, AddressWrapper.class);
                }
            } else {
                addressWrapper = null;
            }
        }catch (Exception e){
            addressWrapper = null;
        }
        return addressWrapper;
    }

    public AddAddressResponse addAddress(String apiToken, int userId, String state, String city, String postalCode, String phoneNo,String landmark){
        AddAddressResponse addAddressResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addAddress(apiToken, userId, state, city, postalCode, phoneNo, landmark);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addAddressResponse = null;
                }else{
                    addAddressResponse = gson.fromJson(responseBody, AddAddressResponse.class);
                }
            } else {
                addAddressResponse = null;
            }
        }catch (Exception e){
            addAddressResponse = null;
        }
        return addAddressResponse;
    }

    public AddressDeleteResponse deleteAddress(String apiToken, int userId, int addressId) {
        AddressDeleteResponse addressDeleteResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> delete = mRepository.deleteAddress(apiToken, userId, addressId);

            Response<ResponseBody> response = delete.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addressDeleteResponse = null;
                }else{
                    addressDeleteResponse = gson.fromJson(responseBody, AddressDeleteResponse.class);
                }
            } else {
                addressDeleteResponse = null;
            }
        }catch (Exception e){
            addressDeleteResponse = null;
        }
        return addressDeleteResponse;
    }

    public EditAddressResponse editAddress(String apiToken, int userId, String state, String city, String postalCode, String phoneNo,String landmark, int addressId) {
        EditAddressResponse editAddressResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> edit = mRepository.editAddress(apiToken, userId, state, city, postalCode, phoneNo, landmark, addressId);

            Response<ResponseBody> response = edit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    editAddressResponse = null;
                }else{
                    editAddressResponse = gson.fromJson(responseBody, EditAddressResponse.class);
                }
            } else {
                editAddressResponse = null;
            }
        }catch (Exception e){
            editAddressResponse = null;
        }
        return editAddressResponse;
    }

    public DownlinesWrapper fetchDownlines(String apiToken, int userId) {
        DownlinesWrapper downlinesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchDownlines(apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    downlinesWrapper = null;
                }else{
                    downlinesWrapper = gson.fromJson(responseBody, DownlinesWrapper.class);
                }
            } else {
                downlinesWrapper = null;
            }
        }catch (Exception e){
            downlinesWrapper = null;
        }
        return downlinesWrapper;
    }

    public WalletAmountWrapper fetchWalletAmount(String apiToken, int userId) {
        WalletAmountWrapper walletAmountWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWalletAmount(apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletAmountWrapper = null;
                }else{
                    walletAmountWrapper = gson.fromJson(responseBody, WalletAmountWrapper.class);
                }
            } else {
                walletAmountWrapper = null;
            }
        }catch (Exception e){
            walletAmountWrapper = null;
        }
        return walletAmountWrapper;
    }

    public WalletHistoryWrapper fetchWalletHistories(String apiToken, int userId) {
        WalletHistoryWrapper walletHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWalletHistories(apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletHistoryWrapper = null;
                }else{
                    walletHistoryWrapper = gson.fromJson(responseBody, WalletHistoryWrapper.class);
                }
            } else {
                walletHistoryWrapper = null;
            }
        }catch (Exception e){
            walletHistoryWrapper = null;
        }
        return walletHistoryWrapper;
    }

    public OrderPlaceResponse placeOrder(String apiToken, int userId, int shippingAddressId, int walletStatus) {
        OrderPlaceResponse orderPlaceResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> place = mRepository.placeOrder(apiToken, userId, shippingAddressId, walletStatus);

            Response<ResponseBody> response = place.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderPlaceResponse = null;
                }else{
                    orderPlaceResponse = gson.fromJson(responseBody, OrderPlaceResponse.class);
                }
            } else {
                orderPlaceResponse = null;
            }
        }catch (Exception e){
            orderPlaceResponse = null;
        }
        return orderPlaceResponse;
    }

    public OrdersWrapper fetchOrders(String apiToken, int userId) {
        OrdersWrapper ordersWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchOrderHistories(apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    ordersWrapper = null;
                }else{
                    ordersWrapper = gson.fromJson(responseBody, OrdersWrapper.class);
                }
            } else {
                ordersWrapper = null;
            }
        }catch (Exception e){
            ordersWrapper = null;
        }
        return ordersWrapper;
    }

    public CancelOrderResponse cancelOrder(String apiToken, int userId, String orderId) {
        CancelOrderResponse cancelOrderResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> cancel = mRepository.cancelOrder(apiToken, userId, orderId);

            Response<ResponseBody> response = cancel.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cancelOrderResponse = null;
                }else{
                    cancelOrderResponse = gson.fromJson(responseBody, CancelOrderResponse.class);
                }
            } else {
                cancelOrderResponse = null;
            }
        }catch (Exception e){
            cancelOrderResponse = null;
        }
        return cancelOrderResponse;
    }

    public ProductListWrapper fetchRecentlyViews(String apiToken, int userId) {
        ProductListWrapper productListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchRecentlyViews(apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWrapper = null;
                }else{
                    productListWrapper = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                productListWrapper = null;
            }
        }catch (Exception e){
            productListWrapper = null;
        }
        return productListWrapper;
    }

    public AddRecentlyViewedResponse addRecentlyViewed(String apiToken, int userId, int productId) {
        AddRecentlyViewedResponse addRecentlyViewedResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addRecentlyView(apiToken, userId, productId);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addRecentlyViewedResponse = null;
                }else{
                    addRecentlyViewedResponse = gson.fromJson(responseBody, AddRecentlyViewedResponse.class);
                }
            } else {
                addRecentlyViewedResponse = null;
            }
        }catch (Exception e){
            addRecentlyViewedResponse = null;
        }
        return addRecentlyViewedResponse;
    }

    public ProductListWrapper fetchWishList(String apiToken, int userId) {
        ProductListWrapper productListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWishList(apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWrapper = null;
                }else{
                    productListWrapper = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                productListWrapper = null;
            }
        }catch (Exception e){
            productListWrapper = null;
        }
        return productListWrapper;
    }

    public AddToWishListResponse addToWishList(String apiToken, int userId, int productId) {
        AddToWishListResponse addToWishListResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToWishList(apiToken, userId, productId);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addToWishListResponse = null;
                }else{
                    addToWishListResponse = gson.fromJson(responseBody, AddToWishListResponse.class);
                }
            } else {
                addToWishListResponse = null;
            }
        }catch (Exception e){
            addToWishListResponse = null;
        }
        return addToWishListResponse;
    }

    public DeleteFromWishListResponse deleteFromWishList(String apiToken, int userId, int productId) {
        DeleteFromWishListResponse deleteFromWishListResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> delete = mRepository.deleteFromWishList(apiToken, userId, productId);

            Response<ResponseBody> response = delete.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    deleteFromWishListResponse = null;
                }else{
                    deleteFromWishListResponse = gson.fromJson(responseBody, DeleteFromWishListResponse.class);
                }
            } else {
                deleteFromWishListResponse = null;
            }
        }catch (Exception e){
            deleteFromWishListResponse = null;
        }
        return deleteFromWishListResponse;
    }

    public ProductListWrapper getRelatedProducts(int brandId) {
        ProductListWrapper productListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getRelatedProducts(brandId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWrapper = null;
                }else{
                    productListWrapper = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                productListWrapper = null;
            }
        }catch (Exception e){
            productListWrapper = null;
        }
        return productListWrapper;
    }

    public RankingWrapper fetchRank(String apiToken, int userId) {
        RankingWrapper rankingWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchRanking(apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    rankingWrapper = null;
                }else{
                    rankingWrapper = gson.fromJson(responseBody, RankingWrapper.class);
                }
            } else {
                rankingWrapper = null;
            }
        }catch (Exception e){
            rankingWrapper = null;
        }
        return rankingWrapper;
    }

}
