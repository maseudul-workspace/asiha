package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 23-07-2019.
 */

public class Brands {
    @SerializedName("brand_id")
    @Expose
    public String id;

    @SerializedName("brands")
    @Expose
    public String brands;

    public Boolean isChecked = true;

}
