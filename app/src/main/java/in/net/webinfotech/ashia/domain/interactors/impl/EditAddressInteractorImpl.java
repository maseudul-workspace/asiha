package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.EditAddressInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.EditAddressResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 05-08-2019.
 */

public class EditAddressInteractorImpl extends AbstractInteractor implements EditAddressInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;
    String state;
    String city;
    String postalCode;
    String phoneNo;
    String landmark;
    int addressId;

    public EditAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback callback, OtherRepositoryImpl mRepository, String apiToken, int userId, String state, String city, String postalCode, String phoneNo, String landmark, int addressId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.state = state;
        this.city = city;
        this.postalCode = postalCode;
        this.phoneNo = phoneNo;
        this.landmark = landmark;
        this.addressId = addressId;
        mCallback = callback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEditAddressFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEditAddressSuccess();
            }
        });
    }

    @Override
    public void run() {
        final EditAddressResponse editAddressResponse = mRepository.editAddress(apiToken, userId, state, city, postalCode, phoneNo, landmark, addressId);
        if(editAddressResponse == null) {
            notifyError("Something went wrong");
        } else if (!editAddressResponse.status) {
            notifyError("Something went wrong");
        } else {
            postMessage();
        }
    }
}
