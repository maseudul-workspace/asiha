package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 26-07-2019.
 */

public interface SubmitReviewInteractor {
    interface Callback{
        void onReviewSubmitSuccess();
        void onReviewSubmitFail();
    }
}
