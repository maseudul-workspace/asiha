package in.net.webinfotech.ashia.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.net.webinfotech.ashia.R;

/**
 * Created by Raj on 18-07-2019.
 */

public class SortByDialog {

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    TextView txtViewNewestArrivals;
    TextView txtViewPopularity;
    TextView txtViewPriceLow;
    TextView txtViewPriceHigh;
    View layoutNewestArrivals;
    View layoutPopularity;
    View layoutPriceLow;
    View layoutPriceHigh;
    ImageView imgViewNewestSelected;
    ImageView imgViewPopularitySlected;
    ImageView imgViewPriceLow;
    ImageView imgViewPriceHigh;
    ImageView imgViewCancel;
    int sort;
    Button btnApply;

    public interface Callback{
        void onSortApplyBtnClicked(int sort);
    }

    Callback mCallback;

    public SortByDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialogView(){

        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.sort_by_dialog_layout, null);
        txtViewNewestArrivals = (TextView) dialogContainer.findViewById(R.id.txt_view_newest_first);
        txtViewPopularity = (TextView) dialogContainer.findViewById(R.id.txt_view_popularity);
        txtViewPriceLow = (TextView) dialogContainer.findViewById(R.id.txt_view_price_low);
        txtViewPriceHigh = (TextView) dialogContainer.findViewById(R.id.txt_view_price_high);
        imgViewCancel = (ImageView) dialogContainer.findViewById(R.id.img_view_cancel);
        btnApply = (Button) dialogContainer.findViewById(R.id.btn_apply);

        layoutNewestArrivals = (View) dialogContainer.findViewById(R.id.layout_newest_first);
        layoutPopularity = (View) dialogContainer.findViewById(R.id.layout_popularity);
        layoutPriceLow = (View) dialogContainer.findViewById(R.id.layout_low_to_high);
        layoutPriceHigh = (View) dialogContainer.findViewById(R.id.layout_high_to_low);

        imgViewNewestSelected = (ImageView) dialogContainer.findViewById(R.id.img_view_new_selected);
        imgViewPopularitySlected = (ImageView) dialogContainer.findViewById(R.id.img_view_popularity_selected);
        imgViewPriceLow = (ImageView) dialogContainer.findViewById(R.id.img_view_low_selected);
        imgViewPriceHigh = (ImageView) dialogContainer.findViewById(R.id.img_view_high_selected);

        txtViewNewestArrivals.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        imgViewNewestSelected.setVisibility(View.VISIBLE);

        layoutNewestArrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.VISIBLE);
                imgViewPopularitySlected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.GONE);

                txtViewNewestArrivals.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                txtViewPopularity.setTextColor(mContext.getResources().getColor(R.color.black1));
                txtViewPriceHigh.setTextColor(mContext.getResources().getColor(R.color.black1));
                txtViewPriceLow.setTextColor(mContext.getResources().getColor(R.color.black1));
                sort = 1;
            }
        });

        layoutPopularity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPopularitySlected.setVisibility(View.VISIBLE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.GONE);

                txtViewNewestArrivals.setTextColor(mContext.getResources().getColor(R.color.black1));
                txtViewPopularity.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                txtViewPriceHigh.setTextColor(mContext.getResources().getColor(R.color.black1));
                txtViewPriceLow.setTextColor(mContext.getResources().getColor(R.color.black1));
                sort = 2;

            }
        });

        layoutPriceLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPopularitySlected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.VISIBLE);

                txtViewNewestArrivals.setTextColor(mContext.getResources().getColor(R.color.black1));
                txtViewPopularity.setTextColor(mContext.getResources().getColor(R.color.black1));
                txtViewPriceHigh.setTextColor(mContext.getResources().getColor(R.color.black1));
                txtViewPriceLow.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                sort = 3;
            }
        });

        layoutPriceHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPopularitySlected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.VISIBLE);
                imgViewPriceLow.setVisibility(View.GONE);

                txtViewNewestArrivals.setTextColor(mContext.getResources().getColor(R.color.black1));
                txtViewPopularity.setTextColor(mContext.getResources().getColor(R.color.black1));
                txtViewPriceHigh.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                txtViewPriceLow.setTextColor(mContext.getResources().getColor(R.color.black1));
                sort = 4;
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mCallback.onSortApplyBtnClicked(sort);
            }
        });

        imgViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

    }

    public void showDialog(){
        dialog.show();
    }

}
