package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 23-07-2019.
 */

public class ProductListData {
    @SerializedName("products")
    @Expose
    public ProductList[] productLists;

    @SerializedName("brands")
    @Expose
    public Brands[] brands;
}
