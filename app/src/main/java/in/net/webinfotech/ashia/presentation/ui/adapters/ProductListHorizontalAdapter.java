package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.util.GlideHelper;

/**
 * Created by Raj on 18-06-2019.
 */

public class ProductListHorizontalAdapter extends RecyclerView.Adapter<ProductListHorizontalAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
    }

    Context mContext;
    in.net.webinfotech.ashia.domain.models.ProductList[] productLists;
    Callback mCallback;


    public ProductListHorizontalAdapter(Context mContext, in.net.webinfotech.ashia.domain.models.ProductList[] productLists, Callback callback) {
        this.mContext = mContext;
        this.productLists = productLists;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_products_horizontal, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.txtViewProductName.setText(productLists[position].productName);
        holder.txtViewProductPrice.setText("Rs. " + productLists[position].price);
        GlideHelper.setImageView(mContext, holder.imgViewProduct, productLists[position].image);
        holder.txtViewProductSellingPrice.setText("Rs. " + productLists[position].sellingPrice);
        holder.txtViewProductPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(productLists[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.txt_view_product_selling_price)
        TextView txtViewProductSellingPrice;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
