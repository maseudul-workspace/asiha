package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.Brands;

/**
 * Created by Raj on 24-07-2019.
 */

public interface GetBrandsInteractor {
    interface Callback{
        void onGettingBrandsSuccess(Brands[] brands);
        void onGettingBrandsFail();
    }
}
