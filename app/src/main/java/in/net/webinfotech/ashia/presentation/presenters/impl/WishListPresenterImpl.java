package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.AddToCartInteractor;
import in.net.webinfotech.ashia.domain.interactors.AddToWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.DeleteFromWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.FetchWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetCartDetailsInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.AddToCartInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.AddToWishListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.DeleteFromWishListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.FetchWishListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetCartDetailsInteractorImpl;
import in.net.webinfotech.ashia.domain.models.CartDetails;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.presentation.presenters.WishListPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListVerticalAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 26-08-2019.
 */

public class WishListPresenterImpl extends AbstractPresenter implements WishListPresenter,
                                                                        ProductListVerticalAdapter.Callback,
                                                                        FetchWishListInteractor.Callback,
                                                                        AddToWishListInteractor.Callback,
                                                                        DeleteFromWishListInteractor.Callback,
                                                                        AddToCartInteractor.Callback,
                                                                        GetCartDetailsInteractor.Callback
                                                                        {

    Context mContext;
    WishListPresenter.View mView;
    AndroidApplication androidApplication;
    FetchWishListInteractorImpl fetchWishListInteractor;
    ProductListVerticalAdapter adapter;
    AddToWishListInteractorImpl addToWishListInteractor;
    DeleteFromWishListInteractorImpl deleteFromWishListInteractor;
    AddToCartInteractorImpl addToCartInteractor;
    GetCartDetailsInteractorImpl getCartDetailsInteractor;
    int position;

    public WishListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWishList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo != null) {
            fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            fetchWishListInteractor.execute();
        }
    }

    @Override
    public void fetchCartDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo != null) {
            getCartDetailsInteractor = new GetCartDetailsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            getCartDetailsInteractor.execute();
        }
    }

    @Override
    public void goToProductDetails(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void addToCart(int productId, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, 1, productId);
        addToCartInteractor.execute();
    }

    @Override
    public void addToWishList(int productId, int position) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, productId);
        addToWishListInteractor.execute();
    }

    @Override
    public void deleteFromWishList(int productId, int position) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        deleteFromWishListInteractor = new DeleteFromWishListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, productId);
        deleteFromWishListInteractor.execute();
    }

    @Override
    public void onGettingWishListSuccess(ProductList[] productLists) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        CartDetails[] cartDetails = androidApplication.getCartDetails(mContext);
        if(cartDetails != null){
            for(int i = 0; i < productLists.length; i++){
                for(int j = 0; j < cartDetails.length; j++){
                    if(productLists[i].id == cartDetails[j].productId){
                        productLists[i].isAddedToCart = true;
                        break;
                    }
                }
            }
        }
        for(int i = 0; i < productLists.length; i++){
            productLists[i].isWishListPresent = true;
        }
        androidApplication.setWishList(mContext, productLists);
        adapter = new ProductListVerticalAdapter(mContext, productLists, this);
        mView.loadData(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
    }

    @Override
    public void onWishListAddingSuccess() {
       fetchWishList();
       Toasty.success(mContext, "Successfully added", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onWishListAddingFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onDeleteFromWishListSuccess() {
        fetchWishList();
        Toasty.success(mContext, "Removed from wishlist", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onDeleteFromWishListFail() {
        Toasty.error(mContext, "Something went wrong ", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onAddToCartSuccess() {
        fetchCartDetails();
        Toasty.success(mContext, "Successfully added to cart  ", Toast.LENGTH_SHORT, true).show();
        adapter.onAddToCartSuccess(position);
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onGettingCartDetailsSuccess(CartDetails[] cartDetails) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setCartDetails(mContext, cartDetails);
    }

    @Override
    public void onGettingCartDetailsFail() {

    }
}
