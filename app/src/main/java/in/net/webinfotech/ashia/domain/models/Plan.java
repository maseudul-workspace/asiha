package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 29-07-2019.
 */

public class Plan {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("plan_name")
    @Expose
    public String planName;

    @SerializedName("plan_amount")
    @Expose
    public String planAmount;

    @SerializedName("plan_comm")
    @Expose
    public String planComm;

}
