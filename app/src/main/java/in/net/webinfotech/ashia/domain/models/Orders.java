package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 21-08-2019.
 */

public class Orders {

    @SerializedName("orderId")
    @Expose
    public String orderId;

    @SerializedName("totalAmount")
    @Expose
    public String totalAmt;

    @SerializedName("orderStatus")
    @Expose
    public int orderStatus;

    @SerializedName("walletPayStatus")
    @Expose
    public int walletPayStatus;

    @SerializedName("walletPayAmount")
    @Expose
    public String walletAmount;

    @SerializedName("orderDate")
    @Expose
    public String orderDate;

    @SerializedName("productInfo")
    @Expose
    public OrderProducts[] orderProducts;

}
