package in.net.webinfotech.ashia.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.R;

/**
 * Created by Raj on 26-07-2019.
 */

public class ReviewDialog {

    public interface Callback{
        void onSubmitClicked(float rating, String comment);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    TextView txtViewUsername;
    TextView txtViewProductName;
    RatingBar ratingBar;
    EditText editTextComment;
    View layoutLoader;
    Button btnCancel;
    Button btnSubmit;
    float rating  = 0;
    Callback mCallback;

    public ReviewDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialog(String username, String productName){
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.review_dialog_layout, null);
        txtViewUsername = (TextView) dialogContainer.findViewById(R.id.txt_view_user_name);
        txtViewProductName = (TextView) dialogContainer.findViewById(R.id.txt_view_product_name);
        ratingBar = (RatingBar) dialogContainer.findViewById(R.id.rating_bar);
        editTextComment = (EditText) dialogContainer.findViewById(R.id.edit_text_comment);
        layoutLoader = (View) dialogContainer.findViewById(R.id.layout_loader);
        btnCancel = (Button) dialogContainer.findViewById(R.id.btn_cancel);
        btnSubmit = (Button) dialogContainer.findViewById(R.id.btn_submit);
        txtViewUsername.setText(username);
        txtViewProductName.setText(productName);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float r, boolean fromUser) {
                rating = r;
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rating < 1){
                    Toasty.warning(mContext, "Please submit rating", Toast.LENGTH_SHORT, true).show();
                }else{
                    layoutLoader.setVisibility(View.VISIBLE);
                    mCallback.onSubmitClicked(rating, editTextComment.getText().toString());
                }
            }
        });
        builder = new AlertDialog.Builder(mContext);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void showDialog(){
        dialog.show();
    }

    public void hideLoader(){
        layoutLoader.setVisibility(View.INVISIBLE);
    }

    public void hideDialog(){
        dialog.dismiss();
    }

}
