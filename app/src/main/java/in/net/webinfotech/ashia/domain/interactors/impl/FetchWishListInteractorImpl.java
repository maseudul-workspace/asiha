package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.FetchWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.ProductListWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 23-08-2019.
 */

public class FetchWishListInteractorImpl extends AbstractInteractor implements FetchWishListInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiTokem;
    int userId;

    public FetchWishListInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiTokem, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiTokem = apiTokem;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWishListFail(errorMsg);
            }
        });
    }

    private void postMessage(final ProductList[] productLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWishListSuccess(productLists);
            }
        });
    }


    @Override
    public void run() {
        final ProductListWrapper productListWrapper = mRepository.fetchWishList(apiTokem, userId);
        if (productListWrapper == null) {
            notifyError("Something went wrong");
        } else if(!productListWrapper.status) {
            notifyError("Something went wrong");
        } else {
            postMessage(productListWrapper.productLists);
        }
    }
}
