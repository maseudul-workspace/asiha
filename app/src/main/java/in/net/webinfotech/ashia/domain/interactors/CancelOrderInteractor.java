package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 22-08-2019.
 */

public interface CancelOrderInteractor {
    interface Callback {
        void onOrderCancelSuccess();
        void onOrderCancelFailed(String errorMsg);
    }
}
