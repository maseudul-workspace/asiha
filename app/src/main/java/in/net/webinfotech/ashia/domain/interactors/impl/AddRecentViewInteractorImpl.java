package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.AddRecentViewInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.AddRecentlyViewedResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 23-08-2019.
 */

public class AddRecentViewInteractorImpl extends AbstractInteractor implements AddRecentViewInteractor{

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;
    int productId;

    public AddRecentViewInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId, int productId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.productId = productId;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRecentAddFailed();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRecentAddSuccess();
            }
        });
    }

    @Override
    public void run() {
        final AddRecentlyViewedResponse addRecentlyViewedResponse = mRepository.addRecentlyViewed(apiToken, userId, productId);
        if (addRecentlyViewedResponse == null) {
            notifyError();
        } else if (!addRecentlyViewedResponse.status) {
            notifyError();
        } else {
            postMessage();
        }
    }
}
