package in.net.webinfotech.ashia.presentation.ui.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.presentation.presenters.LoginPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.LoginPresenterImpl;
import in.net.webinfotech.ashia.presentation.ui.router.LoginRouter;
import in.net.webinfotech.ashia.threading.MainThreadImpl;

public class LogInActivity extends AppCompatActivity implements LoginPresenter.View, LoginRouter {


    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    LoginPresenterImpl mPresenter;
    @BindView(R.id.layoutLoader)
    View layoutLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        initialisePresenter();
    }

    public void initialisePresenter(){
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.btn_login) void onLoginClicked(){
        if(editTextPassword.getText().toString().trim().isEmpty() || editTextPhone.getText().toString().trim().isEmpty()){
            Toasty.warning(this, "Fields missing", Toast.LENGTH_SHORT).show();
        }else{
            mPresenter.checkLogin(editTextPhone.getText().toString(), editTextPassword.getText().toString());
            showLoader();
        }
    }

    @OnClick(R.id.txt_view_register) void onRegisterClicked() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToLogin() {
        Intent mainIntent = new Intent(this, MainActivity.class);
        startActivity(mainIntent);
    }
}
