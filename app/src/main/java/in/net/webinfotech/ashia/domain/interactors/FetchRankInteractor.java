package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.Ranking;

/**
 * Created by Raj on 27-08-2019.
 */

public interface FetchRankInteractor {
    interface Callback {
        void onGettingRankSuccess(Ranking ranking);
        void onGettingRankFail();
    }
}
