package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.PlaceOrderInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.OrderPlaceResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 19-08-2019.
 */

public class PlaceOrderInteractorImpl extends AbstractInteractor implements PlaceOrderInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int addressId;
    int walletStatus;

    public PlaceOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId, int addressId, int walletStatus) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.addressId = addressId;
        this.walletStatus = walletStatus;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderSuccess();
            }
        });
    }


    @Override
    public void run() {
        final OrderPlaceResponse response = mRepository.placeOrder(apiKey, userId, addressId, walletStatus);
        if (response ==  null) {
            notifyError("Something went wrong");
        } else if (!response.status) {
            notifyError("Something went wrong");
        } else {
            postMessage();
        }
    }
}
