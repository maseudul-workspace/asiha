package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.SubmitReviewInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.ReviewResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 26-07-2019.
 */

public class SubmitReviewInteractorImpl extends AbstractInteractor implements SubmitReviewInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    int userId;
    int productId;
    String apiToken;
    String comments;
    int rating;

    public SubmitReviewInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, int userId, int productId, String apiToken, String comments, int rating) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.userId = userId;
        this.productId = productId;
        this.apiToken = apiToken;
        this.comments = comments;
        this.rating = rating;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onReviewSubmitFail();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onReviewSubmitSuccess();
            }
        });
    }

    @Override
    public void run() {
        final ReviewResponse reviewResponse = mRepository.submitReview(userId, productId, rating, comments, apiToken);
        if(reviewResponse == null){
            notifyError();
        }else if(!reviewResponse.status){
            notifyError();
        }else{
            postMessage();
        }
    }
}
