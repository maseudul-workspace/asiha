package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.DeleteAddressInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.AddressDeleteResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 05-08-2019.
 */

public class DeleteAddressInteractorImpl extends AbstractInteractor implements DeleteAddressInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;
    int addressId;

    public DeleteAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId, int addressId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.addressId = addressId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressDeleteFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressDeleteSuccess();
            }
        });
    }

    @Override
    public void run() {
        final AddressDeleteResponse addressDeleteResponse = mRepository.deleteAddress(apiToken, userId, addressId);
        if(addressDeleteResponse == null) {
            notifyError("Something went wrong");
        } else if(!addressDeleteResponse.status) {
            notifyError("Something went wrong");
        } else {
            postMessage();
        }
    }
}
