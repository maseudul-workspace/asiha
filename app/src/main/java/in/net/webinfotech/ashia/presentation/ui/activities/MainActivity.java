package in.net.webinfotech.ashia.presentation.ui.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Handler;
import com.google.android.material.snackbar.Snackbar;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.domain.models.BrandsMain;
import in.net.webinfotech.ashia.domain.models.HomeSliders;
import in.net.webinfotech.ashia.domain.models.Testing.HomeHeaderSliders;
import in.net.webinfotech.ashia.domain.models.Testing.ProductList;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.presentation.presenters.MainActivityPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.MainActivityPresenterImpl;
import in.net.webinfotech.ashia.presentation.ui.adapters.BrandsMainAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.CategoryListSmallHorizontalAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListHorizontalAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.SlidingImagesAdapter;
import in.net.webinfotech.ashia.threading.MainThreadImpl;
import in.net.webinfotech.ashia.util.GlideHelper;

public class MainActivity extends BaseActivity implements MainActivityPresenter.View{

    @BindView(R.id.recycler_view_popular_items)
    RecyclerView recyclerViewPopularItems;
    @BindView(R.id.recycler_view_newest_arrivals)
    RecyclerView recyclerViewNewestArrivals;
    ArrayList<ProductList> productsListsBestSelling;
    ArrayList<ProductList> productListsNewestArrival;
    @BindView(R.id.image_slider_viewpager)
    ViewPager imgSliderViewPager;
    ArrayList<HomeHeaderSliders> headerSliders;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    MainActivityPresenterImpl mPresenter;
    @BindView(R.id.recycler_view_category_small_horizontal)
    RecyclerView recyclerViewCategorySmallHorizontal;
    AndroidApplication androidApplication;
    @BindView(R.id.nestedScrollView)
    ScrollView nestedScrollView;
    @BindView(R.id.img_view_promotion_image1)
    ImageView imgViewPromotionImage1;
    @BindView(R.id.img_view_promotion_image2)
    ImageView imgViewPromotionImage2;
    @BindView(R.id.img_view_promotion_image3)
    ImageView imgViewPromotionImage3;
    @BindView(R.id.layout_recently_viewed)
    View layoutRecentlyViewed;
    @BindView(R.id.recycler_view_recently_viewed)
    RecyclerView recentlyViewRecyclerView;
    ArrayList<BrandsMain> brandsMains;
    @BindView(R.id.recycler_view_brands)
    RecyclerView recyclerViewBrands;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        initialisePresenter();
        showUserInfo();
        mPresenter.getData();
        setBrandsMains();
    }

    public void showUserInfo(){
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if(userInfo != null){

        }
    }

    public void initialisePresenter(){
        mPresenter = new MainActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setBrandsMains() {
        brandsMains = new ArrayList<>();
        BrandsMain brandsMain1 = new BrandsMain("https://cdn.iconscout.com/icon/free/png-512/sega-3-283027.png");
        BrandsMain brandsMain2 = new BrandsMain("https://i.pinimg.com/originals/d8/1d/b3/d81db3bdfce08bd18f950c809a8109cc.jpg");
        BrandsMain brandsMain3 = new BrandsMain("https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Adidas_Logo.svg/1200px-Adidas_Logo.svg.png");
        BrandsMain brandsMain4 = new BrandsMain("https://www.clipartwiki.com/clipimg/detail/279-2791841_puma-logo-png.png");
        BrandsMain brandsMain5 = new BrandsMain("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhMTEhIVFRUVFhUYFRgXFxcVFRgXGBgYFxcVGBUYHSggGBslGxUXITEhJSkrLi4uFx8zODMtNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAKYBLwMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAABgcFCAECBAP/xABLEAABAwIDAwQMCgkDBQEAAAABAAIDBBEFEiEGBzFBUWGxEyIlMnFyc4GRkrLRCCMkM0JSg6HBwhQXNDVTY5Oz0hXD4SZDVGLxNv/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwC8UREBERAREQEREBERAREQEXF18KiujZ372t8LgOtB6EUdqtsadhIAe63KALea5WLm26NzlhFuQucb+gBBNrpdV27bSo5Az1T711btlU3+h6v/ACgsZFFMI2wa8hkrcribAjvdbWvzKVAoOUREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAXWR4AJOgGpXZebEIi+N7Rxc0gehBDMe2pkcSyA5WfWt2x0+5RqUueS4m5OpPKV9KqnLSWuuCDqFmdkBB2UiW17dpm4X86DH0mCzyAlkTiOc3bfXkvxWUpdjJnWLi1nQTc/cp8F2QQ2HYZv05SfALda5k2HZY2ldfkuNLqYFVnitbO2d+aWRpB4AuGnJpdB48Ywt1O/sbje+oIvqDw0+7zKysCB/R4bm942H0gFVnU1kkzw+RxJ0F+YBWhhcWSKNoNwGNF+ewCD1oiICIiAiIgIiICIiAiIgIi4ug5REQEREBERAREQEREBERAREQEREGOxXBopx24s4cHC1x71Fa/ZGVlzGQ8c3A+jlU7SyCu6PGKmmcGuDi23ePv9xPBSjDtqIJNHXjdzPtbm0cNFlaukZIMsjQ4dP4cyjlfsZG65jeWHkBGYenigkzJA4XaQRzggj0qFbdUgEjJB9IWJ6R08unUvm/AKyH5ok9LXWJ8xWPxTF5Xs7FOLua64JFiDYi2iDGgcFbFEe0Z4reoKqA3m51ZeDYlHKxmV4Lg0XHAg2F9EGTREQEREBERAREQEREBF1cbLrBJmaHWIuL2IsfOEH0XFlyiAiIgIi6vag7IiICIiAiIgIiICIiAiIgIi6SShouSAOc8EHddJJA0EkgAcSdAFHK/a1jTaJuf/2Js3zcpUXxLFZZic7jb6o0A81tUEnxnapjO1hs8/W0yj3qGVMpkcXuuXE3JK+1HQvkcGxtLjy9HuUnw/ZAWvM7X6reHpsghgUk2YwWR5bLmMbQRa3E2PDXkXhxuhEMzmC+XQi+psQpVsZNmhLfquPoOoQZ8LlEQEREBERAREQEREBERAREQEREBERARFHtrNsKbD+x/pGf4zNlyNzd7a99R9YIJCirs75cL55v6R964O+bC+ef+kfegsVFBaPexhb+M5j8dpas/gu1dFVG1PURvd9UGzvVOpQZtFxdcoCIiAvhVVTIxme4NHOV91gtpsJkqMgY4ANve/TbXggxuKbXC1oQb8rnDqCjVZWyTG73lx4dHgsFMKLZGJvzhLjfgNBx9PBZemwqGPvImjptc+koIJRbO1ElyG5BzvuL+AEXKkuGbKRMA7J27tOho6LcvnUjUOxzeRh9JM+CeR4kZbMBG5w1APEDpQS2KFrRZrQBzAAL6WVf/rgwn+LJ/Sf7lKdmdooK6Hs1OXFmZzbuaWm7eOhQYvbenNmSC+lwePhH4r4bCnt5ddMo6RxUrqqRkjcr2hw42K60dDHELRsDRy295QelERAREQEREBEXBKDlFxdcoCIuLoOUXXMEzIOyLqSuQg5REQFTfwhj2tJ9r+RXIqb+EQe1o/DL+RBSBclz4fvXKle6oA4pSggEZnceHzb0ETJX2oqp0b2yRuLXtN2uHEFT3flRxx17OxsazNC1zsoAu7O8XNuWwCroINqd2u0Tq2ijkkPxgGV/S4EjNbkvxUsVQfB+Pyao8qPZCze87b80EYjiAdPIDludGjTtiOXoCCwJZ2t1c4DwkBeT/Wqb/wAiL12+9alYtj1TUPL5ZpHEknVzra8wvayxYIvw1QbqRTtd3rg7wEHqXcFadYdjVRA4PhnkYQRwe4cOi9leu63eSawCnqPnwO1dySAAknhobBBaF15avEoYvnJWM8ZwHWobvW2qfR0Z7EbSS3Y1wOrbg3cPB+IWttdXSSuLpZHvcdSXuLifSg3EpcRhkF45GPGurXA8OPBazb2n91qzx4/7MR96i+G4rNTvZJDI6NzDcEG2vLpw15dNV9tocafVzvqJAA6TJmsdLtYxl9ecMv50HgzLYrcM4f6b9rL7S1xuthNxcnc77WX2ggtIvC6yVLGi7nADnJsqZ3kb0HxPfS0mjm2D5b6g8SGdI0VPV2LVEpJlnleXcc0jnX8Nyg2/GKwfxWeu33r1NkB4G60rZJbUaHo0t7lmcG2prKZ4fHUS6G5aXuc0+EEkINvbro6doNiQPCofsBtg3EKfslgyRptIwG9jyEdBVRb66p7cSdle5o7FEdHEDgeZBsb2dvOPSuGztPAhaafp0v8AFk9d3vWcoNr5oKSSnjc4OleHPkLjmAAADW8oGhKDaKfGoGOs+aNvhcAV9W1DXag36QbrTiWdztXOLjzk3PpKkOyG2NRRTNeHvfHwfGXOLSOi/A9KDaZ1QALkgDnOn3rzsxuBxytmjJ5g9t+tay7X7bVFbIXF72RguyRhxAtfQnndYaqMtkI1BIPOCQfSg3L/AEhcOmVIbmtr5XSGjmc94IzRlzi4tDRq3Xk1CsfazaaOhp3SvGY8Gt+s7mvyIJIZgF8pMUiGhlYLcbuC1d2j2vq6uQvfM9ovdrGucGt4cLW5gsDLOSbuJJ5yb/eUG40Va13ekEW5CCvo2Vae02JTR2Mc0jLcMr3Nt6CrP2A3pyNeyCtOdriGtlPfAkgDNzi54oL5a+65JXkik0uOVfaPwoPQqa+EV3tF4Zv9tXKVTXwiu9ovDP1RoKQWd2IxeOkroKiW+SMuLsozHVrgLDzrBXXenp3yODI2ue48GtBcTymwGp0B9CCUbydqo8QqRLG0hjWBjc2jiAS65HJ3yiQK7zwPY4te1zXDiHAgjoIKzOy+J00EgNRSCcXBuXkFvgFrHz8yC4txNC9lE+R2jZZMzOkAZSfBcFVpvcrjJiU4uSGZWgc1mgkfer/2WxOCopYpKYBseUBrBYZLfQsNARzLXLeT+86zyo9hiCMlXTgeEwnZ2R3YmZ3RyOc6wzX4XB5NGgebpVLq9tnR/wBPEcfipPzG6CiXLMbG1Do62nc02PZB6DcH7iVhnL24GflEPjt60Fsb/wB92Uep1M3VGqZJVv793fF0P2/3CL3qnygsDcpRRy1zhKxrwInEBwuL3GuqxO86lZFidWyMBrQ5hAGgGaKN5t0Xc5Z7cT+3v8i7rCwu9Y91avxo/wCzEgiCvzcobYY480kqoNX9uRYThrgOWSUelBRVZK573Od3ziSfCSSfvX0wiPNUQNPB0sQPneArQn3KSkk/pjB9mf8AJYmp2IpqGRkk2KQ5o3Nf2MNu92R18oAdfXLa/BBld9mDxMbTyxxsYdWnKA2/KLgc1j6VUimu8Tbj9PLGsjLI472ubucTykcAoUgtPcJOf0iobftTG0kcl81h+PpWM32Dui7yUX4r0bi5CKySx4sF+kar4b7B3R+yj/Mgr4lXBuewqGSjqXPja5xcRcgE6MGguqfKvDcYb0k/RJ+QaIKXrI8r3N5iR968/Kvbi5+Ok8d3WvFdBdW3OEQswWIsjaC1kBa7KM2oAJLrcbE38Kpcq+Nu/wBytH8uH2W+hUM9BK91kxbiUFvpZ2nwZHHrCl+/Krdmp4/o2c7z6DqUK3aO7p0vju/tvUr35H4+DxHdYQVgSrG3K0MUs1T2VjXgRNAzAEauN7X5dFXBVo7ivnanxG+0gge0cDWVVQxoDWtlkaAOAAeQAFjr8qy21w+W1XlpPaKxACDaDd3XOmw+me4knJYk6k5SW3PqqVxlQfdIe5lN4H/3HqcxhB6Sqb+EUO0ovDP1Rq5FTvwifm6Pxp/9tBRpUo3YfvWj8d/3xSKMKVbrR3VovHd/aegkW/qFrayIgAXhBNha5zu1KrAK09/7flkHkB7b1VrUF57hnH9FnGukot0Xbqqv3jm+J1vlfytVofB/1pqkfzh7AVZby4y3Equ44yXHgIFvx9CCMFXxs3/+ePkZOo9Cocq+dnG/9OnyD+ooKFdxXswYfHxeO3rXkcNSsnstFmrKcc8rOu34oLL38jtMP8FR1Qqn3K4t/wC2woPBU/7Cp1BZG4j9uk8i72gsNvY/elV4zP7UYWa3Eft0nkfzBYTesO6lV4zf7bEEPAWwG4s2w53N2aTrC1/Kv7cj+7HeVk/BBX23G8SpqpHsikMcAJDQwkFwF7OLhrrobBQbVzudzj5ySeX0/eugOg8AXrwZrTUQZuHZY735swv9yCQbQ7EyUdJHPM/4yR3eDg1pBOptx0UUKu3foQKeAaauv6LjiqSKCxtxn7bJ4g/Mum+794fZR/ivruLb8sk8QfmXz34N7o/ZR/mQV2rv3Fn5JP5QewFSBV47im/JZ9P+4PYCCmsV+dk8d/tFeJ/Ar24r89J47/aK8b+BQX5tz+5R5OLqaqFcr927b3G8EcXUFQTigku7Qd06Xx3ew9Srfj+0QeTf1hRbdmO6VN4x9lyle/WO09MddYpL+sxBVxVobivnKnxG9ZVXuCtLcS34yp8VnWUEH2vHy2q8tJ1lYayzG1p+W1XlpPaKxACDY/c+e5dN9r/dep5E0qE7oqcswumBHESO8zpHuB9BCnjBog+ip/4Q/wA3R+NP1Rq4FgNq9kabEBGKjPaMuLcrsvfZb30170INTbKW7qv3rR+O72HK4huewzml9cf4r3YNuyoKaZk8Qlzxm7bvBF7EajL0oK1+EEz5ZB5H87lVQC2s2o2Fo6+RslQHlzW5RlflFrk8LdKww3P4X9ST1/8AhBgvg8s+TVXlm+wFhN+WycjZhWxtc5jxaWwvkcAACTzEdGllbey2ytNQNkZTBwEjg52Z2bUC3UFmJoWvaWuaHNOhBFwRzEFBpi5qztHtfVx0r6Nkg7C+4IIBIBGUta7iBZXvjG6TDp3Fwa+Ik3+LIDfM21gsX+o+h/j1HpZ/ig1+Vnbl9jnzVDauRrmxRG7bjR7tRoTzedWNg+6LDYXBzmvmI/iG7fVFgp5FC1oDWgADgALAeZBVu/jBHSUsUzGlxhc7Nb6LHt7Zx87GDzqgC1bpSxBwLXAEEWIOoI5iFX+L7oMPnkMg7JFmNy1hGW/QCNEFBbN4/NQzdmgIzWsQRcOHMQvPjVdLUTPmm7+Q5jpl4gWsOQWsticI3RYbA8PLHykWsJHXaCCDewAB4cvOvpje6qgqpnzyGVrn5bhjmtYMrWsAa3LoLNCDWPKtgdxkN8NeAP8AuvHVdez9SuG/Wn9dv+Kl+ymzENBCYYC8tLi7tyCbnwAINTsTw58Ej4ngh0Zym4twNr25ivhE4tIcOIII5dQbhbS7XbvqOvJfI0sltbsjLZtL2uDoeKif6iqXT5VN6rPcgqbafamrrmMM9i2PS7W2Fzz9KjlltJPu1oHUzaXK9sbXZyWkBznWtdxtrosWNzOGfzvXH+KCutxDflknie9fLfm3uj9jH+ZXFsxu8o6GTssHZMxFu2cCPQAuNpd3VFXTdmn7JnyhvauAFhe2ljzoNWC1XnuIHySfyg9lZ39TOGfzvXH+Kk+y2yVPQRvjp8+V5uczs2tracyDVTFm/HSeO/2ivE5unpWy026LDnOc4tkJJJ7/AJTrzLp+p3Dfqyev/wAIMft4zuJf+VD1NWvzgtucR2ZgnphSyZuxgNbobOs3hr5lF37m8LP0Jf6rh1IKW3YM7p0vjH2XK2t7ezL6mlEkbSZIbmwFyWmwcOnQXsszg+7HD6aZk8TJBIw3beRzhexGoJtyqZmMWQaZPjIOt1mtltpJqF8j4Mt3tynMLjS5BHnK2G2k3eUVXmc+LJIQe3Z2rr8hPPZRD9R8H/lS+qz3IKUq6l0j3yPN3PcXE85JuVktl9npqydscTCRduZwGjWk6kngNNVcFHuSpWvBkqJnj6tmt++ysTAtn6elZkp4msbpew1NuBceUoO2D4Y2CGOGMWZG0Nby6Dgsi0FdwEAQdkREBERAREQEREBERAREQEREBERAREQF1kbcEHlFtND5iOC7IUGDbhTezFuaWwa11uyPOtyOU8NF3gqHsBJF2mYt4ku7Z2UaW5D08FlREMxdykW8wv718zSttb/2zee9+tBjhjN7lrCQHZeDsxsbEiwtz8q9WMXMTrXHDhcHiOUcF3OGsvygXuWgkNJ5yF6ZIwRY8EGNpKZsYLgJBYE9vIS0+lxA4cVw3EnBwaQCXNc5uUk8BfLcix8PWslLEHNLSLgggjoOi8gwplwTcloLW3JNgdLIPNNWuMcly0PDbtFyDx+q4AjhxXapqXsY/sjQbMzANJHA2IJ84Xq/05mua7iRa5NyBzA8y+lRSNeCHcrS08mhQeKLECWOe1oyMuDcnMcvGwt/9Xd1ZIQXMa3I0AklxuRbN3oGnpX2dhzL31HOA4hptzjlX2/R22cORwsfBa3mQdXvvGTe123ueAuOKwjWZAxzg4aj40OzNdfhpfgVIBGLZeS1vNwXlGGMuNXEDg0ucWjm7XhyIMTS1j2h30y6YsAJNh519Z8byvLMurbZrBxuTrYZQdfCskcOZe9j3+fjpmta9l1nw5rnF13NJFnZTYOHSg8Dsd7YhrCLBpOYOub2uBYHX3Fe6mne597WaWNNiddb8RbTgu0mHNJuC5pIscriARzEefiLHpXEVN29+2aGgNFnXDmgX1HQSUHuRAuHBByiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiIOpXIREHKIiD//2Q==");
        BrandsMain brandsMain6 = new BrandsMain("https://www.pnglot.com/pngfile/detail/230-2309107_black-levis-logo.png");
        BrandsMain brandsMain7 = new BrandsMain("http://logok.org/wp-content/uploads/2014/11/Lee-logo-880x660.png");
        BrandsMain brandsMain8 = new BrandsMain("https://www.fulsport.pl/pol_pl_Rakietka-do-tenisa-stolowego-Butterfly-Timo-Boll-500-raczka-anatomiczna-2780_3.jpg");
        BrandsMain brandsMain9 = new BrandsMain("https://cdn.dribbble.com/users/898482/screenshots/3477141/roadsterlogo.jpg");
        brandsMains.add(brandsMain1);
        brandsMains.add(brandsMain2);
        brandsMains.add(brandsMain3);
        brandsMains.add(brandsMain4);
        brandsMains.add(brandsMain5);
        brandsMains.add(brandsMain6);
        brandsMains.add(brandsMain7);
        brandsMains.add(brandsMain8);
        brandsMains.add(brandsMain9);
        BrandsMainAdapter brandsMainAdapter = new BrandsMainAdapter(this, brandsMains);
        recyclerViewBrands.setAdapter(brandsMainAdapter);
        recyclerViewBrands.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewBrands.setNestedScrollingEnabled(false);
    }

    @Override
    public void loadCategoryAdapter(CategoryListSmallHorizontalAdapter adapter) {
        recyclerViewCategorySmallHorizontal.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewCategorySmallHorizontal.setAdapter(adapter);
    }

    @Override
    public void loadData(final HomeSliders[] homeSliders, HomeSliders[] promotionSliders, ProductListHorizontalAdapter newestArrivalsAdapter, ProductListHorizontalAdapter bestSellingAdapter) {

//        Set up slider
        SlidingImagesAdapter slidingImagesAdapter = new SlidingImagesAdapter(this, homeSliders);
        imgSliderViewPager.setAdapter(slidingImagesAdapter);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == homeSliders.length) {
                    currentPage = 0;
                }
                imgSliderViewPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 2500);

//        Set up promotional images
        try {
            GlideHelper.setImageView(this, imgViewPromotionImage1, promotionSliders[0].imgPath);
            GlideHelper.setImageView(this, imgViewPromotionImage2, promotionSliders[1].imgPath);
            GlideHelper.setImageView(this, imgViewPromotionImage3, promotionSliders[2].imgPath);
        } catch (Exception e) {

        }


//        Set newest arrival products
        recyclerViewNewestArrivals.setAdapter(newestArrivalsAdapter);
        recyclerViewNewestArrivals.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

//        Set popular products
        recyclerViewPopularItems.setAdapter(bestSellingAdapter);
        recyclerViewPopularItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

    }


    @Override
    public void goToSubCategoryActivity(ImageView imageView, int categoryId, String categoryName, String categoryImage) {
        Intent intent = new Intent(this, SubCategoryActivity.class);
        intent.putExtra("categoryId", categoryId);
        intent.putExtra("categoryImage", categoryImage);
        intent.putExtra("categoryTitle", categoryName);

        Pair[] pairs = new Pair[1];
        pairs[0] = new Pair<View, String> (imageView, "imageTransition");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
            startActivity(intent, options.toBundle());
        }else{
            startActivity(intent);
        }
    }

    @Override
    public void loadCartCount(int count) {
    }

    @Override
    public void loadRecentlyViewed(ProductListHorizontalAdapter adapter) {
        layoutRecentlyViewed.setVisibility(View.VISIBLE);
        recentlyViewRecyclerView.setAdapter(adapter);
        recentlyViewRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }


    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(nestedScrollView, "You must be logged in", Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(cartIntent);
                finish();
            }
        });
        snackbar.show();
    }

    public void goToCartActivity(){
        Intent carIntent = new Intent(this, CartActivity.class);
        startActivity(carIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getCartDetails();
        mPresenter.fetchRecentlyViewed();
        mPresenter.fetchWishList();
    }

}
