package in.net.webinfotech.ashia.presentation.presenters;

import in.net.webinfotech.ashia.presentation.ui.adapters.SubcategoryGridAdapter;

/**
 * Created by Raj on 15-07-2019.
 */

public interface SubCategoryPresenter {
    void getSubCategoriesList(int id);
    interface View{
        void loadAdapter(SubcategoryGridAdapter adapter);
        void showLoader();
        void hideLoader();
    }
}
