package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;

import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetWalletAmountInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetWalletHistoryInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.GetWalletAmountInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetWalletHistoryInteractorImpl;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.domain.models.WalletAmount;
import in.net.webinfotech.ashia.domain.models.WalletHistory;
import in.net.webinfotech.ashia.presentation.presenters.WalletPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.ui.adapters.WalletHistoryAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 10-08-2019.
 */

public class WalletPresenterImpl extends AbstractPresenter implements WalletPresenter, GetWalletAmountInteractor.Callback, GetWalletHistoryInteractor.Callback {

    Context mContext;
    WalletPresenter.View mView;
    AndroidApplication androidApplication;
    GetWalletAmountInteractorImpl getWalletAmountInteractor;
    GetWalletHistoryInteractorImpl getWalletHistoryInteractor;
    WalletHistoryAdapter adapter;

    public WalletPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWalletAmount() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);

        if(userInfo != null) {
            getWalletAmountInteractor = new GetWalletAmountInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            getWalletAmountInteractor.execute();
        }

    }

    @Override
    public void fetchWalletHistories() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);

        if(userInfo != null) {
            getWalletHistoryInteractor = new GetWalletHistoryInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            getWalletHistoryInteractor.execute();
        }
    }

    @Override
    public void onGettingWalletAmountSuccess(WalletAmount walletAmount) {
        mView.loadWalletAmount(walletAmount.totalAmount);
    }

    @Override
    public void onGettingWalletAmountFail(String errorMsg) {

    }

    @Override
    public void onGettingWalletHistorySuccess(WalletHistory[] walletHistories) {
        adapter = new WalletHistoryAdapter(mContext, walletHistories);
        mView.loadWalletHistoriesAdapter(adapter);
    }

    @Override
    public void onGettingWalletHistoryFail(String errorMsg) {

    }
}
