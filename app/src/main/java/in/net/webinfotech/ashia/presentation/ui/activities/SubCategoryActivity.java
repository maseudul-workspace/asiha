package in.net.webinfotech.ashia.presentation.ui.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.presentation.presenters.SubCategoryPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.SubCategoryPresenterImpl;
import in.net.webinfotech.ashia.presentation.routers.SubCategoryRouter;
import in.net.webinfotech.ashia.presentation.ui.adapters.SubcategoryGridAdapter;
import in.net.webinfotech.ashia.threading.MainThreadImpl;
import in.net.webinfotech.ashia.util.GlideHelper;

public class SubCategoryActivity extends AppCompatActivity implements SubCategoryPresenter.View, SubCategoryRouter {

    @BindView(R.id.img_view_category)
    ImageView imgViewCategoryImage;
    @BindView(R.id.txt_view_category_name)
    TextView txtViewCategoryName;
    @BindView(R.id.recycler_view_sub_category)
    RecyclerView recyclerViewSubCategory;
    String categoryTitle;
    String categoryImage;
    int categoryId;
    SubCategoryPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        ButterKnife.bind(this);
        categoryTitle = getIntent().getStringExtra("categoryTitle");
        categoryImage = getIntent().getStringExtra("categoryImage");
        categoryId = getIntent().getIntExtra("categoryId", 1);
        getSupportActionBar().setTitle(categoryTitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtViewCategoryName.setText(categoryTitle);
        GlideHelper.setImageView(this, imgViewCategoryImage, categoryImage);
        initialisePresenter();
        mPresenter.getSubCategoriesList(categoryId);
    }

    public void initialisePresenter(){
        mPresenter = new SubCategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @Override
    public void loadAdapter(SubcategoryGridAdapter adapter) {
        recyclerViewSubCategory.setAdapter(adapter);
        recyclerViewSubCategory.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewSubCategory.setNestedScrollingEnabled(false);
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void goToProductListActivity(int id) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("subCategoryId", id);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
