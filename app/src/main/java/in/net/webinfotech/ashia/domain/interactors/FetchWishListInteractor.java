package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.ProductList;

/**
 * Created by Raj on 23-08-2019.
 */

public interface FetchWishListInteractor {
    interface Callback {
        void onGettingWishListSuccess(ProductList[] productLists);
        void onGettingWishListFail(String errorMsg);
    }
}
