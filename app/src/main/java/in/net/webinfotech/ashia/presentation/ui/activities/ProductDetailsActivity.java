package in.net.webinfotech.ashia.presentation.ui.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import com.google.android.material.snackbar.Snackbar;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.domain.models.ProductDetails;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.presentation.presenters.ProductDetailsPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.ProductDetailsPresenterImpl;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductDetailsImagesAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListHorizontalAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ReviewsAdapter;
import in.net.webinfotech.ashia.presentation.ui.dialogs.ReviewDialog;
import in.net.webinfotech.ashia.threading.MainThreadImpl;
import in.net.webinfotech.ashia.util.GlideHelper;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailsPresenter.View, ReviewDialog.Callback {

    @BindView(R.id.image_slider_viewpager)
    ViewPager viewPager;
    @BindView(R.id.dots_indicator_layout)
    LinearLayout dotsLayout;
    int sliderPosition = 0;
    @BindView(R.id.txt_view_price)
    TextView txtViewPrice;
    @BindView(R.id.txt_view_product_description)
    TextView txtViewProductDescription;
    @BindView(R.id.recycler_view_related_products)
    RecyclerView recyclerViewRelatedProducts;
    @BindView(R.id.recycler_view_reviews)
    RecyclerView recyclerViewReviews;
    @BindView(R.id.txt_view_qty)
    TextView txtViewQty;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.txt_view_product_name)
    TextView txtViewProductName;
    @BindView(R.id.txt_view_selling_price)
    TextView txtViewSellingPrice;
    @BindView(R.id.txt_view_total_rating)
    TextView txtViewTotalRating;
    int quantity = 1;
    int imageCount;
    int productId;
    ProductDetailsPresenterImpl mPresenter;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.layoutLoader)
    View layoutLoader;
    ReviewDialog reviewDialog;
    @BindView(R.id.relative_layout)
    RelativeLayout relativeLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_view_cart_count)
    TextView txtViewCartCount;
    @BindView(R.id.btn_add_cart)
    Button btnAddToCart;
    @BindView(R.id.img_view_heart_outline)
    ImageView imgViewHeartOutline;
    @BindView(R.id.img_view_heart_fill)
    ImageView imgViewHeartFill;
    int totalQty = 1;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        txtViewPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            txtViewProductDescription.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }
        productId = getIntent().getIntExtra("productId", 0);
        initialisePresenter();
        showLoader();
        initialiseReviewDialog();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrow_white);
    }

    public void initialiseReviewDialog(){
        reviewDialog = new ReviewDialog(this, this, this);
    }

    public void initialisePresenter(){
        mPresenter = new ProductDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void prepareDotsIndicator(int sliderPosition){
        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[this.imageCount];
        for(int i = 0; i < this.imageCount; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(12, 12);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsLayout.addView(dots[i], layoutParams);

        }
    }



    @OnClick(R.id.btn_add_qty) void addQtyClicked(){
        if(quantity < totalQty) {
            quantity++;
            txtViewQty.setText(Integer.toString(quantity));
        }
    }

    @OnClick(R.id.btn_remove_qty) void removeQtyClicked(){
        if(quantity != 1){
            quantity--;
            txtViewQty.setText(Integer.toString(quantity));
        }
    }

    @Override
    public void loadProductDetails(ProductDetails productDetails) {
        mainLayout.setVisibility(View.VISIBLE);
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if(userInfo != null){
            reviewDialog.setUpDialog(userInfo.name, productDetails.productName);
        }
        txtViewProductName.setText(productDetails.productName);
        txtViewProductDescription.setText(Html.fromHtml(productDetails.productDescription));
        ratingBar.setRating(productDetails.rating);
        txtViewPrice.setText("Rs. " + Double.toString(productDetails.price));
        txtViewSellingPrice.setText("Rs. " + Double.toString(productDetails.sellingPrice));
        txtViewTotalRating.setText(Integer.toString(productDetails.reviews.length));
        getSupportActionBar().setTitle(productDetails.productName);
        ProductList[] wishLists = androidApplication.getWishList(this);
        if (wishLists != null) {
            for(int i = 0; i < wishLists.length; i++) {
                if (wishLists[i].id == productDetails.productId) {
                    imgViewHeartFill.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
        totalQty = productDetails.stock;
        if(productDetails.stock == 0) {
            btnAddToCart.setEnabled(false);
            btnAddToCart.setText("Out Of Stock");
        }
        mPresenter.fetchRelatedProducts(productDetails.brandId);

    }

    @Override
    public void loadProductImagesAdapter(ProductDetailsImagesAdapter adapter, int imageCount) {
        this.imageCount = imageCount;
        prepareDotsIndicator(0);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                sliderPosition = position;
                prepareDotsIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void loadReviewsAdapter(ReviewsAdapter reviewsAdapter) {
        recyclerViewReviews.setAdapter(reviewsAdapter);
        recyclerViewReviews.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewReviews.setNestedScrollingEnabled(false);
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void hideDialog() {
        reviewDialog.hideLoader();
        reviewDialog.hideDialog();
    }

    @Override
    public void loadCartCount(int cartCount, boolean isCartPresent) {
        txtViewCartCount.setText(Integer.toString(cartCount));
        if(isCartPresent){
            btnAddToCart.setText("Added To Cart");
            btnAddToCart.setEnabled(false);
        }
    }

    @Override
    public void onWishListAddSuccess() {
        imgViewHeartFill.setVisibility(View.VISIBLE);
        imgViewHeartOutline.setVisibility(View.GONE);
    }

    @Override
    public void onWishListRemovedSuccess() {
        imgViewHeartFill.setVisibility(View.GONE);
        imgViewHeartOutline.setVisibility(View.VISIBLE);
    }

    @Override
    public void loadRelatedProducts(ProductListHorizontalAdapter adapter) {
        recyclerViewRelatedProducts.setAdapter(adapter);
        recyclerViewRelatedProducts.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void onCartAddedFail() {
        btnAddToCart.setEnabled(true);
        btnAddToCart.setText("Add To Cart");
    }

    @Override
    public void goToPreviewImage(String img) {
        Intent intent = new Intent(this, ImageViewActivity.class);
        intent.putExtra("imageUrl", img);
        startActivity(intent);
    }

    @OnClick(R.id.btn_submit_review) void onReviewClicked(){
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) == null){
            showLogInSnackbar();
        }else{
            reviewDialog.showDialog();
        }
    }

    @Override
    public void onSubmitClicked(float rating, String comment) {
        mPresenter.submitReview(productId, (int) rating, comment);
    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(relativeLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(cartIntent);
                finish();
            }
        });
        snackbar.show();
    }

    @OnClick(R.id.btn_add_cart) void onAddToCartClicked(){
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) == null){
            showLogInSnackbar();
        }else{
            mPresenter.addToCart(productId, quantity);
            btnAddToCart.setText("Please Wait...");
            btnAddToCart.setEnabled(false);
        }
    }

    public void goToCartActivity(){
        Intent carIntent = new Intent(this, CartActivity.class);
        startActivity(carIntent);
    }

    @OnClick(R.id.layout_cart) void onCartClicked() {
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) == null){
            showLogInSnackbar();
        }else{
            goToCartActivity();
        }
    }

    @OnClick(R.id.img_view_heart_outline) void onHeartOutlineClicked() {
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) == null){
            showLogInSnackbar();
        }else{
            mPresenter.addToWishList(productId);
        }
    }

    @OnClick(R.id.img_view_heart_fill)void onHeartFillClicked() {
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) == null){
            showLogInSnackbar();
        }else{
            mPresenter.deleteFromWishList(productId);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if(userInfo != null){
            mPresenter.getCartDetails(productId);
        }
        mPresenter.getProductDetails(productId);
    }

}
