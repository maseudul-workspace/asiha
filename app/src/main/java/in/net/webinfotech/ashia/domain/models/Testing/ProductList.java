package in.net.webinfotech.ashia.domain.models.Testing;

/**
 * Created by Raj on 18-06-2019.
 */

public class ProductList {

    public String productName;
    public String imageUrl;
    public double price;
    public int rating;
    public boolean isWishlistPresent;
    public String brands;

    public ProductList(String productName, String imageUrl, double price, int rating, boolean isWishlistPresent, String brands) {
        this.productName = productName;
        this.imageUrl = imageUrl;
        this.price = price;
        this.rating = rating;
        this.isWishlistPresent = isWishlistPresent;
        this.brands = brands;
    }
}
