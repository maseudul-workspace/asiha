package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.BrandsMain;
import in.net.webinfotech.ashia.util.GlideHelper;

public class BrandsMainAdapter extends RecyclerView.Adapter<BrandsMainAdapter.ViewHolder> {

    Context mContext;
    ArrayList<BrandsMain> brandsMains;

    public BrandsMainAdapter(Context mContext, ArrayList<BrandsMain> brandsMains) {
        this.mContext = mContext;
        this.brandsMains = brandsMains;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_brands_main, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.imgViewBrand, brandsMains.get(position).image);
    }

    @Override
    public int getItemCount() {
        return brandsMains.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_brand)
        ImageView imgViewBrand;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
