package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.AddAddressInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.AddAddressResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 02-08-2019.
 */

public class AddAddressInteractorImpl extends AbstractInteractor implements AddAddressInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;
    String state;
    String city;
    String postalCode;
    String phoneNo;
    String landmark;

    public AddAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId, String state, String city, String postalCode, String phoneNo, String landmark) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.state = state;
        this.city = city;
        this.postalCode = postalCode;
        this.phoneNo = phoneNo;
        this.landmark = landmark;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressAddFailed(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressAddSuccess();
            }
        });
    }

    @Override
    public void run() {
        final AddAddressResponse addAddressResponse = mRepository.addAddress(apiToken, userId, state, city, postalCode, phoneNo, landmark);
        if(addAddressResponse == null) {
            notifyError("Something went wrong");
        } else if(!addAddressResponse.status){
            notifyError("Something went wrong");
        } else {
            postMessage();
        }
    }
}
