package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 10-08-2019.
 */

public class WalletHistory {

    @SerializedName("transactionType")
    @Expose
    public int transactionType;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("totalAmount")
    @Expose
    public String totalAmount;

    @SerializedName("comment")
    @Expose
    public String comment;

    @SerializedName("date")
    @Expose
    public String date;

}
