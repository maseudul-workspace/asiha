package in.net.webinfotech.ashia.presentation.ui.activities;

import android.content.Intent;
import androidx.annotation.LayoutRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.UserInfo;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    androidx.appcompat.widget.Toolbar toolbar;
    Drawer drawer;
    AndroidApplication androidApplication;
    @BindView(R.id.constraintLayout)
    ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    protected void inflateContent(@LayoutRes int inflatedResID) {
        setContentView(R.layout.activity_base);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflatedResID, contentFrameLayout);
        ButterKnife.bind(this);
        setDrawer();
    }

    public void setDrawer(){
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.colorPrimaryDark)
                .addProfiles(new ProfileDrawerItem().withIcon(R.drawable.logo_white).withName("Ashia"))
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .withHasStableIds(true)
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Home").withIcon(R.drawable.home_color_accent).withSelectable(false).withIdentifier(1),
                        new PrimaryDrawerItem().withName("My Cart").withIcon(R.drawable.cart_color_accent).withSelectable(false).withIdentifier(4),
                        new PrimaryDrawerItem().withName("My Orders").withIcon(R.drawable.orders_color_accent).withSelectable(false).withIdentifier(5),
                        new PrimaryDrawerItem().withName("My Wishlist").withIcon(R.drawable.heart_color_accent).withSelectable(false).withIdentifier(11),
                        new PrimaryDrawerItem().withName("Add Address").withIcon(R.drawable.address_color_accent).withSelectable(false).withIdentifier(10),
                        new PrimaryDrawerItem().withName("About").withIcon(R.drawable.about_color_accent).withSelectable(false).withIdentifier(9)
                ).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if(drawerItem != null) {
                            switch (((int) drawerItem.getIdentifier())) {
                                case 8:
                                    Intent loginIntent = new Intent(getApplicationContext(), LogInActivity.class);
                                    startActivity(loginIntent);
                                    break;
                                case 7:
                                    androidApplication = (AndroidApplication) getApplicationContext();
                                    androidApplication.setUserInfo(getApplicationContext(), null);
                                    drawer.addItem(new PrimaryDrawerItem().withName("Log In").withIcon(R.drawable.login_outline).withIdentifier(8).withSelectable(false));
                                    drawer.removeItem(7);
                                    Toasty.success(getApplicationContext(), "Successfully Logged Out", Toast.LENGTH_SHORT).show();
                                    Intent homeIntent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(homeIntent);
                                    finish();
                                    break;
                                case 4:
                                    if(isLoggedIn()){
                                        Intent cartIntent = new Intent(getApplicationContext(), CartActivity.class);
                                        startActivity(cartIntent);
                                    }else{
                                        showLogInSnackbar();
                                    }
                                    break;
                                case 10:
                                    if(isLoggedIn()){
                                        Intent addAddressActivity = new Intent(getApplicationContext(), AddressActivity.class);
                                        startActivity(addAddressActivity);
                                    }else{
                                        showLogInSnackbar();
                                    }
                                    break;
                                case 5:
                                    if(isLoggedIn()){
                                        Intent ordersActivity = new Intent(getApplicationContext(), OrderListActivity.class);
                                        startActivity(ordersActivity);
                                    }else{
                                        showLogInSnackbar();
                                    }
                                    break;
                                case 11:
                                    if(isLoggedIn()){
                                        Intent wishListActivity = new Intent(getApplicationContext(), WishListActivity.class);
                                        startActivity(wishListActivity);
                                    }else{
                                        showLogInSnackbar();
                                    }
                                    break;

                            }
                        }
                        return false;
                    }
                })
                .build();

//        Checking user login
        androidApplication = (AndroidApplication) getApplicationContext();

        if(androidApplication.getUserInfo(this) == null){
            drawer.addItem(new PrimaryDrawerItem().withName("Log In").withIcon(R.drawable.login_outline).withSelectable(false).withIdentifier(8));
        }else{
            drawer.addItem(new PrimaryDrawerItem().withName("Log Out").withIcon(R.drawable.logout).withSelectable(false).withIdentifier(7));
        }

        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(constraintLayout, "You must be logged in", Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(cartIntent);
                finish();
            }
        });
        snackbar.show();
    }

    public boolean isLoggedIn(){
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if(userInfo == null){
            return false;
        }else {
            return true;
        }
    }

}
