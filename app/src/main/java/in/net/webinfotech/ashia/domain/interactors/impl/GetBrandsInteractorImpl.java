package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetBrandsInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.Brands;
import in.net.webinfotech.ashia.domain.models.BrandsWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 24-07-2019.
 */

public class GetBrandsInteractorImpl extends AbstractInteractor implements GetBrandsInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    int subCategoryId;

    public GetBrandsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, int subCategoryId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.subCategoryId = subCategoryId;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBrandsFail();
            }
        });
    }

    private void postMessage(final Brands[] brands){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBrandsSuccess(brands);
            }
        });
    }

    @Override
    public void run() {
        final BrandsWrapper brandsWrapper = mRepository.getBrands(subCategoryId);
        if(brandsWrapper == null){
            notifyError();
        }else if(!brandsWrapper.status){
            notifyError();
        }else{
            postMessage(brandsWrapper.brands);
        }
    }
}
