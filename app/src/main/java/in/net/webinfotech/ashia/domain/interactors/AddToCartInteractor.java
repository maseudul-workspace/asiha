package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 01-08-2019.
 */

public interface AddToCartInteractor {
    interface Callback{
        void onAddToCartSuccess();
        void onAddToCartFail(String errorMsg);
    }
}
