package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.CancelOrderInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.CancelOrderResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 22-08-2019.
 */

public class CancelOrderInteractorImpl extends AbstractInteractor implements CancelOrderInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;
    String orderId;

    public CancelOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId, String orderId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.orderId = orderId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderCancelFailed(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderCancelSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CancelOrderResponse cancelOrderResponse = mRepository.cancelOrder(apiToken, userId, orderId);
        if (cancelOrderResponse ==  null) {
            notifyError("Something went wrong");
        } else if (!cancelOrderResponse.status) {
            notifyError("Something went wrong");
        } else {
            postMessage();
        }
    }
}
