package in.net.webinfotech.ashia.presentation.ui.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.presentation.presenters.OrderListPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.OrderListPresenterImpl;
import in.net.webinfotech.ashia.presentation.ui.adapters.OrdersAdapter;
import in.net.webinfotech.ashia.threading.MainThreadImpl;

public class OrderListActivity extends AppCompatActivity implements OrderListPresenter.View{

    @BindView(R.id.recycler_view_orders)
    RecyclerView recyclerViewOrders;
    @BindView(R.id.layoutLoader)
    View layoutLoader;
    @BindView(R.id.task_loader)
    View taskLoader;
    OrderListPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);
        initialisePresenter();
        showLoader();
        mPresenter.fetchOrderHistory();
        getSupportActionBar().setTitle("My Orders");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new OrderListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(OrdersAdapter adapter) {
        recyclerViewOrders.setVisibility(View.VISIBLE);
        recyclerViewOrders.setAdapter(adapter);
        recyclerViewOrders.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void showTaskLoader() {
        taskLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideTaskLoader() {
        taskLoader.setVisibility(View.GONE);
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
