package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetPlansInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.Plan;
import in.net.webinfotech.ashia.domain.models.PlanWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 30-07-2019.
 */

public class GetPlansInteractorImpl extends AbstractInteractor implements GetPlansInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;

    public GetPlansInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingPlansFail();
            }
        });
    }

    private void postMessage(final Plan[] plans){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingPlansSuccess(plans);
            }
        });
    }

    @Override
    public void run() {
        final PlanWrapper planWrapper = mRepository.getPlans(userId, apiToken);
        if(planWrapper == null){
            notifyError();
        }else if(!planWrapper.status){
            notifyError();
        }else{
            postMessage(planWrapper.plans);
        }
    }
}
