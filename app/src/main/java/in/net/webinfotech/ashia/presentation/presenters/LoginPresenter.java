package in.net.webinfotech.ashia.presentation.presenters;

/**
 * Created by Raj on 22-07-2019.
 */

public interface LoginPresenter {
    void checkLogin(String phone, String password);
    interface View{
        void showLoader();
        void hideLoader();
    }
}
