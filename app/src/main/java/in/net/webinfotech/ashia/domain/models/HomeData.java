package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 13-07-2019.
 */

public class HomeData {
    @SerializedName("category")
    @Expose
    public CategoryList[] categoryLists;

    @SerializedName("slider")
    @Expose
    public HomeSliders[] homeSliders;

    @SerializedName("promotionalSlider")
    @Expose
    public HomeSliders[] promotionalSliders;

    @SerializedName("newArrival")
    @Expose
    public ProductList[] newArrivals;

    @SerializedName("popularProduct")
    @Expose
    public ProductList[] popularProducts;

}
