package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.Plan;

/**
 * Created by Raj on 30-07-2019.
 */

public interface GetPlansInteractor {
    interface Callback{
        void onGettingPlansSuccess(Plan[] plans);
        void onGettingPlansFail();
    }
}
