package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 13-07-2019.
 */

public class HomeSliders {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("img_path")
    @Expose
    public String imgPath;
}
