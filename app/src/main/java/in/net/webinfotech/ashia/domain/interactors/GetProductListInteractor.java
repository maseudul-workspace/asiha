package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.ProductList;

/**
 * Created by Raj on 17-07-2019.
 */

public interface GetProductListInteractor {
    interface Callback {
        void onGettingProductListSuccess(ProductList[] productLists, int totalPage);
        void onGettingProductListFail(String errorMsg);
    }
}
