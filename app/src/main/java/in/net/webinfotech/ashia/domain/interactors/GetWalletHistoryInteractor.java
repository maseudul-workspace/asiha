package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.WalletHistory;

/**
 * Created by Raj on 10-08-2019.
 */

public interface GetWalletHistoryInteractor {
    interface Callback {
        void onGettingWalletHistorySuccess(WalletHistory[] walletHistories);
        void onGettingWalletHistoryFail(String errorMsg);
    }
}
