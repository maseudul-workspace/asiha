package in.net.webinfotech.ashia.presentation.ui.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.Reviews;

/**
 * Created by Raj on 15-07-2019.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {

    Reviews[] reviews;

    public ReviewsAdapter(Reviews[] reviews) {
        this.reviews = reviews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_reviews, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewUserName.setText(reviews[i].userName);
        viewHolder.txtViewReview.setText(reviews[i].comment);
        viewHolder.ratingBar.setRating(reviews[i].rating);
        viewHolder.txtViewReviewDate.setText(reviews[i].date);
    }

    @Override
    public int getItemCount() {
        return reviews.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_user_name)
        TextView txtViewUserName;
        @BindView(R.id.rating_bar)
        RatingBar ratingBar;
        @BindView(R.id.txt_view_review)
        TextView txtViewReview;
        @BindView(R.id.txt_view_review_date)
        TextView txtViewReviewDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
