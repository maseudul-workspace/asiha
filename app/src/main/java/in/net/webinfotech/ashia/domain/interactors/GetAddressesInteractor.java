package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.Address;

/**
 * Created by Raj on 02-08-2019.
 */

public interface GetAddressesInteractor {
    interface Callback {
        void onAddressGettingSuccess(Address[] addresses);
        void onAddressGettingFail(String errorMsg);
    }
}
