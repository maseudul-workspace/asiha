package in.net.webinfotech.ashia.presentation.presenters;

import in.net.webinfotech.ashia.presentation.ui.adapters.WalletHistoryAdapter;

/**
 * Created by Raj on 10-08-2019.
 */

public interface WalletPresenter {
    void fetchWalletAmount();
    void fetchWalletHistories();
    interface View {
        void loadWalletAmount(long walletAmount);
        void loadWalletHistoriesAdapter(WalletHistoryAdapter adapter);
    }
}
