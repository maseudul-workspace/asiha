package in.net.webinfotech.ashia.presentation.ui.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.Brands;

/**
 * Created by Raj on 22-07-2019.
 */

public class BrandsAdapter extends RecyclerView.Adapter<BrandsAdapter.ViewHolder> {

    public interface Callback{
        void onBrandsSelected(int position);
    }

    Brands[] brands;
    Callback mCallback;

    public BrandsAdapter(Brands[] brands, Callback callback) {
        mCallback = callback;
        this.brands = brands;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_brands, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.checkBoxBrands.setText(brands[i].brands);
        if(brands[i].isChecked){
            viewHolder.checkBoxBrands.setChecked(true);
        }else{
            viewHolder.checkBoxBrands.setChecked(false);
        }
        viewHolder.checkBoxBrands.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCallback.onBrandsSelected(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return brands.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.checkbox_brands)
        CheckBox checkBoxBrands;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(Brands[] brands){
        this.brands = brands;
    }

}
