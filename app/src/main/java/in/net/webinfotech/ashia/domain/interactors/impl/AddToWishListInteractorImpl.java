package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.AddToWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.AddToWishListResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 23-08-2019.
 */

public class AddToWishListInteractorImpl extends AbstractInteractor implements AddToWishListInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;
    int productId;

    public AddToWishListInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId, int productId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.productId = productId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishListAddingFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishListAddingSuccess();
            }
        });
    }

    @Override
    public void run() {
        final AddToWishListResponse addToWishListResponse = mRepository.addToWishList(apiToken, userId, productId);
        if(addToWishListResponse ==  null) {
            notifyError("Something went wrong");
        } else if (!addToWishListResponse.status) {
            notifyError("Something went wrong");
        } else {
            postMessage();
        }
    }
}
