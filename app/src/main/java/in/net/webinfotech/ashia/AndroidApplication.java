package in.net.webinfotech.ashia;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import in.net.webinfotech.ashia.domain.models.CartDetails;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.UserInfo;

/**
 * Created by Raj on 22-07-2019.
 */

public class AndroidApplication extends Application {

    UserInfo userInfo;
    CartDetails[] cartDetails;
    ProductList[] wishLists;
    int walletStatus;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_ASIHA_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString(getString(R.string.KEY_USER_DETAILS), new Gson().toJson(userInfo));
        } else {
            editor.putString(getString(R.string.KEY_USER_DETAILS), "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context){
        UserInfo user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_ASIHA_APP_PREFERENCES), Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString(getString(R.string.KEY_USER_DETAILS),"");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

    public void setCartDetails(Context context, CartDetails[] cartDetails){
        this.cartDetails = cartDetails;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_ASIHA_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(cartDetails != null) {
            editor.putString(getString(R.string.KEY_CART_DETAILS), new Gson().toJson(cartDetails));
        } else {
            editor.putString(getString(R.string.KEY_CART_DETAILS), "");
        }

        editor.commit();
    }

    public CartDetails[] getCartDetails(Context context){
        CartDetails[] carts;
        if(cartDetails != null){
            carts = cartDetails;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_ASIHA_APP_PREFERENCES), Context.MODE_PRIVATE);
            String cartDetailsInfoJson = sharedPref.getString(getString(R.string.KEY_CART_DETAILS),"");
            if(cartDetailsInfoJson.isEmpty()){
                carts = null;
            }else{
                try {
                    carts = new Gson().fromJson(cartDetailsInfoJson, CartDetails[].class);
                    this.cartDetails = carts;
                }catch (Exception e){
                    carts = null;
                }
            }
        }
        return carts;
    }

    public void setSelectedAddressId(Context context, int addressId) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_ASIHA_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("ADDRESS_ID", addressId);
        editor.commit();
    }

    public int getSelectedAddressId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_ASIHA_APP_PREFERENCES), Context.MODE_PRIVATE);
        int selectedAddressId = sharedPref.getInt("ADDRESS_ID", 0);
        return selectedAddressId;
    }

    public void setWalletStatus(int status) {
        walletStatus = status;
    }

    public int getWalletStatus() {
        return walletStatus;
    }

    public void setWishList(Context context, ProductList[] lists){
        this.wishLists = lists;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_ASIHA_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(wishLists != null) {
            editor.putString("WISH_LIST", new Gson().toJson(wishLists));
        } else {
            editor.putString("WISH_LIST", "");
        }

        editor.commit();
    }

    public ProductList[] getWishList(Context context) {
        ProductList[] productLists;
        if(wishLists != null){
            productLists = wishLists;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_ASIHA_APP_PREFERENCES), Context.MODE_PRIVATE);
            String productListJson = sharedPref.getString("WISH_LIST","");
            if(productListJson.isEmpty()){
                productLists = null;
            }else{
                try {
                    productLists = new Gson().fromJson(productListJson, ProductList[].class);
                    this.wishLists = productLists;
                }catch (Exception e){
                    productLists = null;
                }
            }
        }
        return productLists;
    }

}
