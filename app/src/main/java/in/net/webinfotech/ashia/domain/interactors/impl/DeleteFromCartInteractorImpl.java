package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.DeleteFromCartInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.DeleteCartResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 01-08-2019.
 */

public class DeleteFromCartInteractorImpl extends AbstractInteractor implements DeleteFromCartInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;
    int cartId;

    public DeleteFromCartInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId, int cartId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.cartId = cartId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCartDeleteFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCartDeleteSuccess();
            }
        });
    }

    @Override
    public void run() {
        DeleteCartResponse deleteCartResponse = mRepository.deleteFromCart(apiToken, userId, cartId);
        if(deleteCartResponse == null){
            notifyError("Something went ");
        }else if(!deleteCartResponse.status){
            notifyError("Something went wrong");
        }else {
            postMessage();
        }
    }
}
