package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.FetchOrderHistoriesInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.Orders;
import in.net.webinfotech.ashia.domain.models.OrdersWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 21-08-2019.
 */

public class FetchOrderHistoriesInteractorImpl extends AbstractInteractor implements FetchOrderHistoriesInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;

    public FetchOrderHistoriesInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrdersFail(errorMsg);
            }
        });
    }

    private void postMessage(final Orders[] orders){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrdersSuccess(orders);
            }
        });
    }

    @Override
    public void run() {
        final OrdersWrapper ordersWrapper = mRepository.fetchOrders(apiKey, userId);
        if(ordersWrapper == null) {
            notifyError("Something went wrong");
        } else if(!ordersWrapper.status) {
            notifyError("Something went wrong");
        } else {
            postMessage(ordersWrapper.orders);
        }
    }
}
