package in.net.webinfotech.ashia.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.presentation.presenters.RegistrationPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.RegistrationPresenterImpl;
import in.net.webinfotech.ashia.presentation.ui.adapters.PlansAdapter;
import in.net.webinfotech.ashia.threading.MainThreadImpl;

public class RegistrationActivity extends AppCompatActivity implements RegistrationPresenter.View {

    @BindView(R.id.recycler_view_plans)
    RecyclerView recyclerViewPlans;
    @BindView(R.id.edit_text_member_name)
    EditText editTextMemberName;
    @BindView(R.id.edit_text_member_phone)
    EditText editTextMemberPhone;
    @BindView(R.id.edit_text_member_email)
    EditText editTextMemberEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_repeat_password)
    EditText editTextRepeatPassword;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.layoutLoader)
    View layoutLoader;
    RegistrationPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_downline);
        ButterKnife.bind(this);
        initialisePresenter();
    }

    public void initialisePresenter(){
       mPresenter = new RegistrationPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onRegistrationSuccess() {
        finish();
    }


    @OnClick(R.id.btn_add_member) void addMember(){
        if(editTextMemberName.getText().toString().isEmpty() ||
                    editTextMemberEmail.getText().toString().isEmpty() ||
                    editTextMemberPhone.getText().toString().isEmpty() ||
                    editTextAddress.getText().toString().isEmpty() ||
                    editTextPassword.getText().toString().isEmpty() ||
                    editTextRepeatPassword.getText().toString().isEmpty()
                    ){
            Toasty.warning(this, "Fields missing", Toast.LENGTH_SHORT, true).show();
        } else if(!editTextPassword.getText().toString().equals(editTextRepeatPassword.getText().toString())) {
            Toasty.warning(this, "Password mismatch", Toast.LENGTH_SHORT, true).show();
        }else{
            mPresenter.addDownline(editTextMemberName.getText().toString(), editTextMemberEmail.getText().toString(), editTextMemberPhone.getText().toString(), editTextAddress.getText().toString(), editTextPassword.getText().toString());
            showLoader();
        }
    }

}
