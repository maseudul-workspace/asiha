package in.net.webinfotech.ashia.presentation.routers;

/**
 * Created by Raj on 25-07-2019.
 */

public interface ProductListRouter {
    void goToProductDetails(int productId);
}
