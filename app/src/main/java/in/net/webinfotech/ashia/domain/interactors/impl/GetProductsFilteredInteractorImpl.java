package in.net.webinfotech.ashia.domain.interactors.impl;

import java.util.ArrayList;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetProductsFilteredInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.ProductListWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 24-07-2019.
 */

public class GetProductsFilteredInteractorImpl extends AbstractInteractor implements GetProductsFilteredInteractor {

    OtherRepositoryImpl mRepository;
    Callback mCallback;
    int subcategoryId;
    int sort;
    ArrayList<String> brands;
    int min;
    int max;
    int page;

    public GetProductsFilteredInteractorImpl(Executor threadExecutor, MainThread mainThread, OtherRepositoryImpl mRepository, Callback mCallback, int subcategoryId, int sort, ArrayList<String> brands, int min, int max, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.subcategoryId = subcategoryId;
        this.sort = sort;
        this.brands = brands;
        this.min = min;
        this.max = max;
        this.page = page;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductsFilteredFail(errorMsg);
            }
        });
    }

    private void postMessage(final ProductList[] productLists, final int totaPages){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductsFilteredSuccess(productLists, totaPages);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper productListWrapper = mRepository.getProductsFiltered(subcategoryId, sort, brands, min, max, page);
        if(productListWrapper == null){
            notifyError("Something went wrong");
        }else if(!productListWrapper.status){
            notifyError("No products found");
        }else {
            postMessage(productListWrapper.productLists, productListWrapper.totalPage);
        }
    }
}
