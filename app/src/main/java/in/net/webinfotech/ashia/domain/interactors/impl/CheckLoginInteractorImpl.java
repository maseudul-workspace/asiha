package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.CheckLoginInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.domain.models.UserInfoWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 22-07-2019.
 */

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String phone;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String phone, String password) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.phone = phone;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(final UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.checkLogin(phone, password);
        if(userInfoWrapper == null){
            notifyError("Something went wrong");
        }else if(!userInfoWrapper.status){
            notifyError("Something went wrong");
        }else{
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
