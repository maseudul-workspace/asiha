package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 05-08-2019.
 */

public interface DeleteAddressInteractor {
    interface Callback {
        void onAddressDeleteSuccess();
        void onAddressDeleteFail(String errorMsg);
    }
}
