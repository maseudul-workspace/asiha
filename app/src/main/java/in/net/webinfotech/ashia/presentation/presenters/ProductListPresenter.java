package in.net.webinfotech.ashia.presentation.presenters;

import in.net.webinfotech.ashia.presentation.ui.adapters.BrandsAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListVerticalAdapter;

/**
 * Created by Raj on 17-07-2019.
 */

public interface ProductListPresenter {
    void getProductList(int subcategoryId, int page, String type);
    void getBrands(int subCategoryId);
    void getProductsFiltered(int subcategoryId, int sort, int min, int max, int page, String type);
    void fetchWishList();
    interface View{
        void loadProductListAdapter(ProductListVerticalAdapter adapter, int totalPages);
        void hideLoader();
        void showLoader();
        void showPaginationProgressBar();
        void hidePaginationProgressBar();
        void loadBrandsAdapter(BrandsAdapter brandsAdapter);
        void showLoginSnackbar();
    }
}
