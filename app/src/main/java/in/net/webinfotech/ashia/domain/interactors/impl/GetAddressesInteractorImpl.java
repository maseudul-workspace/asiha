package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetAddressesInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.Address;
import in.net.webinfotech.ashia.domain.models.AddressWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 02-08-2019.
 */

public class GetAddressesInteractorImpl extends AbstractInteractor implements GetAddressesInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;

    public GetAddressesInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressGettingFail(errorMsg);
            }
        });
    }

    private void postMessage(final Address[] addresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressGettingSuccess(addresses);
            }
        });
    }

    @Override
    public void run() {

        final AddressWrapper addressWrapper = mRepository.fetchAddresses(apiToken, userId);

        if(addressWrapper == null) {
            notifyError("Something went wrong");
        } else if (!addressWrapper.status) {
            notifyError("Something went wrong");
        } else {
            postMessage(addressWrapper.addresses);
        }

    }
}
