package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetProductListInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.ProductListWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 17-07-2019.
 */

public class GetProductListInteractorImpl extends AbstractInteractor implements GetProductListInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    int subcategoryId;
    int page;

    public GetProductListInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, int subcategoryId, int page) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.subcategoryId = subcategoryId;
        this.page = page;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListFail(errorMsg);
            }
        });
    }

    private void postMessage(final ProductList[] productLists, final int totaPages){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListSuccess(productLists, totaPages);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper productListWrapper = mRepository.getProductsList(subcategoryId, page);
        if(productListWrapper == null){
            notifyError("Something went wrong");
        }else if(!productListWrapper.status){
            notifyError("Something went wrong");
        }else{
            postMessage(productListWrapper.productLists, productListWrapper.totalPage);
        }
    }
}
