package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.RegistrationInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.DownlineAddResponse;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 30-07-2019.
 */

public class RegistrationInteractorImpl extends AbstractInteractor implements RegistrationInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String membersName;
    String memberEmail;
    String memberAddress;
    String password;
    String mobileNo;

    public RegistrationInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository,String membersName, String memberEmail, String memberAddress, String password, String mobileNo) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.membersName = membersName;
        this.memberEmail = memberEmail;
        this.memberAddress = memberAddress;
        this.password = password;
        this.mobileNo = mobileNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddDownlineFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddDownlineSuccess();
            }
        });
    }

    @Override
    public void run() {
        final DownlineAddResponse downlineAddResponse = mRepository.addDownline(membersName, memberEmail, mobileNo, password, memberAddress);
        if(downlineAddResponse == null){
            notifyError("Something went wrong");
        } else if(!downlineAddResponse.status){
            notifyError(downlineAddResponse.message);
        } else {
            postMessage();
        }
    }
}
