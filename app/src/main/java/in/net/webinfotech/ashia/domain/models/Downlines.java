package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 05-08-2019.
 */

public class Downlines {

    @SerializedName("userName")
    @Expose
    public String userName;

    @SerializedName("userAdd")
    @Expose
    public String userAdd;

    @SerializedName("userMobileNo")
    @Expose
    public String userMobileNo;

    @SerializedName("userEmail")
    @Expose
    public String userEmail;

}
