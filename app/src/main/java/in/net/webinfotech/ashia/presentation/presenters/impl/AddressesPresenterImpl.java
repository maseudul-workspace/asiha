package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.AddAddressInteractor;
import in.net.webinfotech.ashia.domain.interactors.DeleteAddressInteractor;
import in.net.webinfotech.ashia.domain.interactors.EditAddressInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetAddressesInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.AddAddressInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.DeleteAddressInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.EditAddressInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetAddressesInteractorImpl;
import in.net.webinfotech.ashia.domain.models.Address;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.presentation.presenters.AddressesPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.ui.adapters.AddressAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 02-08-2019.
 */

public class AddressesPresenterImpl extends AbstractPresenter implements AddressesPresenter,
                                                                            GetAddressesInteractor.Callback,
                                                                            AddAddressInteractor.Callback,
                                                                            AddressAdapter.Callback,
                                                                            DeleteAddressInteractor.Callback,
                                                                            EditAddressInteractor.Callback
                                                                            {

    Context mContext;
    AddressesPresenter.View mView;
    AndroidApplication androidApplication;
    GetAddressesInteractorImpl getAddressesInteractor;
    AddAddressInteractorImpl addAddressInteractor;
    DeleteAddressInteractorImpl deleteAddressInteractor;
    EditAddressInteractorImpl editAddressInteractor;
    Address[] addresses;
    AddressAdapter addressAdapter;

    public AddressesPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchAddress() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo != null) {
            getAddressesInteractor = new GetAddressesInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            getAddressesInteractor.execute();
        }
    }

    @Override
    public void addAddress(String state, String city, String phoneNo, String postalCode, String landmark) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        addAddressInteractor = new AddAddressInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, state, city, postalCode, phoneNo, landmark);
        addAddressInteractor.execute();
    }

    @Override
    public void editAddress(String state, String city, String phoneNo, String postalCode, String landmark, int addressId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        editAddressInteractor = new EditAddressInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, state, city, postalCode, phoneNo, landmark, addressId);
        editAddressInteractor.execute();
    }

    @Override
    public void onAddressGettingSuccess(Address[] addresses) {
        this.addresses = addresses;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if (addresses.length > 0) {
            if(androidApplication.getSelectedAddressId(mContext) != 0) {
                for(int i = 0; i < this.addresses.length; i++) {
                    if(this.addresses[i].id == androidApplication.getSelectedAddressId(mContext)) {
                        this.addresses[i].isSelected = true;
                        break;
                    }
                }
            } else {
                this.addresses[0].isSelected = true;
            }
        } else {
            androidApplication.setSelectedAddressId(mContext, 0);
        }
        addressAdapter = new AddressAdapter(mContext, addresses, this);
        mView.loadAddressAdapter(addressAdapter);
        mView.hideLooader();
        this.addresses = addresses;
    }

    @Override
    public void onAddressGettingFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideLooader();
    }

    @Override
    public void onAddressAddSuccess() {
        fetchAddress();
        Toasty.success(mContext, "Successfully added", Toast.LENGTH_SHORT, true).show();
        mView.onAddAddressSuccess();
    }

    @Override
    public void onAddressAddFailed(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.onAddressFailed();
    }

    @Override
    public void onDeleteClicked(int addressId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        deleteAddressInteractor = new DeleteAddressInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, addressId);
        deleteAddressInteractor.execute();
    }

    @Override
    public void onEditClicked(Address address) {
        mView.onAddressEditClicked(address);
    }

    @Override
    public void layoutClicked(int selectedId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setSelectedAddressId(mContext, selectedId);
        for (int i = 0; i < addresses.length; i++) {
            if (addresses[i].id == selectedId) {
                addresses[i].isSelected = true;
            } else {
                addresses[i].isSelected = false;
            }
        }
        addressAdapter.updateDataSet(addresses);
    }

    @Override
    public void onAddressDeleteSuccess() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setSelectedAddressId(mContext, 0);
        Toasty.success(mContext, "Successfully deleted", Toast.LENGTH_SHORT, true).show();
        fetchAddress();
    }

    @Override
    public void onAddressDeleteFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onEditAddressSuccess() {
        Toasty.success(mContext, "Address changed successfully", Toast.LENGTH_SHORT, true).show();
        mView.onAddAddressSuccess();
        fetchAddress();
    }

    @Override
    public void onEditAddressFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
