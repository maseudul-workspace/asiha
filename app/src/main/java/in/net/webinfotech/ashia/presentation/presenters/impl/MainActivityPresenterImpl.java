package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;
import android.widget.ImageView;

import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.FetchDownlinesInteractor;
import in.net.webinfotech.ashia.domain.interactors.FetchRankInteractor;
import in.net.webinfotech.ashia.domain.interactors.FetchRecentlyViewedInteractor;
import in.net.webinfotech.ashia.domain.interactors.FetchWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetCartDetailsInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetHomeDataInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetWalletAmountInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.FetchDownlineInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.FetchRankInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.FetchRecentlyViewedInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.FetchWishListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetCartDetailsInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetHomeDataInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetWalletAmountInteractorImpl;
import in.net.webinfotech.ashia.domain.models.CartDetails;
import in.net.webinfotech.ashia.domain.models.Downlines;
import in.net.webinfotech.ashia.domain.models.HomeData;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.Ranking;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.domain.models.WalletAmount;
import in.net.webinfotech.ashia.presentation.presenters.MainActivityPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.ui.adapters.CategoryListSmallHorizontalAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListHorizontalAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 08-07-2019.
 */

public class MainActivityPresenterImpl extends AbstractPresenter implements MainActivityPresenter,
                                                                            GetHomeDataInteractor.Callback,
                                                                            CategoryListSmallHorizontalAdapter.Callback,
                                                                            GetCartDetailsInteractor.Callback,
                                                                            FetchRecentlyViewedInteractor.Callback,
                                                                            ProductListHorizontalAdapter.Callback,
                                                                            FetchWishListInteractor.Callback
                                                                            {

    Context mContext;
    MainActivityPresenter.View mView;
    GetHomeDataInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    GetCartDetailsInteractorImpl cartDetailsInteractor;
    ProductListHorizontalAdapter popularProductsAdapter;
    ProductListHorizontalAdapter newestArrivalAdapter;
    ProductListHorizontalAdapter recentlyViewedAdapter;
    FetchRecentlyViewedInteractorImpl recentlyViewedInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;

    public MainActivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getData() {
        mInteractor = new GetHomeDataInteractorImpl(mExecutor, mMainThread, new OtherRepositoryImpl(), this);
        mInteractor.execute();
    }

    @Override
    public void getCartDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo != null){
            cartDetailsInteractor = new GetCartDetailsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            cartDetailsInteractor.execute();
        }
    }

    @Override
    public void fetchRecentlyViewed() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo != null){
            recentlyViewedInteractor = new FetchRecentlyViewedInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            recentlyViewedInteractor.execute();
        }
    }

    @Override
    public void fetchWishList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo != null){
            fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            fetchWishListInteractor.execute();
        }
    }

    @Override
    public void onDataGettingSuccess(HomeData homeData) {
        CategoryListSmallHorizontalAdapter categoryListSmallHorizontalAdapter = new CategoryListSmallHorizontalAdapter(mContext, homeData.categoryLists, this);
        mView.loadCategoryAdapter(categoryListSmallHorizontalAdapter);
        newestArrivalAdapter = new ProductListHorizontalAdapter(mContext, homeData.newArrivals, this);
        popularProductsAdapter = new ProductListHorizontalAdapter(mContext, homeData.popularProducts, this);
        mView.loadData(homeData.homeSliders, homeData.promotionalSliders, newestArrivalAdapter, popularProductsAdapter);
    }

    @Override
    public void onDataGettingFail(String errorMsg) {

    }

    @Override
    public void onCategoryClicked(ImageView imageView, int categoryId, String categoryName, String categoryImage) {
        mView.goToSubCategoryActivity(imageView, categoryId, categoryName, categoryImage);
    }

    @Override
    public void onGettingCartDetailsSuccess(CartDetails[] cartDetails) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setCartDetails(mContext, cartDetails);
        mView.loadCartCount(cartDetails.length);
    }

    @Override
    public void onGettingCartDetailsFail() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setCartDetails(mContext, null);
    }

    @Override
    public void onGettingRecentlyViewedSuccess(ProductList[] productLists) {
        if (productLists.length > 0) {
            recentlyViewedAdapter = new ProductListHorizontalAdapter(mContext, productLists, this);
            mView.loadRecentlyViewed(recentlyViewedAdapter);
        }
    }

    @Override
    public void onGettingRecentlyViewedFail(String errorMsg) {

    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onGettingWishListSuccess(ProductList[] productLists) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, productLists);
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, null);
    }

}
