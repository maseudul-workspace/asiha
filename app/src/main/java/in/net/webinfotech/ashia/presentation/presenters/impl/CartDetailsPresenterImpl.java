package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.DeleteFromCartInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetAddressesInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetCartDetailsInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetWalletAmountInteractor;
import in.net.webinfotech.ashia.domain.interactors.PlaceOrderInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.DeleteFromCartInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetAddressesInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetCartDetailsInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetWalletAmountInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.PlaceOrderInteractorImpl;
import in.net.webinfotech.ashia.domain.models.Address;
import in.net.webinfotech.ashia.domain.models.CartDetails;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.domain.models.WalletAmount;
import in.net.webinfotech.ashia.presentation.presenters.CartDetailsPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.ui.adapters.CartAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 31-07-2019.
 */

public class CartDetailsPresenterImpl extends AbstractPresenter implements CartDetailsPresenter, GetCartDetailsInteractor.Callback,
                                                                            CartAdapter.Callback, DeleteFromCartInteractor.Callback,
                                                                            GetWalletAmountInteractor.Callback, GetAddressesInteractor.Callback,
                                                                            PlaceOrderInteractor.Callback
{

    Context mContext;
    CartDetailsPresenter.View mView;
    GetCartDetailsInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    int position;
    DeleteFromCartInteractorImpl deleteFromCartInteractor;
    GetWalletAmountInteractorImpl getWalletAmountInteractor;
    GetAddressesInteractorImpl getAddressesInteractor;
    PlaceOrderInteractorImpl placeOrderInteractor;

    public CartDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getCartDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new GetCartDetailsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
        mInteractor.execute();
    }

    @Override
    public void getWalletDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getWalletAmountInteractor = new GetWalletAmountInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            getWalletAmountInteractor.execute();
        }
    }

    @Override
    public void fetchAddress() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getAddressesInteractor = new GetAddressesInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            getAddressesInteractor.execute();
        }
    }

    @Override
    public void placeOrder(int addressID, int walletStatus) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            placeOrderInteractor = new PlaceOrderInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, addressID, walletStatus);
            placeOrderInteractor.execute();
        }
    }

    @Override
    public void onGettingCartDetailsSuccess(CartDetails[] cartDetails) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setCartDetails(mContext, cartDetails);
        CartAdapter adapter = new CartAdapter(mContext, cartDetails, this);
        double totalPrice = 0;
        for (int i = 0; i < cartDetails.length; i++){
            totalPrice = totalPrice + cartDetails[i].sellingPrice;
        }
        mView.loadData(adapter, cartDetails.length, totalPrice);
        mView.hideLoader();
    }

    @Override
    public void onGettingCartDetailsFail() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setCartDetails(mContext, null);
        mView.hideLoader();
    }

    @Override
    public void onDeleteClicked(int cartId, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        deleteFromCartInteractor = new DeleteFromCartInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, cartId);
        deleteFromCartInteractor.execute();
    }

    @Override
    public void onCartDeleteSuccess() {
        Toasty.success(mContext, "Deleted Successfully", Toast.LENGTH_SHORT, true).show();
        getCartDetails();
    }

    @Override
    public void onCartDeleteFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onGettingWalletAmountSuccess(WalletAmount walletAmount) {
        mView.loadWalletAmount(walletAmount.totalAmount);
    }

    @Override
    public void onGettingWalletAmountFail(String errorMsg) {

    }

    @Override
    public void onAddressGettingSuccess(Address[] addresses) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if (addresses.length > 0) {
            if(androidApplication.getSelectedAddressId(mContext) != 0) {
                for(int i = 0; i < addresses.length; i++) {
                    if(addresses[i].id == androidApplication.getSelectedAddressId(mContext)) {
                        mView.loadAddress(addresses[i]);
                        break;
                    } else {
                        mView.loadAddress(addresses[0]);
                    }
                }
            } else {
                mView.loadAddress(addresses[0]);
            }
        } else {
           androidApplication.setSelectedAddressId(mContext, 0);
        }
    }

    @Override
    public void onAddressGettingFail(String errorMsg) {

    }

    @Override
    public void onPlaceOrderSuccess() {
        mView.onOrderPlacedSuccess();
        Toasty.success(mContext, "Order Placed Successfully", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onPlaceOrderFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.onOrderPlacedFail();
    }
}
