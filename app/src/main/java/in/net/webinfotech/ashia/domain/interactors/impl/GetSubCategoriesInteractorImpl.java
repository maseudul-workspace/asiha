package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.GetSubCategoriesInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.SubCategoryList;
import in.net.webinfotech.ashia.domain.models.SubCategoryListWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 15-07-2019.
 */

public class GetSubCategoriesInteractorImpl extends AbstractInteractor implements GetSubCategoriesInteractor {

    OtherRepositoryImpl mRepository;
    int id;
    Callback mCallback;

    public GetSubCategoriesInteractorImpl(Executor threadExecutor, MainThread mainThread, OtherRepositoryImpl mRepository, int id, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.id = id;
        this.mCallback = mCallback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataFail(errorMsg);
            }
        });
    }

    private void postMessage(final SubCategoryList[] subCategoryLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataSuccess(subCategoryLists);
            }
        });
    }

    @Override
    public void run() {
        final SubCategoryListWrapper subCategoryListWrapper = mRepository.getSubCategories(id);
        if(subCategoryListWrapper == null){
            notifyError("Something went wrong");
        }else if(!subCategoryListWrapper.status){
            notifyError("Something went wrong");
        }else{
            postMessage(subCategoryListWrapper.subCategoryLists);
        }
    }
}
