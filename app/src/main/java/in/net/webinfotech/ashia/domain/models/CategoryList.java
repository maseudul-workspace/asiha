package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 08-07-2019.
 */

public class CategoryList {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("cate_name")
    @Expose
    public String categoryName;

    @SerializedName("img_path")
    @Expose
    public String imgPath;

}
