package in.net.webinfotech.ashia.presentation.ui.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.presentation.presenters.WishListPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.WishListPresenterImpl;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListVerticalAdapter;
import in.net.webinfotech.ashia.threading.MainThreadImpl;

public class WishListActivity extends AppCompatActivity implements WishListPresenter.View {

    @BindView(R.id.recycler_view_wishlist)
    RecyclerView recyclerViewWishlist;
    @BindView(R.id.layoutLoader)
    View layoutLoader;
    WishListPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        ButterKnife.bind(this);
        initialisePresenter();
        mPresenter.fetchWishList();
        showLoader();
        getSupportActionBar().setTitle("My Wishlist");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new WishListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void loadData(ProductListVerticalAdapter adapter) {
        recyclerViewWishlist.setVisibility(View.VISIBLE);
        recyclerViewWishlist.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewWishlist.setAdapter(adapter);
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
