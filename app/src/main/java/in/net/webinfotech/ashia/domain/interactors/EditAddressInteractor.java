package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 05-08-2019.
 */

public interface EditAddressInteractor {
    interface Callback {
        void onEditAddressSuccess();
        void onEditAddressFail(String errorMsg);
    }
}
