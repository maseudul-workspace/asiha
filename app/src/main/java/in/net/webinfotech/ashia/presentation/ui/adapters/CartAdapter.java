package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.models.CartDetails;
import in.net.webinfotech.ashia.util.GlideHelper;

/**
 * Created by Raj on 23-07-2019.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    public interface Callback{
        void onDeleteClicked(int cartId, int position);
    }

    Context mContext;
    CartDetails[] cartDetails;
    Callback mCallback;

    public CartAdapter(Context mContext, CartDetails[] cartDetails, Callback callback) {
        this.mContext = mContext;
        this.cartDetails = cartDetails;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_cart, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        GlideHelper.setImageView(mContext, viewHolder.imgViewProduct, cartDetails[i].productImage);
        viewHolder.txtViewProductName.setText(cartDetails[i].productName);
        viewHolder.txtViewSellingPrice.setText("Rs." + Double.toString(cartDetails[i].sellingPrice));
        viewHolder.txtViewBrands.setText("Brands: " + cartDetails[i].brands);
        viewHolder.txtViewProductPrice.setText("Rs." + cartDetails[i].price);
        viewHolder.txtViewProductPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        viewHolder.txtViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirmation dialog");
                builder.setMessage("You are about to remove a item from cart. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(cartDetails[i].cartId, i);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartDetails.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_view_product_image)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.txt_view_brands)
        TextView txtViewBrands;
        @BindView(R.id.txt_view_product_selling_price)
        TextView txtViewSellingPrice;
        @BindView(R.id.txt_view_delete)
        TextView txtViewDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
