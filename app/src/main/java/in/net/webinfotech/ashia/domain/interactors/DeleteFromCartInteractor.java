package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 01-08-2019.
 */

public interface DeleteFromCartInteractor {
    interface Callback{
        void onCartDeleteSuccess();
        void onCartDeleteFail(String errorMsg);
    }
}
