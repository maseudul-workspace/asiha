package in.net.webinfotech.ashia.repository;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Raj on 13-07-2019.
 */

public interface OtherRepository {


    @GET("api/main_category")
    Call<ResponseBody> getHomeData();

    @GET("api/sub_category/{pro_cate_id}")
    Call<ResponseBody> getSubCategories(@Path("pro_cate_id") int id);

    @GET("api/products_sub_category/{sub_cate_id}/{page}")
    Call<ResponseBody> getProductsList(@Path("sub_cate_id") int subCategoryId,
                                       @Path("page") int page
                                        );

    @POST("api/user_login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("user_name") String phoneNumber,
                                  @Field("password") String password
                                  );

    @GET("api/brands_retrive/{sub_cate_id}")
    Call<ResponseBody> getBrands(@Path("sub_cate_id") int subCategoryId
    );

    @POST("api/products_search")
    @FormUrlEncoded
    Call<ResponseBody> getProductsFiltered(@Field("sub_cate_id") int subCateId,
                                           @Field("brands[]") ArrayList<String> brands,
                                           @Field("sort") int sort,
                                           @Field("min") int min,
                                           @Field("max") int max,
                                           @Field("page") int page
                                           );

    @GET("api/product_details/{product_id}")
    Call<ResponseBody> getProductDetails(@Path("product_id") int productId
    );

    @POST("api/new_review")
    @FormUrlEncoded
    Call<ResponseBody> submitReview(@Field("product_id") int productId,
                                           @Field("rating") int rating,
                                           @Field("comment") String comment,
                                           @Field("api_token") String apiToken,
                                           @Field("userId") int userId
    );

    @POST("api/plan")
    @FormUrlEncoded
    Call<ResponseBody> fetchPlans(@Field("api_token") String apiToken,
                                    @Field("userId") int userId
    );

    @POST("api/new_registration")
    @FormUrlEncoded
    Call<ResponseBody> addDownline(@Field("members_name") String membersName,
                                   @Field("member_email") String memberEmail,
                                   @Field("mobile_no") String mobile_no,
                                   @Field("password") String password,
                                   @Field("address") String address
    );

    @POST("api/fetch_cart_products")
    @FormUrlEncoded
    Call<ResponseBody> fetchCartDetails(@Field("api_token") String apiToken,
                                  @Field("userId") int userId2
    );

    @POST("api/add_cart")
    @FormUrlEncoded
    Call<ResponseBody> addToCart(@Field("api_token") String apiToken,
                                 @Field("userId") int userId,
                                 @Field("productId") int productId,
                                 @Field("quantity") int quantity
                                 );

    @POST("api/delete_cart")
    @FormUrlEncoded
    Call<ResponseBody> deleteFromCart(@Field("api_token") String apiToken,
                                 @Field("userId") int userId,
                                 @Field("cartId") int cartId
    );

    @POST("api/fetch_address")
    @FormUrlEncoded
    Call<ResponseBody> fetchAddress(@Field("api_token") String apiToken,
                                      @Field("userId") int userId
    );

    @POST("api/add_address")
    @FormUrlEncoded
    Call<ResponseBody> addAddress(@Field("api_token") String apiToken,
                                  @Field("userId") int userId,
                                  @Field("state") String state,
                                  @Field("city") String city,
                                  @Field("postalCode") String postalCode,
                                  @Field("phoneNo") String phoneNo,
                                  @Field("landmark") String landmark
    );

    @POST("api/delete_address")
    @FormUrlEncoded
    Call<ResponseBody> deleteAddress(@Field("api_token") String apiToken,
                                     @Field("userId") int userId,
                                     @Field("addId") int addId
    );

    @POST("api/edit_address")
    @FormUrlEncoded
    Call<ResponseBody> editAddress(@Field("api_token") String apiToken,
                                  @Field("userId") int userId,
                                  @Field("state") String state,
                                  @Field("city") String city,
                                  @Field("postalCode") String postalCode,
                                  @Field("phoneNo") String phoneNo,
                                  @Field("landmark") String landmark,
                                   @Field("addId") int addId
    );

    @POST("api/downline_list")
    @FormUrlEncoded
    Call<ResponseBody> fetchDownlines(@Field("api_token") String apiToken,
                                    @Field("userId") int userId
    );

    @POST("api/wallet")
    @FormUrlEncoded
    Call<ResponseBody> fetchWalletAmount(@Field("api_token") String apiToken,
                                         @Field("userId") int userId
    );

    @POST("api/wallethistory")
    @FormUrlEncoded
    Call<ResponseBody> fetchWalletHistories(@Field("api_token") String apiToken,
                                            @Field("userId") int userId
    );

    @POST("api/api_new_order")
    @FormUrlEncoded
    Call<ResponseBody> placeOrder(@Field("api_token") String apiToken,
                                 @Field("userId") int userId,
                                 @Field("shippingAddressId") int shippingAddressId,
                                 @Field("wallet") int wallet
                                 );


    @POST("api/order_history")
    @FormUrlEncoded
    Call<ResponseBody> fetchOrderHistories(@Field("api_token") String apiToken,
                                            @Field("userId") int userId
    );

    @POST("api/api_order_cancel")
    @FormUrlEncoded
    Call<ResponseBody> cancelOrder(@Field("api_token") String apiToken,
                                   @Field("userId") int userId,
                                   @Field("orderId") String orderId
    );

    @POST("api/recently_view")
    @FormUrlEncoded
    Call<ResponseBody> fetchRecentlyViews(@Field("api_token") String apiToken,
                                           @Field("userId") int userId
    );

    @POST("api/add_recently_view")
    @FormUrlEncoded
    Call<ResponseBody> addRecentlyView(@Field("api_token") String apiToken,
                                       @Field("userId") int userId,
                                       @Field("productId") int productId
    );

    @POST("api/fetch_wishlist_products")
    @FormUrlEncoded
    Call<ResponseBody> fetchWishList(@Field("api_token") String apiToken,
                                     @Field("userId") int userId
    );

    @POST("api/add_wishlist")
    @FormUrlEncoded
    Call<ResponseBody> addToWishList(@Field("api_token") String apiToken,
                                     @Field("userId") int userId,
                                     @Field("productId") int productId
    );

    @POST("api/delete_wishlist_products")
    @FormUrlEncoded
    Call<ResponseBody> deleteFromWishList(@Field("api_token") String apiToken,
                                          @Field("userId") int userId,
                                          @Field("productId") int productId
    );

    @GET("api/related_product/{brandId}")
    Call<ResponseBody> getRelatedProducts(@Path("brandId") int brandId);

    @POST("api/ranking")
    @FormUrlEncoded
    Call<ResponseBody> fetchRanking(@Field("api_token") String apiToken,
                                     @Field("userId") int userId
    );

}