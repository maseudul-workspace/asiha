package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 17-07-2019.
 */

public class ProductList {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("pro_cate_id")
    @Expose
    public int categoryId;

    @SerializedName("sub_cate_id")
    @Expose
    public int subCategoryId;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("selling_price")
    @Expose
    public String sellingPrice;

    @SerializedName("rating")
    @Expose
    public int rating;

    @SerializedName("pro_name")
    @Expose
    public String productName;

    @SerializedName("cover_img_path")
    @Expose
    public String image;

    @SerializedName("stock")
    @Expose
    public int stock;

    @SerializedName("brand_name")
    @Expose
    public String brand_name;

    public boolean isAddedToCart = false;
    public boolean isWishListPresent = false;

}
