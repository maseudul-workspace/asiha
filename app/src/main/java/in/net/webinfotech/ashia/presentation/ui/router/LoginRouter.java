package in.net.webinfotech.ashia.presentation.ui.router;

/**
 * Created by Raj on 22-07-2019.
 */

public interface LoginRouter {
    void goToLogin();
}
