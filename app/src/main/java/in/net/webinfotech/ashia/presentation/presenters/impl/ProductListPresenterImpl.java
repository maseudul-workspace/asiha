package in.net.webinfotech.ashia.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.ashia.AndroidApplication;
import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.AddToCartInteractor;
import in.net.webinfotech.ashia.domain.interactors.AddToWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.DeleteFromWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.FetchWishListInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetBrandsInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetProductListInteractor;
import in.net.webinfotech.ashia.domain.interactors.GetProductsFilteredInteractor;
import in.net.webinfotech.ashia.domain.interactors.impl.AddToCartInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.AddToWishListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.DeleteFromWishListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.FetchWishListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetBrandsInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetProductListInteractorImpl;
import in.net.webinfotech.ashia.domain.interactors.impl.GetProductsFilteredInteractorImpl;
import in.net.webinfotech.ashia.domain.models.Brands;
import in.net.webinfotech.ashia.domain.models.CartDetails;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.UserInfo;
import in.net.webinfotech.ashia.presentation.presenters.ProductListPresenter;
import in.net.webinfotech.ashia.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ashia.presentation.routers.ProductListRouter;
import in.net.webinfotech.ashia.presentation.ui.adapters.BrandsAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListVerticalAdapter;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 17-07-2019.
 */

public class ProductListPresenterImpl extends AbstractPresenter implements ProductListPresenter,
                                                                            GetProductListInteractor.Callback,
                                                                            ProductListVerticalAdapter.Callback,
                                                                            BrandsAdapter.Callback,
                                                                            GetBrandsInteractor.Callback,
                                                                            GetProductsFilteredInteractor.Callback,
                                                                            AddToCartInteractor.Callback,
                                                                            AddToWishListInteractor.Callback,
                                                                            DeleteFromWishListInteractor.Callback,
                                                                            FetchWishListInteractor.Callback
{

    Context mContext;
    ProductListPresenter.View mView;
    ProductList[] newProductLists;
    GetProductListInteractorImpl productListInteractor;
    ProductListVerticalAdapter adapter;
    ArrayList<String> brandsIds;
    Brands brands[];
    BrandsAdapter brandsAdapter;
    GetBrandsInteractorImpl getBrandsInteractor;
    GetProductsFilteredInteractorImpl getProductsFilteredInteractor;
    ProductListRouter mRouter;
    AndroidApplication androidApplication;
    int position;
    AddToCartInteractorImpl addToCartInteractor;
    AddToWishListInteractorImpl addToWishListInteractor;
    DeleteFromWishListInteractorImpl deleteFromWishListInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;

    public ProductListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView, ProductListRouter router) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
        mRouter = router;
    }

    @Override
    public void getProductList(int subcategoryId, int page, String type) {
        if(type.equals("refresh")){
            newProductLists = null;
        }
        productListInteractor = new GetProductListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), subcategoryId, page);
        productListInteractor.execute();
    }

    @Override
    public void getBrands(int subCategoryId) {
        getBrandsInteractor = new GetBrandsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), subCategoryId);
        getBrandsInteractor.execute();
    }

    @Override
    public void getProductsFiltered(int subcategoryId, int sort, int min, int max, int page, String type) {
        if(type.equals("refresh")){
            newProductLists = null;
        }
        getProductsFilteredInteractor = new GetProductsFilteredInteractorImpl(mExecutor, mMainThread, new OtherRepositoryImpl(), this, subcategoryId, sort, brandsIds, min, max, page);
        getProductsFilteredInteractor.execute();
    }

    @Override
    public void fetchWishList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo != null) {
            fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            fetchWishListInteractor.execute();
        }
    }


    @Override
    public void onGettingProductListSuccess(ProductList[] productLists, int totalPage) {
        mView.hideLoader();
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if(androidApplication.getUserInfo(mContext) != null) {
            CartDetails[] cartDetails = androidApplication.getCartDetails(mContext);
            if(cartDetails != null){
                for(int i = 0; i < productLists.length; i++){
                    for(int j = 0; j < cartDetails.length; j++){
                        if(productLists[i].id == cartDetails[j].productId){
                            productLists[i].isAddedToCart = true;
                            break;
                        }
                    }
                }
            }
            ProductList[] wishLists = androidApplication.getWishList(mContext);
            if(wishLists != null){
                for(int i = 0; i < productLists.length; i++){
                    for(int j = 0; j < wishLists.length; j++){
                        if(productLists[i].id == wishLists[j].id){
                            productLists[i].isWishListPresent = true;
                            break;
                        }
                    }
                }
            }
        }
        if(productLists.length > 0){
            ProductList[] tempProductLists;
            tempProductLists = newProductLists;
            try {
                int len1 = tempProductLists.length;
                int len2 = productLists.length;
                newProductLists = new ProductList[len1 + len2];
                System.arraycopy(tempProductLists, 0, newProductLists, 0, len1);
                System.arraycopy(productLists, 0, newProductLists, len1, len2);
                adapter.updateDataSet(newProductLists);
                adapter.notifyDataSetChanged();
                mView.hidePaginationProgressBar();
            }catch (NullPointerException e){
                newProductLists = productLists;
                adapter = new ProductListVerticalAdapter(mContext, productLists, this);
                mView.loadProductListAdapter(adapter, totalPage);
            }
        }

    }

    @Override
    public void onGettingProductListFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
    }

    @Override
    public void goToProductDetails(int productId) {
        mRouter.goToProductDetails(productId);
    }

    @Override
    public void addToCart(int productId, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo == null) {
            mView.showLoginSnackbar();
        } else {
            addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, 1, productId);
            addToCartInteractor.execute();
        }
    }

    @Override
    public void addToWishList(int productId, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo == null) {
            mView.showLoginSnackbar();
        } else {
            addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, productId);
            addToWishListInteractor.execute();
        }
    }

    @Override
    public void deleteFromWishList(int productId, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo == null) {
            mView.showLoginSnackbar();
        } else {
            deleteFromWishListInteractor = new DeleteFromWishListInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.apiToken, userInfo.userId, productId);
            deleteFromWishListInteractor.execute();
        }
    }

    @Override
    public void onBrandsSelected(int position) {
        brands[position].isChecked = !brands[position].isChecked;
        if(brands[position].isChecked){
            brandsIds.add(brands[position].id);
        }else{
            brandsIds.remove(brands[position].id);
        }
        brandsAdapter.updateData(brands);
    }

    @Override
    public void onGettingBrandsSuccess(Brands[] brands) {
        if(brands.length > 0){
            this.brands = brands;
            brandsIds = new ArrayList<>();
            for(int i = 0; i < brands.length; i++){
                brandsIds.add(brands[i].id);
            }
            brandsAdapter = new BrandsAdapter(brands, this);
            mView.loadBrandsAdapter(brandsAdapter);
        }
    }

    @Override
    public void onGettingBrandsFail() {

    }

    @Override
    public void onGettingProductsFilteredSuccess(ProductList[] productLists, int totalPage) {
        mView.hideLoader();
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if(androidApplication.getUserInfo(mContext) != null) {
            CartDetails[] cartDetails = androidApplication.getCartDetails(mContext);
            if(cartDetails != null){
                for(int i = 0; i < productLists.length; i++){
                    for(int j = 0; j < cartDetails.length; j++){
                        if(productLists[i].id == cartDetails[j].productId){
                            productLists[i].isAddedToCart = true;
                            break;
                        }
                    }
                }
            }
        }
        if(productLists.length > 0){
            ProductList[] tempProductLists;
            tempProductLists = newProductLists;
            try {
                int len1 = tempProductLists.length;
                int len2 = productLists.length;
                newProductLists = new ProductList[len1 + len2];
                System.arraycopy(tempProductLists, 0, newProductLists, 0, len1);
                System.arraycopy(productLists, 0, newProductLists, len1, len2);
                adapter.updateDataSet(newProductLists);
                adapter.notifyDataSetChanged();
                mView.hidePaginationProgressBar();
            }catch (NullPointerException e){
                newProductLists = productLists;
                adapter = new ProductListVerticalAdapter(mContext, productLists, this);
                mView.loadProductListAdapter(adapter, totalPage);
            }
        }
    }

    @Override
    public void onGettingProductsFilteredFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
    }

    @Override
    public void onAddToCartSuccess() {
        Toasty.success(mContext, "Added to cart", Toast.LENGTH_SHORT, true).show();
        adapter.onAddToCartSuccess(position);
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onWishListAddingSuccess() {
        Toasty.success(mContext, "Added to wish list", Toast.LENGTH_SHORT, true).show();
        adapter.onAddToWishListSuccess(position);
        fetchWishList();
    }

    @Override
    public void onWishListAddingFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onDeleteFromWishListSuccess() {
        Toasty.success(mContext, "Removed from wishlist", Toast.LENGTH_SHORT, true).show();
        adapter.onDeleteFromWishListSuccess(position);
        fetchWishList();
    }

    @Override
    public void onDeleteFromWishListFail() {
        Toasty.error(mContext, "Something went wrong", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onGettingWishListSuccess(ProductList[] productLists) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishList(mContext, productLists);
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {

    }
}
