package in.net.webinfotech.ashia.presentation.presenters;

import in.net.webinfotech.ashia.domain.models.ProductDetails;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductDetailsImagesAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ProductListHorizontalAdapter;
import in.net.webinfotech.ashia.presentation.ui.adapters.ReviewsAdapter;

/**
 * Created by Raj on 25-07-2019.
 */

public interface ProductDetailsPresenter {
    void getProductDetails(int productId);
    void getCartDetails(int productId);
    void submitReview(int productId, int rating , String comment);
    void addToCart(int productId, int quantity);
    void addRecentView(int productId);
    void addToWishList(int productId);
    void fetchWishList();
    void deleteFromWishList(int productId);
    void fetchRelatedProducts(int brandId);
    interface View{
        void loadProductDetails(ProductDetails productDetails);
        void loadProductImagesAdapter(ProductDetailsImagesAdapter adapter, int imageCount);
        void loadReviewsAdapter(ReviewsAdapter adapter);
        void showLoader();
        void hideLoader();
        void hideDialog();
        void loadCartCount(int cartCount, boolean isCartPresent);
        void onWishListAddSuccess();
        void onWishListRemovedSuccess();
        void loadRelatedProducts(ProductListHorizontalAdapter adapter);
        void goToProductDetails(int productId);
        void onCartAddedFail();
        void goToPreviewImage(String img);
    }
}
