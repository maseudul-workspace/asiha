package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.FetchRecentlyViewedInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.ProductList;
import in.net.webinfotech.ashia.domain.models.ProductListWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 22-08-2019.
 */

public class FetchRecentlyViewedInteractorImpl extends AbstractInteractor implements FetchRecentlyViewedInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;

    public FetchRecentlyViewedInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
    }


    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingRecentlyViewedFail(errorMsg);
            }
        });
    }

    private void postMessage(final ProductList[] productLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingRecentlyViewedSuccess(productLists);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper productListWrapper = mRepository.fetchRecentlyViews(apiToken, userId);
        if(productListWrapper == null) {
            notifyError("");
        } else if (!productListWrapper.status) {
            notifyError("");
        } else {
            postMessage(productListWrapper.productLists);
        }
    }
}
