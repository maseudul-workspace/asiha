package in.net.webinfotech.ashia.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import in.net.webinfotech.ashia.domain.models.HomeSliders;
import in.net.webinfotech.ashia.util.GlideHelper;

/**
 * Created by Raj on 25-04-2019.
 */

public class SlidingImagesAdapter extends PagerAdapter {


    Context mContext;
    HomeSliders[] homeSliders;
    int customPosition = 0;

    public SlidingImagesAdapter(Context mContext, HomeSliders[] homeSliders) {
        this.mContext = mContext;
        this.homeSliders = homeSliders;
    }

    @Override
    public int getCount() {
        return homeSliders.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideHelper.setImageView(mContext, imageView, homeSliders[position].imgPath);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
