package in.net.webinfotech.ashia.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 15-07-2019.
 */

public class SubCategoryList {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("pro_cate_id")
    @Expose
    public int categoryId;

    @SerializedName("sub_cate_name")
    @Expose
    public String subCategoryName;

    @SerializedName("img_path")
    @Expose
    public String imgPath;
}
