package in.net.webinfotech.ashia.presentation.ui.activities;

import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ashia.R;
import in.net.webinfotech.ashia.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.ashia.domain.models.Address;
import in.net.webinfotech.ashia.presentation.presenters.CartDetailsPresenter;
import in.net.webinfotech.ashia.presentation.presenters.impl.CartDetailsPresenterImpl;
import in.net.webinfotech.ashia.presentation.ui.adapters.CartAdapter;
import in.net.webinfotech.ashia.presentation.ui.dialogs.OrderConfirmDialog;
import in.net.webinfotech.ashia.threading.MainThreadImpl;

public class CartActivity extends AppCompatActivity implements CartDetailsPresenter.View, OrderConfirmDialog.Callback {

    @BindView(R.id.recycler_view_carts)
    RecyclerView recyclerViewCarts;
    @BindView(R.id.txt_view_total_item)
    TextView txtViewTotalItem;
    @BindView(R.id.txt_view_total_price)
    TextView txtViewTotalPrice;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.layoutLoader)
    View layoutLoader;
    @BindView(R.id.btn_select_address)
    Button btnSelectAddress;
    @BindView(R.id.txt_view_wallet_amount)
    TextView txtViewWalletAmt;
    @BindView(R.id.txt_view_net_price)
    TextView txtViewNetPrice;
    @BindView(R.id.wallet_layout)
    View walletLayout;
    CartDetailsPresenterImpl mPresenter;
    long walletAmount;
    double totalAmount;
    double netAmount;
    @BindView(R.id.wallet_checkbox)
    CheckBox walletCheckbox;
    @BindView(R.id.layout_price_details)
    View layoutPriceDetails;
    @BindView(R.id.relative_layout)
    RelativeLayout relativeLayout;
    OrderConfirmDialog orderConfirmDialog;
    Address selectedAddress = null;
    int walletPay = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        initialisePresenter();
        initialiseDialog();
        orderConfirmDialog.setUpDialog();
        mPresenter.getCartDetails();
        mPresenter.getWalletDetails();
        showLoader();
        getSupportActionBar().setTitle("Cart Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter(){
        mPresenter = new CartDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseDialog() {
        orderConfirmDialog = new OrderConfirmDialog(this, this, this);
    }

    @Override
    public void loadData(CartAdapter adapter, int itemCount, double totalPrice) {
        if(itemCount == 0) {
            btnSelectAddress.setVisibility(View.GONE);
            mainLayout.setVisibility(View.GONE);
            layoutPriceDetails.setVisibility(View.GONE);
        } else {
            btnSelectAddress.setVisibility(View.VISIBLE);
            mainLayout.setVisibility(View.VISIBLE);
            layoutPriceDetails.setVisibility(View.VISIBLE);
            recyclerViewCarts.setAdapter(adapter);
            recyclerViewCarts.setLayoutManager(new LinearLayoutManager(this));
            recyclerViewCarts.setNestedScrollingEnabled(false);
            txtViewTotalItem.setText(Integer.toString(itemCount));
            txtViewTotalPrice.setText("Rs. " + totalPrice);
            totalAmount = totalPrice;
            netAmount = totalPrice;
            if(walletPay == 1) {
                if (totalAmount >= walletAmount) {
                    netAmount = totalAmount - walletAmount;
                    txtViewNetPrice.setText("Rs. " + netAmount);
                    txtViewWalletAmt.setText("Rs. 0");
                } else {
                    double netWalletAmt = walletAmount - totalAmount;
                    txtViewWalletAmt.setText("Rs. " + netWalletAmt);
                    netAmount = 0;
                    txtViewNetPrice.setText("Rs. 0");
                }
            } else {
                txtViewNetPrice.setText("Rs. " + totalPrice);
            }
        }
    }

    @Override
    public void loadWalletAmount(long amount) {
        walletLayout.setVisibility(View.VISIBLE);
        txtViewWalletAmt.setText("Rs. " + amount);
        walletAmount = amount;
        walletCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    walletPay = 1;
                    if (totalAmount >= walletAmount) {
                        netAmount = totalAmount - walletAmount;
                        txtViewNetPrice.setText("Rs. " + netAmount);
                        txtViewWalletAmt.setText("Rs. 0");
                    } else {
                        double netWalletAmt = walletAmount - totalAmount;
                        txtViewWalletAmt.setText("Rs. " + netWalletAmt);
                        netAmount = 0;
                        txtViewNetPrice.setText("Rs. 0");
                    }
                } else {
                    walletPay = 0;
                    netAmount = totalAmount;
                    txtViewNetPrice.setText("Rs. " + totalAmount);
                    txtViewWalletAmt.setText("Rs. " + walletAmount);
                }
            }
        });
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void loadAddress(Address address) {
        selectedAddress = address;
    }

    @OnClick(R.id.btn_select_address) void onOrderConfirmClicked() {
        if (selectedAddress == null) {
            showAddressSnackBar();
        } else {
            orderConfirmDialog.showDialog(selectedAddress, netAmount);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.fetchAddress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onEditAddressClicked() {
        orderConfirmDialog.hideDialog();
        Intent addressIntent = new Intent(this, AddressActivity.class);
        startActivity(addressIntent);
        finish();
    }

    @Override
    public void onConfirmOrderClicked() {
        mPresenter.placeOrder(selectedAddress.id, walletPay);
    }

    @Override
    public void onOrderPlacedSuccess() {
        orderConfirmDialog.hideLoader();
        orderConfirmDialog.hideDialog();
        Intent productListIntent = new Intent(this, OrderListActivity.class);
        startActivity(productListIntent);
        finish();
    }

    @Override
    public void onOrderPlacedFail() {
        orderConfirmDialog.hideLoader();
        orderConfirmDialog.hideDialog();
    }

    public void showAddressSnackBar() {
        Snackbar snackbar = Snackbar.make(relativeLayout, "Please add shipping address", Snackbar.LENGTH_LONG);
        snackbar.setAction("Add", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addressIntent = new Intent(getApplicationContext(), AddressActivity.class);
                startActivity(addressIntent);
                finish();
            }
        });
        snackbar.show();
    }

}
