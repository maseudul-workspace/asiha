package in.net.webinfotech.ashia.domain.interactors.impl;

import in.net.webinfotech.ashia.domain.executors.Executor;
import in.net.webinfotech.ashia.domain.executors.MainThread;
import in.net.webinfotech.ashia.domain.interactors.FetchDownlinesInteractor;
import in.net.webinfotech.ashia.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.ashia.domain.models.Downlines;
import in.net.webinfotech.ashia.domain.models.DownlinesWrapper;
import in.net.webinfotech.ashia.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 05-08-2019.
 */

public class FetchDownlineInteractorImpl extends AbstractInteractor implements FetchDownlinesInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiToken;
    int userId;

    public FetchDownlineInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGetDownlinesFail(errorMsg);
            }
        });
    }

    private void postMessage(final Downlines[] downlines){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGetDownlinesSuccess(downlines);
            }
        });
    }

    @Override
    public void run() {
        final DownlinesWrapper downlinesWrapper = mRepository.fetchDownlines(apiToken, userId);
        if(downlinesWrapper == null) {
            notifyError("Something went wrong");
        } else if(!downlinesWrapper.status) {
            notifyError("Something went wrong");
        } else {
            postMessage(downlinesWrapper.downlines);
        }
    }
}
