package in.net.webinfotech.ashia.domain.interactors;

import in.net.webinfotech.ashia.domain.models.ProductDetails;

/**
 * Created by Raj on 25-07-2019.
 */

public interface GetProductDetailsInteractor {
    interface Callback{
        void onGettingProductDetailSuccess(ProductDetails productDetails);
        void onGettingProductDetailsFail(String errorMsg);
    }
}
