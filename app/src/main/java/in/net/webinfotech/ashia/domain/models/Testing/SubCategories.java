package in.net.webinfotech.ashia.domain.models.Testing;

/**
 * Created by Raj on 09-07-2019.
 */

public class SubCategories {
    public String subCategoryName;
    public String subCategoryImage;

    public SubCategories(String subCategoryName, String subCategoryImage) {
        this.subCategoryName = subCategoryName;
        this.subCategoryImage = subCategoryImage;
    }

}
