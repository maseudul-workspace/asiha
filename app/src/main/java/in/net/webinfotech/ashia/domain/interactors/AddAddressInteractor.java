package in.net.webinfotech.ashia.domain.interactors;

/**
 * Created by Raj on 02-08-2019.
 */

public interface AddAddressInteractor {
    interface Callback {
        void onAddressAddSuccess();
        void onAddressAddFailed(String errorMsg);
    }
}
